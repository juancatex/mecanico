<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<style>


.body_wrapper {
    padding: 10px 20px 10px 20px;
    background: rgb(255, 255, 255) none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    max-width: 750px;
    margin: 0 auto;
    
}
table {
        
        border-collapse: collapse;
        /* border: 1px solid #000; */
        width: 100%;
        font-size: 12px; 
        
        caption-side: top; 
    }
    .borr{
        border-bottom: 1px solid gray;
    }
    
    body{/* quitar el body para la impresion*/
        font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        position: relative;
        font-size:12px;
        
       /*    
        margin: 8px auto 8px auto;
         */

    }
    p{
        text-align: justify;
        font-size: 16px;
    }
    span{
        font-weight: bold;
    }
    .span2{
        font-size: 10px;
    }
    .derecha{
        text-align:right;
    }
</style>
<body style="text-align:center">
<div class="body_wrapper">

 

   
   <table>
       <tr>
           <td style="width: 130px;padding: 13px;">
           <img src="{{$foto}}" width="90%">
           <div style="color: #0c49a5;text-align: center;font-size: 8px;">ESPECIALISTAS EN:<br>Reparación de Cajas<br>Automáticas y secuenciales</div>
           </td>
           <td colSpan="2" style="text-align: center;">
                    <h1 style="color: #0c49a5;">REPORTE DE CLIENTES</h1> 
                    <h4 style="color: #0c49a5;">Av. Estructurante No 2000 (Zona Villa Mercedes I) <br>El Alto  La Paz - Bolivia<br>Cel. 775 02154<br>Cel. 725 28247</h4>
           </td> 
       </tr>
   </table>

   <hr>
  
   <table>
       <thead>
           <tr>
           <th>Nº</th> 
           <th scope="col">Foto</th>
            <th scope="col">Nombre</th>
            <th scope="col">Apellidos</th>
            <th scope="col">Email</th>
            <th scope="col">Dirección</th>
            <th scope="col">Telefono</th>
            <th scope="col">C.I</th>
           </tr>
          
       </thead>
       <tbody>
           @php 
           $i=1; 
           @endphp
           @foreach ($clientes as $cliente)
           <tr class="borr">
                <td>{{$i}}</td>  
                @if ($cliente->foto)
                <td><img src="{{ url('storage/'.$cliente->foto) }}" style="width:40px; height:40px; padding:2px; border:1px solid gray;margin:5px"/> </td> 
                @else    
                <td></td>                 
                @endif  
                <td>{{ $cliente->nomcli}}</td>
                <td>{{ $cliente->apcli}}</td>
                <td>{{ $cliente->emailcli}}</td>
                <td>{{ $cliente->dircli}}</td>
                <td>{{ $cliente->telcli}}</td>
                <td>{{ $cliente->ci}}</td> 
           </tr>
           <tr style="height: 15px;"></tr>
           @php 
           $i=$i+1
           @endphp
           @endforeach
            
       </tbody>
   </table>
<hr>
   

  

   
    
    </div> 
</body>
</html>