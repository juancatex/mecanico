<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<style>


.body_wrapper {
    padding: 10px 20px 10px 20px;
    background: rgb(255, 255, 255) none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    max-width: 750px;
    margin: 0 auto;
    
}
table {
        
        border-collapse: collapse;
        /* border: 1px solid #000; */
        width: 100%;
        font-size: 12px; 
        
        caption-side: top; 
    }
    .borr{
        border: 1px solid gray;
    }
    
    body{/* quitar el body para la impresion*/
        font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        position: relative;
        font-size:12px;
        
       /*    
        margin: 8px auto 8px auto;
         */

    }
    p{
        text-align: justify;
        font-size: 16px;
    }
    span{
        font-weight: bold;
    }
    .span2{
        font-size: 10px;
    }
    .derecha{
        text-align:right;
    }
</style>
<body style="text-align:center">
<div class="body_wrapper"> 
   
   <table>
   <tr>
           <td colSpan="3">
           <h2 style="color: red;padding:0px;margin:0px;text-align: right;">Cod. {{ str_pad($codigo,6,"0", STR_PAD_LEFT)}}</h2> 
           </td>
       </tr>
       <tr>
           <td style="width: 130px;padding: 13px;">
           <img src="{{$foto}}" width="90%">
           <div style="color: #0c49a5;text-align: center;font-size: 8px;">ESPECIALISTAS EN:<br>Reparación de Cajas<br>Automáticas y secuenciales</div>
           </td>
           <td colSpan="2" style="text-align: center;">
                    <h1 style="color: #0c49a5;">REPORTE DE PAGOS</h1> 
                    <h4 style="color: #0c49a5;">Av. Estructurante No 2000 (Zona Villa Mercedes I) <br>El Alto  La Paz - Bolivia<br>Cel. 775 02154<br>Cel. 725 28247</h4>
           </td> 
       </tr>
   </table>

   <hr>
  
   <table>
       <thead>
           <tr>
           <th  class="borr">Nº</th>  
            <th  class="borr" scope="col">Vehiculo</th> 
            <th  class="borr" scope="col">Codigo de orden de trabajo</th> 
            <th  class="borr" scope="col">Detalle</th>
            <th  class="borr" scope="col">Monto</th>
            <th  class="borr" scope="col">Fecha</th>
           </tr>
          
       </thead>
       <tbody>
           @php 
           $i=1;  
           @endphp
           @php    
           $sumat=0; 
           @endphp
           @foreach ($pagos as $vehi)
           <tr >
                <td class="borr" style="text-align: center;">{{$i}}</td>  
                <td class="borr" style="text-align: left;">{{$vehi->placa}} : {{$vehi->marca}} - {{$vehi->modelo}}</td> 
                <td class="borr" style="text-align: center;">{{ str_pad($vehi->idorden,6,"0", STR_PAD_LEFT)}}</td> 
                <td class="borr" >{!! nl2br(e($vehi->detalle)) !!}</td>
                <td class="borr" style="text-align: right;">{{ $vehi->monto}} Bs.</td> 
                <td class="borr" style="text-align: center;">{{ $vehi->created_at}}</td>                   
           </tr>
           @php 
           $i=$i+1 
           @endphp
           @php  
           $sumat=$sumat+$vehi->monto;
           @endphp
           @endforeach
           <tr >
                <td></td>  
                <td></td>  
                <td></td> 
                <td class="borr" style="text-align: right;"><b>Total:</b></td> 
                <td class="borr" style="text-align: right;">{{$sumat}} Bs.</td> 
                <td></td>  
           </tr>
       </tbody>
   </table> 
   
    
    </div> 
</body>
</html>