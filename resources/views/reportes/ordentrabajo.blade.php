<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<style>
 @page {
                margin: 0.6cm 1cm;
            }

.body_wrapper {
    padding: 10px 20px 10px 20px;
    background: rgb(255, 255, 255) none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px;
    max-width: 750px;
    margin: 0 auto;
    
}
table {
        
        border-collapse: collapse;
        /* border: 1px solid #000; */
        width: 100%;
        font-size: 12px; 
        
        caption-side: top; 
    }
    .borr{font-size: 14px;
        border: 1px solid gray;
    }
    
    body{/* quitar el body para la impresion*/
        font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        position: relative;
        font-size:12px;
        
       /*    
        margin: 8px auto 8px auto;
         */

    }
    p{
        text-align: justify;
        font-size: 16px;
    }
    span{
        font-weight: bold;
    }
    .span2{
        font-size: 10px;
    }
    .derecha{
        text-align:right;
    }
    footer {
                position: fixed; 
                bottom: 0px; 
                left: 0px; 
                right: 0px; 
                
            }
</style>
<body style="text-align:center">
<div class="body_wrapper"> 
   <table>
       <tr>
           <td colSpan="3">
           <h2 style="color: red;padding:0px;margin:0px;text-align: right;">Cod. {{ str_pad($orden->idorden,6,"0", STR_PAD_LEFT)}}</h2> 
           </td>
       </tr>
       <tr>
           <td style="width: 150px;padding: 13px;text-align: center;">
           <img src="{{$foto}}" width="60%">
           <div style="color: #0c49a5;text-align: center;font-size: 8px;">ESPECIALISTAS EN: Reparación de Cajas Automáticas y secuenciales</div>
           <h4 style="color: #0c49a5;margin:0px;font-size: 8px;">Av. Estructurante No 2000 (Zona Villa Mercedes I)  El Alto  La Paz - Bolivia<br>Cel. 775 02154  Cel. 725 28247</h4>
           </td>
           <td colSpan="2" style="text-align: center;">
                    <h1 style="color: #0c49a5;">ORDEN DE TRABAJO </h1> 
                   <table >
                       <tr style="background-color: #ebebeb;font-size: 10px;">
                           <td rowSpan="3"> 
                           @if ($orden->foto)
                           <img src="{{ url('storage/'.$orden->foto) }}" 
                           style="width:50px; padding:2px; border:1px solid gray;margin:5px"/> 
                            @else    
                            <img src="{{ url('storage/user.png') }}" 
                                    style="width:50px; padding:2px; border:1px solid gray;margin:5px"/>             
                            @endif
                          
                        </td>
                           <td><span style="font-weight: bold;">Nombre:</span> {{$orden->nomcli}} {{$orden->apcli}}</td>

                           <td rowSpan="3"> 
                           @if ($orden->fotov)
                           <img src="{{ url('storage/'.$orden->fotov) }}" 
                           style="width:50px; padding:2px; border:1px solid gray;margin:5px"/> 
                            @else    
                            <img src="{{ url('storage/nologo.jpg') }}" 
                                    style="width:50px; padding:2px; border:1px solid gray;margin:5px"/>             
                            @endif
                          
                        </td>
                           <td><span style="font-weight: bold;">Marca - Modelo - Año :</span>  {{$orden->marca}} - {{$orden->modelo}} - {{$orden->anio}}</td>
                       </tr>
                       <tr style="background-color: #ebebeb;font-size: 10px;">
                           <td><span style="font-weight: bold;">Dirección:</span> {{$orden->dircli}}</td>
                           <td><span style="font-weight: bold;">Fecha de registro :</span> {{$orden->created_at}}</td>
                       </tr>
                       <tr style="background-color: #ebebeb;font-size: 10px;">
                           <td><span style="font-weight: bold;">Telefono:</span> {{$orden->telcli}}</td>
                           <td><span style="font-weight: bold;">      Placa :</span> {{$orden->placa}}</td>
                       </tr>
                       
                   </table>
           </td> 
       </tr>
   </table>
   <h2 style="text-align: left;background-color: #403939;
    color: white;
    padding: 6px;">Diagnostico :</h2> 
  
   <table>
    <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento correctivo</th>  
    <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento Periodico</th>  
    <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento Programado</th>  
    <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento Predictivo</th> 
       <tr>
           
       <td style="font-size: 9px !important;"> 
                                              <table style="width: 100%;padding-left: 10px;"> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Correcion en el motor</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mc1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Arreglo de la suspension</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mc2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de llanta</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mc3==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Reparacion de transmision y direccion</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mc4==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Revision del sistema de refrigeracion</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mc5==0?'No':'Si'}}</td></tr> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de bateria</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mc6==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td> 
                                        <td style="font-size: 9px !important;"> 
                                              <table style="width: 100%;padding-left: 10px;"> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Revision de la presion de los neumaticos</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mp1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Revisar el liquido de transmicion</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mp2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Liquido de freno</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mp3==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de bujia</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mp4==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de amortiguadores</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mp5==0?'No':'Si'}}</td></tr> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de filtro de combustible</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mp6==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td> 
                                        <td style="font-size: 9px !important;"> 
                                              <table style="width: 100%;padding-left: 10px;"> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Limpieza tecnica de equipos</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mpr1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Sustitucion de elementos a desgaste (Rodetes, Rodamientos, Cojinetes, Culata)</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mpr2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Kilometraje</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mpr3==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td> 
                                        <td style="font-size: 9px !important;"> 
                                              <table style="width: 100%;padding-left: 10px;"> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambiar el aceite de motor</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mpre1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambiar el filtro de aire</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mpre2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambiar el filtro de aceite</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mpre3==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Presion de llantas</td><td style="font-size: 10px !important;padding:0px">{{ $orden->mpre4==0?'No':'Si'}}</td></tr>
                                                </table> 
                                        </td> 
       </tr>
   </table>
   <br> 
  <table> 
      <tbody> 
             <tr >
               <td class="borr" style="text-align: left; min-height: 30px;padding:15px;font-size: 10px !important;">{{$orden->diag}}</td>    
            </tr>  
      </tbody>
  </table>
 
<!-- <h2 style="color: #0c49a5;">Detalle Mano de Obra</h2>  -->
<h2 style="text-align: left;background-color: #403939;
    color: white;
    padding: 6px;margin-top:15px;">Detalle Mano de Obra</h2>   
   <table>
       <thead>
           <tr>
           <th class="borr">Nº</th> 
            <th class="borr" scope="col">Detalle de Trabajo</th> 
            <th class="borr" scope="col">Monto</th>  
            <th class="borr" scope="col">Fecha de registro</th>  
           </tr>
          
       </thead>
       <tbody>
       @php 
           $i=1; 
           @endphp
           @foreach ($mano as $manoobr)
           <tr >
                <td class="borr" style="text-align: center;">{{$i}}</td>  
                <td class="borr" >{{ $manoobr->detalle}}</td> 
                <td class="borr" style="text-align: right;">{{ $manoobr->monto}} Bs.</td>  
                <td class="borr" style="text-align: center;">{{ $manoobr->created_at}}</td>   
           </tr>
           @php 
           $i=$i+1
           @endphp
           @endforeach
       </tbody>
   </table> 
    
   <!-- <h2 style="color: #0c49a5;">Detalle de Repuestos</h2>  -->
   <h2 style="text-align: left;background-color: #403939;
    color: white;
    padding: 6px;margin-top:15px;">Detalle de Repuestos</h2>  
  
  <table>
      <thead>
          <tr>
          <th class="borr">Nº</th> 
           <th class="borr" scope="col">Detalle de repuestos</th> 
           <th class="borr" scope="col">Cantidad</th>  
           <th class="borr" scope="col">Monto</th>  
           <th class="borr" scope="col">Fecha de registro</th>  
          </tr>
         
      </thead>
      <tbody>
      @php 
          $i=1; 
          @endphp
          @foreach ($repu as $repues)
          <tr >
               <td class="borr" style="text-align: center;">{{$i}}</td>  
               <td class="borr" >{!! nl2br(e($repues->detalle)) !!}</td>  
               <td class="borr" style="text-align: center;" >{{ $repues->cant}}</td> 
               <td class="borr" style="text-align: right;">{{ $repues->monto}} Bs.</td>  
               <td class="borr" style="text-align: center;">{{ $repues->created_at}}</td>   
          </tr>
          @php 
          $i=$i+1
          @endphp
          @endforeach
      </tbody>
  </table>  
   <!-- <h2 style="color: #0c49a5;">Resumen</h2>  -->
   <h2 style="text-align: left;background-color: #403939;
    color: white;
    padding: 6px;margin-top:15px;">Resumen</h2>   
  <table>
       
      <tbody> 
          <tr >
               <th class="borr" style="text-align: right;">Subtotal Repuestos : </th>   
               <td class="borr" style="text-align: right;" >{{ $orden->sumrepuestos}} Bs.</td> 
            </tr> 
            <tr>
               <th class="borr" style="text-align: right;">Subtotal Mano de obra : </th>   
               <td class="borr" style="text-align: right;" >{{ $orden->summano}} Bs.</td>   
          </tr>
          <tr>
               <th class="borr" style="text-align: right;font-size: 15px;">Precio Total (Expresado en Bolivianos): </th>   
               <td class="borr" style="text-align: right;font-size: 17px;font-weight: bold;" >{{ $orden->total}} Bs.</td>   
          </tr>
          
      </tbody>
  </table>
  <br>
 
  <br><br><br>
  <div>
   <table> 
      <tbody> 
             <tr >
               <td class="" style="text-align: left; min-width: 180px;padding:15px;text-align: center;"> </td>   
               <td class="" style="text-align: left; height: 80px;padding:15px;text-align: center;"> <br><br>______________________<br>Firma Mecanico</td>   
               <td class="" style="text-align: left; height: 80px;padding:15px;text-align: center;"> <br><br>______________________<br>Firma Cliente</td>   
            </tr>  
      </tbody>
  </table>
  </div>
 
 
    
    </div> 
    <footer>
      <div style="font-size: 6px;text-align:left;"> Yo autorizo la reparación de mi vehículo, con todos los repuestos necesarios que se requiera como también su testeo de conducción dentro del radio urbano de la ciudad, así mismo reconozco que mi vehículo es usado y que durante la reparación puede sufrir desperfectos imprevistos en repuestos mecánicos o electrónicos, no atribuible al taller. Me responsabilizo por todo accesorio extra, efectos personales, que no se consignen en inventario de recepción de mi vehículo libero de toda acción penal o civil, en caso de embargo, incautación, capturas u otros provenientes de autoridades, como también por robos, incendios, desastres naturales o problemas de convulsión social.
      <br>Cancelare en efectivo por servicios prestados en su totalidad dentro de 48hrs., caso contrario me someto a costos de parqueo.
      <br>Precio estimado puede variar en repuestos, mano de obra y tiempo de entrega de acuerdo al desarrollo de trabajo"</div>
    </footer>
</body>
</html>