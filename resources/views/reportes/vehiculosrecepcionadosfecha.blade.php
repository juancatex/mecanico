<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
</head>
<style>


.body_wrapper {
    padding: 10px 10px 10px 10px;
    background: rgb(255, 255, 255) none;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    border-radius: 5px; 
    margin: 0 auto;
    
}
table {
        
        border-collapse: collapse;
        /* border: 1px solid #000; */
        width: 100%;
        font-size: 12px; 
        
        caption-side: top; 
    }
    .borr{
        border: 1px solid gray;
    }
    
    body{/* quitar el body para la impresion*/
        font-family: "Trebuchet MS", Verdana, Arial, Helvetica, sans-serif;
        position: relative;
        font-size:12px;
        
       /*    
        margin: 8px auto 8px auto;
         */

    }
    p{
        text-align: justify;
        font-size: 16px;
    }
    span{
        font-weight: bold;
    }
    .span2{
        font-size: 10px;
    }
    .derecha{
        text-align:right;
    }
</style>
<body style="text-align:center">
<div class="body_wrapper">

 

   
   <table >
       <tr>
           <td style="width: 180px;padding: 1px;text-align: center;">
           <img src="{{$foto}}" width="30%">
           <div style="color: #0c49a5;text-align: center;font-size: 8px;">ESPECIALISTAS EN:<br>Reparación de Cajas Automáticas y secuenciales</div>
           </td>
           <td   style="text-align: center;">
                    <h2 style="color: #0c49a5;padding:0px;margin:0px">REPORTE DE VEHICULOS RECEPCIONADOS EN FECHA: {{$fecha}}</h2> 
                    <h5 style="color: #0c49a5;padding:0px;margin:0px">Av. Estructurante No 2000 (Zona Villa Mercedes I) <br>El Alto  La Paz - Bolivia<br>Cel. 775 02154<br>Cel. 725 28247</h5>
           </td> 
       </tr>
   </table>

   <hr>
  
   <table>
       <thead>
           <tr>
            <th class="borr" style="font-size: 9px !important;">Nº</th> 
            <th class="borr" style="font-size: 9px !important;min-width: 50px;" scope="col">Fecha</th>
            <th class="borr" style="font-size: 9px !important;" scope="col">Vehículo </th>
            <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Mantenimiento</th>   
            <th class="borr" style="font-size: 9px !important;" scope="col">Diagnostico</th> 
            <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Recepcion Vehicular</th>  
            <th class="borr" style="font-size: 9px !important;" scope="col">Observación</th> 
           </tr>
          
       </thead>
       <tbody>
           @php 
           $i=1; 
           @endphp
           @foreach ($vehiculos as $recepcionn)
           <tr>
                <td class="borr" style="font-size: 8px !important;text-align: center;">{{ $recepcionn->idrec}}</td>  
                <td class="borr" style="font-size: 8px !important;text-align: center;">{{ str_replace(' ',"\n",$recepcionn->created_at)}}</td>  
                <td class="borr" style="font-size: 8px !important;text-align: center;">{{ $recepcionn->placa}} - {{ $recepcionn->marca}} - {{ $recepcionn->modelo}} </td>
                <td class="borr" style="font-size: 8px !important;"> 
                                              <table style="width: 100%;"> 
                                              <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Mantenimiento Correctivo</th>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Correcion en el motor</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mc1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Arreglo de la suspension</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mc2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de llanta</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mc3==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Reparacion de transmision y direccion</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mc4==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Revision del sistema de refrigeracion</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mc5==0?'No':'Si'}}</td></tr> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de bateria</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mc6==0?'No':'Si'}}</td></tr> 
                                                <!-- </table> 
                                        </td> 
                                        <td class="borr" style="font-size: 8px !important;"> 
                                              <table style="width: 100%;">  -->
                                              <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Mantenimiento Periodico</th>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Revision de la presion de los neumaticos</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mp1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Revisar el liquido de transmicion</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mp2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Liquido de freno</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mp3==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de bujia</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mp4==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de amortiguadores</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mp5==0?'No':'Si'}}</td></tr> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambio de filtro de combustible</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mp6==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td> 
                                        <td class="borr" style="font-size: 8px !important;"> 
                                              <table style="width: 100%;"> 
                                              <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Mantenimiento Programado</th>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Limpieza tecnica de equipos</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mpr1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Sustitucion de elementos a desgaste (Rodetes, Rodamientos, Cojinetes, Culata)</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mpr2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Kilometraje</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mpr3==0?'No':'Si'}}</td></tr> 
                                                <!-- </table> 
                                        </td> 
                                        <td class="borr" style="font-size: 8px !important;"> 
                                              <table style="width: 100%;">  -->
                                              <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Mantenimiento Predictivo</th>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambiar el aceite de motor</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mpre1==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambiar el filtro de aire</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mpre2==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cambiar el filtro de aceite</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mpre3==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Presion de llantas</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->mpre4==0?'No':'Si'}}</td></tr>
                                                </table> 
                                        </td> 

                <td class="borr" style="font-size: 8px !important;text-align: center;">{{ $recepcionn->diag}}</td>  
                <td class="borr" style="font-size: 8px !important;">
                                              <table style="width: 100%;"> 
                                              <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Recepcion entrante Vehicular</th>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Luces principales</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev1==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Luces stop - guiñadores</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev2==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Antena de radio</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev3==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Limpia parabrisas</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev4==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Espejo lateral derecho</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev5==0?'No':'Si'}}</td></tr> 
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Espejo lateral izquierdo</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev6==0?'No':'Si'}}</td></tr> 
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Vidrios laterales</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev7==0?'No':'Si'}}</td></tr> 
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Tapon de gasolina</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev8==0?'No':'Si'}}</td></tr> 
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Placas delantera y trasera</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->rev9==0?'No':'Si'}}</td></tr> 
                                              <!-- </table> 
                                        </td> 
                                          <td class="borr" style="font-size: 8px !important;">
                                              <table style="width: 100%;">  -->
                                              <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Exteriores</th>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Unidad de luces</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->ul==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Antenas</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->an==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Espejo lateral</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->el==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Llantas (4)</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->llan==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Bocinas claxon</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->bx==0?'No':'Si'}}</td></tr> 
                                              </table> 
                                        </td> 

                                          <td class="borr" style="font-size: 8px !important;"> 
                                              <table style="width: 100%;"> 
                                              <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Accesorios</th>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Gata</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->ga==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Llave de rueda</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->llar==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Llave de contacto</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->llac==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Estuche herramienta</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->eh==0?'No':'Si'}}</td></tr>
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Triangulo de seguridad</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->ts==0?'No':'Si'}}</td></tr> 
                                                  <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Llanta de auxilio</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->llax==0?'No':'Si'}}</td></tr> 
                                                <!-- </table> 
                                        </td>  

                                        <td class="borr" style="font-size: 8px !important;">
                                            <table style="width: 100%;">  -->
                                            <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Interiores </th>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Radio</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->ra==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Instrumentos tablero</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->it==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Encendedor</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->en==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Calefaccion</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->ca==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Cenicero</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->ce==0?'No':'Si'}}</td></tr> 
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Tapetes</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->to==0?'No':'Si'}}</td></tr> 
                                            <!-- </table>
                                        </td>
                                        <td class="borr" style="font-size: 8px !important;">
                                            <table style="width: 100%;">  -->
                                            <th class="borr" style="font-size: 9px !important;min-width: 150px;" colSpan="2" scope="col">Otros </th>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Soat</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->otro1==0?'No':'Si'}}</td></tr>
                                                <tr><td style="border-bottom: 1px solid gray;font-size: 8px !important;padding:0px;padding-right: 5px;">Inspeccion tecnica</td><td style="font-size: 8px !important;padding:0px">{{ $recepcionn->otro2==0?'No':'Si'}}</td></tr>
                                            </table>
                                        </td>
                <td class="borr" style="font-size: 8px !important;text-align: center;">{{ $recepcionn->obsrec}}</td> 
                  
           </tr>
           @php 
           $i=$i+1
           @endphp
           @endforeach
            
       </tbody>
   </table>
 
   

  

   
    
    </div> 
</body>
</html>