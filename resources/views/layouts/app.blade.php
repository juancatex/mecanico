<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="apple-touch-icon" sizes="180x180" href="src/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="src/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="src/images/favicon-16x16.png">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet"> 
     
    <link rel="stylesheet" type="text/css" href="src/styles/core.css">
	<link rel="stylesheet" type="text/css" href="src/styles/icon-font.min.css">
    <link rel="stylesheet" type="text/css" href="src/plugins/datatables/css/dataTables.bootstrap4.min.css">
	<link rel="stylesheet" type="text/css" href="src/plugins/datatables/css/responsive.bootstrap4.min.css">
    <link rel="stylesheet" type="text/css" href="src/plugins/sweetalert2/sweetalert2.css">
    <link rel="stylesheet" type="text/css" href="src/plugins/jquery-steps/jquery.steps.css">
    <link rel="stylesheet" type="text/css" href="src/styles/style.css"> 
    <link rel="stylesheet" type="text/css" href="src/styles/bootstrap-select.min.css"> 
    <link rel="stylesheet" type="text/css" href="css/huicalendar.css"> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script> 
    <!-- <script src="{{ asset('js/app.js') }}" defer></script>   -->
    <script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date()); 
		gtag('config', 'UA-119386393-1');
	</script>
<script src="src/plugins/sweetalert2/sweetalert2.all.js"></script> 
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body class="header-dark sidebar-dark" style=" 
    background-image: url(src/images/fondo2.png);
    background-position: center; 
    background-size: cover;">
        @include('sweetalert::alert')
        @yield('load')  
    
    <div id="app" > 
        @yield('bodyheader') 
        @yield('menus') 
        <div class="mobile-menu-overlay"></div>
        @yield('content') 
    </div>
      
    <script src="src/scripts/core.js"></script>
	<script src="src/scripts/script.min.js"></script>
	<script src="src/scripts/process.js"></script>
	<script src="src/scripts/layout-settings.js"></script> 
	<script src="src/plugins/apexcharts/apexcharts.min.js"></script>
	<script src="src/plugins/datatables/js/jquery.dataTables.min.js"></script>
	<script src="src/plugins/datatables/js/dataTables.bootstrap4.min.js"></script>
	<script src="src/plugins/datatables/js/dataTables.responsive.min.js"></script>
	<script src="src/plugins/datatables/js/responsive.bootstrap4.min.js"></script>
    <script src="src/plugins/sweetalert2/sweetalert2.all.js"></script> 
    <script src="src/plugins/jquery-steps/jquery.steps.js"></script> 
    <script src="src/scripts/bootstrap-select.min.js"></script>
    <script src="js/huicalendar.js"></script>
	<script>function mensa(paso){
        swal({
                title: '¿Esta seguro de eliminar este registro?',
                text: "Este proceso es irreversible",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success margin-5',
                cancelButtonClass: 'btn btn-danger margin-5',
                buttonsStyling: false
            }).then(function (dismiss) { 
                if (dismiss.value) {
                    paso();
                } 
            })
    }function mensa2(paso){
        swal({
                title: '¿Esta seguro de realizar el proceso?',
                text: "Este proceso es irreversible",
                type: 'warning',
                showCancelButton: true,
                confirmButtonText: 'Si',
                cancelButtonText: 'No',
                confirmButtonClass: 'btn btn-success margin-5',
                cancelButtonClass: 'btn btn-danger margin-5',
                buttonsStyling: false
            }).then(function (dismiss) { 
                if (dismiss.value) {
                    paso();
                } 
            })
    }
    $(".tab-wizard").steps({
	headerTag: "h5",
	bodyTag: "section",
	transitionEffect: "fade",
	titleTemplate: '<span class="step">#index#</span> #title#',
	labels: {
		finish: "Registrar",
		next: "Siguiente",
		previous: "Anterior",
	},
	onStepChanged: function (event, currentIndex, priorIndex) {
		$('.steps .current').prevAll().addClass('disabled');
	},
	onFinished: function (event, currentIndex) {
            $(".tab-wizard").submit();
            }
});

$('#marca').selectpicker();

const d = new Date();
let day = d.getDate();
$('.calendariolateral').huicalendar({
		enabledDay: [day],
  viewDay:new Date()
});
</script>
<script>
  
   function ff(file){
    var imgURL = URL.createObjectURL(file),
        img = document.createElement('img');
 
    img.onload = function() {
      URL.revokeObjectURL(imgURL);
    };
 
    img.src = imgURL; 
    img.style.width = "170px"; 
    img.style.border = "4px solid gray"; 
    img.style.height = "170px"; 
    img.style.objectFit = "cover";
    img.style.borderRadius = "50%";  
    $('#previewiamgedit').empty().append(img);
   }
   function ffoto(file){
    var imgURL = URL.createObjectURL(file),
        img = document.createElement('img');
 
    img.onload = function() {
      URL.revokeObjectURL(imgURL);
    };
 
    img.src = imgURL; 
    img.style.width = "170px"; 
    img.style.border = "4px solid gray"; 
    img.style.height = "170px"; 
    img.style.objectFit = "cover";
    img.style.borderRadius = "50%";  
    $('#previewiamge').empty().append(img);
   }
   
</script>
</body>
</html>
