<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1"> 
    <meta name="csrf-token" content="{{ csrf_token() }}"> 
    <title>{{ config('app.name', 'Laravel') }}</title>
    <link rel="apple-touch-icon" sizes="180x180" href="src/images/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="src/images/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="src/images/favicon-16x16.png">
    <link href="https://fonts.googleapis.com/css2?family=Inter:wght@300;400;500;600;700;800&display=swap" rel="stylesheet"> 
	<link rel="stylesheet" type="text/css" href="src/styles/core.css">
	<link rel="stylesheet" type="text/css" href="src/styles/icon-font.min.css">
	<link rel="stylesheet" type="text/css" href="src/styles/style.css"> 
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-119386393-1"></script> 
    <script src="{{ asset('js/app.js') }}" defer></script>  
    <script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date()); 
		gtag('config', 'UA-119386393-1');
	</script>
</head>
<body >
    <div id="app" style="height: 100%;background-color: black;"> 
    <!-- <div class="pre-loader">
		<div class="pre-loader-box">
			<div class="loader-logo"><img src="src/images/logomotorr.png" alt="" width="30%"></div>
			<div class='loader-progress' id="progress_div">
				<div class='bar' id='bar1'></div>
			</div>
			<div class='percent' id='percent1'>0%</div>
			<div class="loading-text">
				Cargando...
			</div>
		</div>
	</div> --> 
        <main style="height: 100%;" class="login-page">
            @yield('content')
        </main>
    </div>

    <script src="src/scripts/core.js"></script>
	<script src="src/scripts/script.min.js"></script>
	<script src="src/scripts/process.js"></script>
	<script src="src/scripts/layout-settings.js"></script>
</body>
</html>
