<!DOCTYPE html>
<html lang="en" >
<head>
  <meta charset="UTF-8">
  <title>CodePen - 3D Cutout Card</title>
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.0.0-beta/css/bootstrap-reboot.css'> 
<style>
   .card { 
  height: 300px;
  width: 500px; 
  background: #212529; 
  justify-content: flex-start;
  align-items: center;
  padding: 20px;
  color: #fff; 
  font-size: 60px; 
  backface-visibility: hidden;
  box-shadow: 0 10px 30px -3px rgba(0,0,0,.1);
  background-image: radial-gradient(circle farthest-corner at -128px 72px , rgb(255, 0, 0) 33%, rgba(0, 0, 0, 0) 0%, rgb(0, 0, 0) 50%);

} 
h1 {
  text-align: right;
  font-size: 30px; 
  text-transform: uppercase;
  font-weight: 900;
} 
.card .enclosed { 
  color: rgba(249, 198, 26, 1);
  padding: 0 5px; 
  transform: translate(-1px, 1px) scale(0.75);
  transform-origin: right center;
} 
html,
body {
  height: 100%;
}
.contenido{
  width: 82%;
  font-size: 15px;
    padding: 15px;
  border-radius: 8px; 
  height: 148px;
  margin-top: 20px;
  margin-left: auto;
  margin-right: 0;
  border: 5px solid white;
}
.men{
  font-size: 10px;
    margin-top: 24px;
}
 
</style>
</head>
<body>
<!-- partial:index.partial.html -->
<div class="">
  <div class="card">
    <h1>
      <span class="enclosed">Fama</span>Motors
    </h1>
    <div class="contenido"> 
    {{$mensaje}}
    </div>
    <div class="men"> Especialistas en reparacion de cajas manuales y automaticas. <span style="float: right;">cel.: 77502154 - 72528247</span></div>
  </div>
</div>
<!-- partial -->
  
</body>
</html>
