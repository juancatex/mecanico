@extends('layouts.app')


@section('load')  
	@if (!session('mensaje'))
		<div class="pre-loader">
					<div class="pre-loader-box">
						<div class="loader-logo" style="text-align:center"><img src="src/images/logomotorr.png" alt="" width="50%"></div>
						<div class='loader-progress' id="progress_div">
							<div class='bar' id='bar1'></div>
						</div>
						<div class='percent' id='percent1'>0%</div>
						<div class="loading-text">
							Cargando...
						</div>
					</div>
		</div>
	   @endif
@endsection

@section('bodyheader') 
<div class="header">
<div class="header-left">
			<div class="menu-icon dw dw-menu"></div> 
		</div>
		<div class="header-right"> 
			<div class="user-info-dropdown">
				<div class="dropdown">
					<a class="dropdown-toggle" href="#" role="button" data-toggle="dropdown">
						<span class="user-icon">
							<img src="src/images/product-img1.jpg" alt="">
						</span>
						<span class="user-name">{{ Auth::user()->usuario }}</span>
					</a>
					<div class="dropdown-menu dropdown-menu-right dropdown-menu-icon-list"> 
						<!-- <a class="dropdown-item" href="login.html"><i class="dw dw-logout"></i> Log Out</a> -->
                        <a class="dropdown-item" href="{{ route('logout') }}"  onclick="event.preventDefault();
                                                     document.getElementById('logout-route').submit();">
                                     <i class="dw dw-logout"></i>{{ __('Logout') }} </a> 
                                    <form id="logout-route" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
					</div>
				</div>
			</div> 
		</div>
 </div>
 @endsection


@section('content')  
 
 @if (session('menu'))
            @switch(session('submenu'))
                @case(1)
				   @include('pages.clientes') 
                @break 
                @case(2)
				   @include('pages.clientesreportes') 
                @break
				@case(3)
				   @include('pages.vehiculo') 
                @break
				@case(4)
				   @include('pages.recepcion') 
                @break
				@case(5)
				  @include('pages.vehiculosreportes')  
                @break
				@case(6)
				@include('pages.ordentrabajo') 
                @break
				@case(7)
				@include('pages.reparacion') 
                @break

				@case(9)
				@include('pages.email') 
                @break
				@case(10)
				@include('pages.pagos') 
                @break
				@case(11)
				@include('pages.pagosreportes') 
                @break
 
            @endswitch 
 @else
 @include('pages.principal') 
@endif
 
     
@endsection


 @section('menus')
 
 <div class="left-side-bar">
		<div class="brand-logo">
			<a href="{{ url('/') }}"> 
				<img src="src/images/logomotorr.png" alt="" class="light-logo">
			</a>
			<div class="close-sidebar" data-toggle="left-sidebar-close">
				<i class="ion-close-round"></i>
			</div>
		</div>
		<div class="menu-block customscroll">
			<div class="sidebar-menu">
				<ul id="accordion-menu">
					 
	    <?php 
        use App\Http\Controllers\MenuController;
		use App\Http\Controllers\submenuController;
        $menus = MenuController::listarmenus();                                                                                                                                               
        ?>

@foreach ($menus as $menu) 

	                <li class="dropdown">
						<a href="javascript:;" class="dropdown-toggle" id="menu{{$menu->idmenu}}">
							<span class="micon dw {{ $menu->icon }}"></span><span class="mtext">{{ $menu->nombremenu }}</span>
						</a>
						<ul class="submenu">
						<?php  
						$submenus = submenuController::listarsubmenus($menu->idmenu);                                                                                                                                               
						?>
						@foreach ($submenus as $smenu)  
							<li><a id="sub{{$smenu->idsubmenu}}" href="{{ route('menus',['idmenu' => $menu->idmenu,'idsubmenu' =>$smenu->idsubmenu,'pos' =>1]) }}">{{ $smenu->nombresubmenu }}</a></li> 
						@endforeach
						</ul>
					</li>
@endforeach
<!--   
					<li>
						<div class="dropdown-divider"></div>
					</li>
					<li>
						<div class="sidebar-small-cap">Extra</div>
					</li>-->
				 
				</ul>
				
			</div>
		</div>
		
		<div class="calendariolateral" style="position: fixed;
    bottom: 10px;
    left: 10px;"></div>

	</div>
        


		@if (session('menu'))
		<script>  
                    setTimeout(function(){
                        $("#menu{{session('menu')}}").click(); 
                    }, 1500);
                </script> 
		@endif
		@if (session('submenu'))
		<script>  
                    setTimeout(function(){ 
                        $("#sub{{session('submenu')}}").addClass("active");
                    }, 1500);
                </script> 
		@endif
 @endsection