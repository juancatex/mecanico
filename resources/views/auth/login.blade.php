@extends('layouts.applogin')

@section('content')
  
<!-- /////////////////////////////////////// -->
<div class="login-wrap d-flex align-items-center flex-wrap justify-content-center">
		<div class="container">
			<div class="row align-items-center">
				<div class="col-md-6 col-lg-7">
					<img src="src/images/logomotorr.png" alt="">
				</div>
				<div class="col-md-6 col-lg-5">
					<div class="login-box bg-white box-shadow border-radius-10">
						<div class="login-title">
							<h2 class="text-center text-primary">Bienvenido</h2>
						</div> 
                        <form method="POST" action="{{ route('login') }}">
                        @csrf 
						 
							<div class="input-group custom">
								<input type="text" id="usuario" class="form-control form-control-lg @error('usuario') is-invalid @enderror" placeholder="Usuario" name="usuario" value="{{ old('usuario') }}" required autofocus>
								
                                @error('usuario')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @else
                                    <div class="input-group-append custom">
									<span class="input-group-text"><i class="icon-copy dw dw-user1"></i></span>
								    </div>
                                @enderror
							</div>
                       
							<div class="input-group custom">
								<input type="password" id="password" class="form-control form-control-lg @error('password') is-invalid @enderror" placeholder="Contraseña" name="password" required autocomplete="current-password">
								
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @else
                                    <div class="input-group-append custom">
								     	<span class="input-group-text"><i class="dw dw-padlock1"></i></span>
								    </div>
                                @enderror
							</div> 

							<div class="row pb-30">
								<div class="col-6">
									<div class="custom-control custom-checkbox">
										<input type="checkbox" class="custom-control-input" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
										<label class="custom-control-label" for="remember"> {{ __('Remember Me') }}</label>
									</div>
								</div>
								 
							</div> 
 
							<div class="row">
								<div class="col-sm-12">
									<div class="input-group mb-0"> 
									 
                                        <button type="submit" class="btn btn-primary btn-lg btn-block">
                                            {{ __('Login') }}
                                        </button>
									</div>
									 
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection
