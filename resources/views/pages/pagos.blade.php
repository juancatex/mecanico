<?php 
                            
                            use App\Http\Controllers\OrdentrabajoController;  
                            use App\Http\Controllers\PagosController;  
                            $listaordenesapro = OrdentrabajoController::ordenescombo();                                                                                                                             
                              ?>
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Pagos</h4>
							</div>
							  
						</div>
						<div class="col-md-6 col-sm-12 text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevapagos"><i class="fa fa-plus-square"></i> Nuevo Pago</button>
            <!-- /////////////////////// -->
                  <div class="modal fade" id="nuevapagos" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Nuevo Pago</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                         <form  action="{{ route('regpago') }}" method="POST" >
                         @csrf
                         <input  name="menu" type="hidden" value="{{session('menu')}}">
                         <input  name="submenu" type="hidden" value="{{session('submenu')}}">        
                                  <div class="modal-body" style="text-align: left;"> 

                                        <div class="form-group">
                                          <label>Codigo de orden de trabajo :</label> 
                                          <select class="selectpicker col-md-12" name="idorden" data-size="5" data-show-subtext="true"> 
                                            @foreach ($listaordenesapro as $orden)
                                                <!-- <option value="{{$orden->idorden}}"> {{$orden->placa}} : {{$orden->marca}} - {{$orden->modelo}}</option>  -->

                                                     @if ($orden->fotov)
                                                      <option 
                                                      data-content='<img src="{{url("storage/".$orden->fotov)}}" style="width: 30px;margin-right: 15px;"/> {{$orden->placa}} : {{$orden->marca}} - {{$orden->modelo}}'
                                                      value="{{$orden->idorden}}">{{$orden->placa}} : {{$orden->marca}} - {{$orden->modelo}}</option> 
                                                      @else 
                                                        <option 
                                                      data-content='<img src="images/nologo.jpg" style="width: 30px;margin-right: 15px;"/> {{$orden->placa}} : {{$orden->marca}} - {{$orden->modelo}}'
                                                      value="{{$orden->idorden}}">{{$orden->placa}} : {{$orden->marca}} - {{$orden->modelo}}</option> 
                                                      @endif 

                                            @endforeach
                                           </select>
                                        </div> 
                                        <div class="form-group">
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Pastillas de freno delanteros (Juego)</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Presinto de fierro</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Discos de freno delanteros</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Amortiguador delantero - trasero</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Bomba de agua</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Correa de accesorio</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Juego de Disco</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Filtro de combustible</button> 
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Poleas</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Cuerpo de valvulas</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Cinta de corona</button> 
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Muñores</button>
                                                                  </div>
                                        <div class="form-group">
                                                  <label>Detalle :</label>
                                                  <!-- <input class="form-control"  name="detalle" placeholder="Ingrese el detalle" type="text" required> -->
                                                  <textarea class="form-control" style="height: auto;" rows="4" name="detalle" id="faddobs" placeholder="Ingrese el detalle" ></textarea>
                                        </div> 
                                        <div class="form-group">
                                                  <label>Monto :</label>
                                                  <input class="form-control" name="monto" placeholder="Ingrese el monto total en Bs." type="number" required>
                                        </div> 
        
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Registrar</button>
                                  </div>
                          </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
           	</div>
					</div>
				</div>
          <!-- //////////////////// -->
                    <div class="pd-20 card-box mb-30">
                          <div class="clearfix mb-20">
                                      <div class="pull-left">
                                        <h4 class="text-blue h4">Listado de Pagos</h4>
                                       </div> 
                          </div>
                          <div class="table-responsive">
                          <?php  
                                 
                                  if (session('pos')){
                                    $pagoss = PagosController::pagosorden(session('pos')); 
                                  }else{
                                    $pagoss = PagosController::pagosorden(1); 
                                  } 
                                                                                                                                                            
                              ?>
 
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Vehiculo</th> 
                                          <th scope="col">Codigo de orden de trabajo</th> 
                                          <th scope="col">Detalles</th>
                                          <th scope="col">Monto</th>
                                          <th scope="col">Fecha</th>
                                          <th scope="col">Opciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @php
                                      $pos = 1;
                                      @endphp
                                      @foreach ($pagoss as $vehi) 
                                        <tr>
                                          <th scope="row">{{ $pos }}</th>
                                          <td style="text-align: left;">{{$vehi->placa}} : {{$vehi->marca}} - {{$vehi->modelo}}</td> 
                                          <td style="text-align: center;">{{ str_pad($vehi->idorden,6,"0", STR_PAD_LEFT)}}</td> 
                                          <td>{!! nl2br(e($vehi->detalle)) !!} </td> 
                                          <td style="text-align: right;">{{ $vehi->monto}} Bs.</td> 
                                          <td>{{ $vehi->created_at}}</td> 
                                          <td>  
                                          <a class="badge badge-danger"  style="cursor: pointer;width: 100%;  color: white;" onclick="event.preventDefault();
                                                     mensa(()=>{document.getElementById('desactivarpago{{$vehi->idpago}}').submit()});
                                                     "> <i class="fa fa fa-remove"></i> Eliminar</a>
                                       
                                        <form id="desactivarpago{{$vehi->idpago}}" action="{{ route('desactivarpagoo') }}" method="POST" class="d-none">
                                          @csrf
                                            <input  name="menu" type="hidden" value="{{session('menu')}}">
                                            <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                            <input  name="idpago" type="hidden" value="{{$vehi->idpago}}">  
                                       </form>  
                                          
                                       
                                          <!-- /////////////////////////////////////////////////    -->
                                                   
                                        <!-- /////////////////////////////////////////////////    -->
                                        </td>
                                        </tr>
                                        <tr style="height: 15px;"></tr>
                                      @php
                                      $pos++;
                                      @endphp
                                      @endforeach 
                                      </tbody>
                                    </table>
                                    @if ($pagoss->lastPage()>1)
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center;"> 
                                       @if ($pagoss->currentPage()-1>0)
                                       <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$pagoss->currentPage()-1]) }}"  class="btn btn-light"> Anterior </a>
                                      </div>
                                        @endif
                                                  <div class="btn-group mb-15"> 
                                                      @for ($i = 1; $i <= $pagoss->lastPage(); $i++) 
                                                          @if ($pagoss->currentPage()==$i)
                                                          <a class="btn btn-success ">{{$i}}</a>
                                                          @else
                                                          <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$i]) }}"  class="btn btn-light ">{{$i}}</a>
                                                          @endif 
                                                      @endfor
                                                  </div>
                                      
                                      @if ($pagoss->currentPage()+1<=$pagoss->lastPage())
                                      <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$pagoss->currentPage()+1]) }}"  class="btn btn-light"> Siguiente </a>
                                      </div> 
                                        @endif
                                    </div>
                                    @endif
                                   
                                     
                          </div> 
                  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>
<script>
  function addobs(s){
console.log($(s).text());
var $edit = $("#faddobs");

var curValue = $edit.val();

var newValue = curValue.length>0?curValue+ "\r\n"+$(s).text():$(s).text();

$edit.val(newValue);
  }
</script>