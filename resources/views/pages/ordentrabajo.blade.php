<?php 
                               
                              use App\Http\Controllers\RecepcionController;  
                              use App\Http\Controllers\OrdentrabajoController;  
                              $listarecepcionescombo = RecepcionController::listarecepcionescombo(); 
                                
                                                                                                                                                                  
                              ?>
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Orden de Trabajo</h4>
							</div>
							  
						</div>
						<div class="col-md-6 col-sm-12 text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevaorden"><i class="fa fa-plus-square"></i> Nueva Orden de Trabajo</button>
            <!-- /////////////////////// -->
                  <div class="modal fade" id="nuevaorden" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Nueva Orden de Trabajo</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div> 
                          <form  action="{{ route('regorden') }}" method="POST" >
                              @csrf
                              <input  name="menu" type="hidden" value="{{session('menu')}}">
                              <input  name="submenu" type="hidden" value="{{session('submenu')}}">  
                                <div class="modal-body" style="text-align: left;">  
                                    <!-- ///////////////////////////////////////////////////////////////////////////////////////            -->
                                    <div class="form-group">
                                                <label>Vehiculo recepcionado :</label>
                                                <select class="selectpicker col-md-12" name="idrec" data-size="5" data-show-subtext="true"> 
                                                  @foreach ($listarecepcionescombo as $recepcionn)
                                                      
                                                      @if ($recepcionn->fotov)
                                                      <option 
                                                      data-content='<img src="{{url("storage/".$recepcionn->fotov)}}" style="width: 30px;margin-right: 15px;"/> {{ $recepcionn->placa}} - {{ $recepcionn->marca}} - {{ $recepcionn->modelo}}'
                                                      value="{{$recepcionn->idrec}}">{{ $recepcionn->placa}} - {{ $recepcionn->marca}} - {{ $recepcionn->modelo}} </option> 
                                                      @else 
                                                        <option 
                                                      data-content='<img src="images/nologo.jpg" style="width: 30px;margin-right: 15px;"/> {{ $recepcionn->placa}} - {{ $recepcionn->marca}} - {{ $recepcionn->modelo}}'
                                                      value="{{$recepcionn->idrec}}">{{ $recepcionn->placa}} - {{ $recepcionn->marca}} - {{ $recepcionn->modelo}} </option> 
                                                      @endif 

                                                  @endforeach
                                              </select> 
                                    </div>
                                    <!-- ///////////////////////////////////////////////////////////////////////////////////////            --> 
                                </div> 
                                        <div class="modal-footer">
                                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                          <button type="submit" class="btn btn-primary">Registrar</button>
                                        </div>
                          </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
           	</div>
					</div>
				</div>
          <!-- //////////////////// -->
                    <div class="pd-20 card-box mb-30">
                          <div class="clearfix mb-20">
                                      <div class="pull-left">
                                        <h4 class="text-blue h4">Listado de Ordenes de Trabajo</h4>
                                       </div>
                                   <form  action="{{ route('busquedarecep') }}" method="POST" >
                                                 @csrf   
                                          <input  name="menu" type="hidden" value="{{session('menu')}}">
                                        <input  name="submenu" type="hidden" value="{{session('submenu')}}">   
                                            <div class="col-md-12 col-sm-12 text-right">
                                            <div class="row" style="display: inline-flex;">  
                                                    <div style="margin-right: 10px;">
                                                            <input class="form-control" type="text" name="buscar" placeholder="Ingrese el texto a buscar ">
                                                     </div>    
                                                    <button type="submit" class="btn btn-info"><i class="fa fa-plus-square"></i> Buscar</button> 
                                            </div>
                                      </div>
                                        </form>               
                          </div>
                          <div class="table-responsive">
                          <?php  
                                if (session('buscarorden')){
                                  $recepciones = OrdentrabajoController::busordenwhere(session('buscarorden')); 
                                }else{
                                  if (session('pos')){
                                    $recepciones = OrdentrabajoController::listaordenes(session('pos')); 
                                  }else{
                                    $recepciones = OrdentrabajoController::listaordenes(1); 
                                  } 
                                } 
                                                                                                                                                                  
                              ?>
 
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th  scope="col">#</th>
                                          <th  scope="col">Codigo</th>
                                          <th  scope="col">Vehículo</th>
                                          <th  scope="col">Subtotal repuestos</th> 
                                          <th  scope="col">Subtotal mano de obra</th> 
                                          <th  scope="col">Total</th>  
                                          <th  scope="col">Estado</th>
                                          <th  scope="col">Opciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @php
                                      $pos = 1;
                                      @endphp
                                      @foreach ($recepciones as $orden) 
                                        <tr>
                                          <th  scope="row">{{ $pos }}</th>
                                          <td style="text-align: center;">{{ str_pad($orden->idorden,6,"0", STR_PAD_LEFT)}}</td>
                                          <td >{{ $orden->placa}} - {{ $orden->marca}} - {{ $orden->modelo}} </td>
                                          <td style="text-align: right;">{{ $orden->sumrepuestos}} Bs.
                                          @if ($orden->estado==1)  
<!-- //////////////////////////////////////////////////////////////////////////// -->
                                          <a class="badge badge-info"  style="cursor: pointer;width: 100%;  color: white;" data-toggle="modal" data-target="#addrepuestos"> <i class="fa fa fa-plus"></i> Agregar</a>
                                          <div class="modal fade" id="addrepuestos" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog  modal-dialog-centered">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Registro de repuestos</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                      </div> 
                                                      <form  action="{{ route('regorderepues') }}" method="POST" >
                                                          @csrf
                                                          <input  name="menu" type="hidden" value="{{session('menu')}}">
                                                          <input  name="submenu" type="hidden" value="{{session('submenu')}}">  
                                                          <input  name="idorden" type="hidden" value="{{$orden->idorden}}">  
                                                            <div class="modal-body" style="text-align: left;">  
                                                                <!-- ///////////////////////////////////////////////////////////////////////////////////////            -->
                                                                <div class="form-group">
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Pastillas de freno delanteros (Juego)</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Presinto de fierro</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Discos de freno delanteros</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Amortiguador delantero - trasero</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Bomba de agua</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Correa de accesorio</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Juego de Disco</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Filtro de combustible</button> 
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Poleas</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Cuerpo de valvulas</button>
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Cinta de corona</button> 
                                                                  <button type="button" onclick="addobs(this)" class="btn btn-outline-dark btn-sm mb-1">Muñores</button>
                                                                  </div>
                                                                <div class="form-group">
                                                                          <label>Descripcion :</label>
                                                                          <!-- <input class="form-control"  name="desrep" placeholder="Ingrese la descripcion del repuesto" type="text" required> -->
                                                                          <textarea class="form-control" style="height: auto;" rows="4" name="desrep" id="faddobs" placeholder="Ingrese la descripcion del repuesto" ></textarea>
                                                                </div> 
                                                                <div class="form-group">
                                                                          <label>Cantidad :</label>
                                                                          <input class="form-control" name="cant" placeholder="Ingrese la cantidad" type="number" required>
                                                                </div> 
                                                                <div class="form-group">
                                                                          <label>Monto :</label>
                                                                          <input class="form-control" name="montorep" placeholder="Ingrese el monto total en Bs." type="number" required>
                                                                </div> 
                                                                
                                                                <!-- ///////////////////////////////////////////////////////////////////////////////////////            --> 
                                                            </div> 
                                                                    <div class="modal-footer">
                                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                      <button type="submit" class="btn btn-primary">Registrar</button>
                                                                    </div>
                                                      </form>
                                                    </div>
                                                  </div>
                                                </div>
<!-- //////////////////////////////////////////////////////////////////////////// -->
                                    @endif
                                          </td> 
                                          <td style="text-align: right;">{{ $orden->summano}} Bs.
                                            
                                          @if ($orden->estado==1)  
<!-- //////////////////////////////////////////////////////////////////////////// -->
                                          <a class="badge badge-info"  style="cursor: pointer;width: 100%;  color: white;" data-toggle="modal" data-target="#addmanoobra"> <i class="fa fa fa-plus"></i> Agregar</a>
                                          <div class="modal fade" id="addmanoobra" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                  <div class="modal-dialog  modal-dialog-centered">
                                                    <div class="modal-content">
                                                      <div class="modal-header">
                                                        <h4 class="modal-title" id="myLargeModalLabel">Nuevo trabajo de mano de obra</h4>
                                                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                      </div> 
                                                      <form  action="{{ route('regordenmano') }}" method="POST" >
                                                          @csrf
                                                          <input  name="menu" type="hidden" value="{{session('menu')}}">
                                                          <input  name="submenu" type="hidden" value="{{session('submenu')}}">  
                                                          <input  name="idorden" type="hidden" value="{{$orden->idorden}}">  
                                                            <div class="modal-body" style="text-align: left;">  
                                                                <!-- ///////////////////////////////////////////////////////////////////////////////////////            -->
                                                                <div class="form-group">
                                                                          <label>Descripcion :</label>
                                                                          <input class="form-control"  name="desmano" placeholder="Ingrese la descripcion del trabajo" type="text" required>
                                                                </div> 
                                                                <div class="form-group">
                                                                          <label>Monto :</label>
                                                                          <input class="form-control" name="montomano" placeholder="Ingrese el monto total en Bs." type="number" required>
                                                                </div> 
                                                                <!-- ///////////////////////////////////////////////////////////////////////////////////////            --> 
                                                            </div> 
                                                                    <div class="modal-footer">
                                                                      <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                      <button type="submit" class="btn btn-primary">Registrar</button>
                                                                    </div>
                                                      </form>
                                                    </div>
                                                  </div>
                                                </div>
<!-- //////////////////////////////////////////////////////////////////////////// -->
                                    @endif
                                            </td> 
                                          <td style="text-align: right;">{{ $orden->total}} Bs.</td> 
                                          <td style="text-align: center;">{{ $orden->estado==2?'Consolidado':'En proceso'}}</td> 
                                          <td > 
                                        
                                          <a class="badge badge-success" href="{{route('verordendoc',['idorden' =>$orden->idorden])}}" style="cursor: pointer;width: 100%;  color: white;"> <i class="fa fa fa-eye"></i> Ver documento</a> 
                                      @if ($orden->estado==1)  
                                            <a class="badge badge-danger"  style="cursor: pointer;width: 100%;  color: white;" onclick="event.preventDefault();
                                                     mensa(()=>{document.getElementById('desactivarrecp{{$orden->idorden}}').submit()});
                                                     "> <i class="fa fa fa-remove"></i> Eliminar</a>
                                            <a class="badge badge-primary"  style="cursor: pointer;width: 100%;  color: white;" onclick="event.preventDefault();
                                                     mensa2(()=>{document.getElementById('consolidar{{$orden->idorden}}').submit()});
                                                     "> <i class="fa fa fa-check"></i> Consolidar</a>
                                 
                                        <form id="desactivarrecp{{$orden->idorden}}" action="{{ route('ordendesactivar') }}" method="POST" class="d-none">
                                          @csrf
                                            <input  name="menu" type="hidden" value="{{session('menu')}}">
                                            <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                            <input  name="idorden" type="hidden" value="{{$orden->idorden}}">  
                                       </form>   
                                       <form id="consolidar{{$orden->idorden}}" action="{{ route('consolidarorden') }}" method="POST" class="d-none">
                                          @csrf
                                            <input  name="menu" type="hidden" value="{{session('menu')}}">
                                            <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                            <input  name="idorden" type="hidden" value="{{$orden->idorden}}">  
                                       </form>  
                                       @endif


                                        </td>
                                        </tr>
                                        <tr style="height: 15px;"></tr>
                                      @php
                                      $pos++;
                                      @endphp
                                      @endforeach 
                                      </tbody>
                                    </table>
                                    @if ($recepciones->lastPage()>1)
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center;"> 
                                       @if ($recepciones->currentPage()-1>0)
                                       <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$recepciones->currentPage()-1]) }}"  class="btn btn-light"> Anterior </a>
                                      </div>
                                        @endif
                                                  <div class="btn-group mb-15"> 
                                                      @for ($i = 1; $i <= $recepciones->lastPage(); $i++) 
                                                          @if ($recepciones->currentPage()==$i)
                                                          <a class="btn btn-success ">{{$i}}</a>
                                                          @else
                                                          <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$i]) }}"  class="btn btn-light ">{{$i}}</a>
                                                          @endif 
                                                      @endfor
                                                  </div>
                                      
                                      @if ($recepciones->currentPage()+1<=$recepciones->lastPage())
                                      <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$recepciones->currentPage()+1]) }}"  class="btn btn-light"> Siguiente </a>
                                      </div> 
                                        @endif
                                    </div>
                                    @endif
                                   
                                     
                          </div> 
                  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>
<script>
  function addobs(s){
console.log($(s).text());
var $edit = $("#faddobs");

var curValue = $edit.val();

var newValue = curValue.length>0?curValue+ "\r\n"+$(s).text():$(s).text();

$edit.val(newValue);
  }
</script>