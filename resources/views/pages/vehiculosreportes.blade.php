<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
                <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="title">
                          <h4>Reportes Vehículo</h4>
                        </div> 
                      </div> 
                </div>
			  	</div>
          <!-- //////////////////// -->
          <div class="row clearfix">
					 

              <div class="col-lg-6 col-md-6 col-sm-12 pl-30 d-flex align-items-stretch">
                    <div class="da-card row">
                                      <div class="col-md-6">
                                      <div class="da-card-photo">
                                        <img src="src/images/pdfg2.jpg" alt="">
                                              <div class="da-overlay">
                                                    <div class="da-social">
                                                      <ul class="clearfix"> 
                                                        <li><a href="{{route('reporteve')}}"><i class="fa fa-print"></i></a></li> 
                                                      </ul>
                                                    </div>
                                              </div>
                                      </div>
                                      </div>
                                      <div class="col-md-6" style="background-color: #0b132b;color: white;">
                                      <div class="da-card-content" style="background-color: #0b132b;color: white;">
                                  <h5 class="h5 mb-10" style="color: white;">Reporte vehículo</h5>
                                  <p class="mb-0">Listado del registro de vehículo en el sistema.</p>
                                </div>
                                      </div>
                                
                    </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 pl-30 d-flex align-items-stretch">
                    <div class="da-card row">
                                      <div class="col-md-6">
                                        <div class="da-card-photo">
                                          <img src="src/images/pdf4p.jpg" alt="">
                                                <div class="da-overlay">
                                                      <div class="da-social">
                                                        <ul class="clearfix"> 
                                                          <li><a href="{{route('reporterecepall')}}"><i class="fa fa-print"></i></a></li> 
                                                        </ul>
                                                      </div>
                                                </div>
                                        </div>
                                      </div>

                                      <div class="col-md-6" style="background-color: #0b132b;color: white;">
                                        <div class="da-card-content" style="background-color: #0b132b;color: white;">
                                          <h5 class="h5 mb-10" style="color: white;">Reporte vehículo recepcionados</h5>
                                          <p class="mb-0">Listado del registro de vehículo recepcionados en el sistema.</p>
                                        </div>
                                      </div>
                                
                    </div>
              </div>

              <div class="col-lg-6 col-md-6 col-sm-12 pl-30 pt-30 d-flex align-items-stretch">
                    <div class="da-card row">
                                     <div class="col-md-6">
                                     <div class="da-card-photo">
                                        <img src="src/images/pdf3p.jpg" alt="">
                                              <div class="da-overlay">
                                                    <div class="da-social">
                                                      <ul class="clearfix" style="text-align: center;"> 
                                                        
                                                        <form   action="{{ route('reporterecep') }}" method="GET" >
                                                        @csrf 
                                                        <li style="width: 100%;padding-top: 10px;"><input class="form-control" type="date" name="daterange" value="@php echo date('Y-m-d');@endphp" required></li> 
                                                        <li style="width: 100%;padding-top: 10px;"><button type="submit" class="btn btn-light btn-block"><i class="fa fa-print"></i> Generar pdf</button></li> 
                                                    </form>  
                                                      </ul>
                                                    </div>
                                              </div>
                                      </div>
                                     </div>

                                     <div class="col-md-6" style="background-color: #0b132b;color: white;">
                                        <div class="da-card-content" style="background-color: #0b132b;color: white;">
                                          <h5 class="h5 mb-10" style="color: white;">Reporte vehículo recepcionados por fecha</h5>
                                          <p class="mb-0">Listado del registro de vehículo recepcionados en el sistema.</p>
                                        </div>
                                     </div>
                                
                    </div>
              </div>



				  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>