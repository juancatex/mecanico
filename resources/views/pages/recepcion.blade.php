<?php 
                              
                            
                              use App\Http\Controllers\VehiculoController;  
                              use App\Http\Controllers\RecepcionController;  
                              $vehiculoscombo = VehiculoController::vehiculoscombo(); 
                                
                                                                                                                                                                  
                              ?>
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Recepción Vehicular</h4>
							</div>
							  
						</div>
						<div class="col-md-6 col-sm-12 text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevarecep"><i class="fa fa-plus-square"></i> Nueva Recepción</button>
            <!-- /////////////////////// -->
                  <div class="modal fade" id="nuevarecep" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog modal-lg modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Nueva Recepción</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div> 
                           <div class="modal-body" style="text-align: left;"> 

                               <!-- ///////////////////////////////////////////////////////////////////////////////////////            -->
                               <div class="wizard-content">
                                <form class="tab-wizard wizard-circle wizard vertical" action="{{ route('regrecepcion') }}" method="POST">
                                  @csrf
                                <input  name="menu" type="hidden" value="{{session('menu')}}">
                                <input  name="submenu" type="hidden" value="{{session('submenu')}}">  
                                  <h5>Diagnostico previo</h5>
                                  <section>
                                    <div class="row"> 
                                                <div class="col-md-12">
                                                    <div class="form-group">
                                                      <label>Vehiculo :</label>
                                                      <select class="form-control" name="idv"> 
                                                      @foreach ($vehiculoscombo as $vehiculo)
                                                            <option value="{{$vehiculo->idv}}">{{$vehiculo->placa }} - {{$vehiculo->marca }} - {{$vehiculo->modelo }}</option> 
                                                      @endforeach
                                                        </select>
                                                    </div>
                                                </div> 
                                    </div>
                                    <div class="row"> 
                                                          <div class="col-md-12 col-sm-12 row">
                                                                      <label class="weight-600 col-md-12">Mantenimiento correctivo</label>
                                                                      

                                                                      <div class="col-md-6">
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mc1" name="mc1">
                                                                          <label class="custom-control-label" for="mc1">Correcion en el motor</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mc2" name="mc2">
                                                                          <label class="custom-control-label" for="mc2">Arreglo de la suspension</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mc3" name="mc3">
                                                                          <label class="custom-control-label" for="mc3">Cambio de llanta</label>
                                                                        </div>
                                                                      </div>  

                                                                      <div class="col-md-6">
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mc4" name="mc4">
                                                                          <label class="custom-control-label" for="mc4">Reparacion de transmision y direccion</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mc5" name="mc5">
                                                                          <label class="custom-control-label" for="mc5">Revision del sistema de refrigeracion</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mc6" name="mc6">
                                                                          <label class="custom-control-label" for="mc6">Cambio de bateria</label>
                                                                        </div>
                                                                      </div>  
                                                          </div>

                                                          <div class="col-md-12 col-sm-12 row">
                                                                      <label class="weight-600 col-md-12">Mantenimiento Periodico</label>
                                                                      

                                                                      <div class="col-md-6">
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mp1" name="mp1">
                                                                          <label class="custom-control-label" for="mp1">Revision de la presion de los neumaticos</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mp2" name="mp2">
                                                                          <label class="custom-control-label" for="mp2">Revisar el liquido de transmicion</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mp3" name="mp3">
                                                                          <label class="custom-control-label" for="mp3">Liquido de freno</label>
                                                                        </div>
                                                                      </div>  

                                                                      <div class="col-md-6">
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mp4" name="mp4">
                                                                          <label class="custom-control-label" for="mp4">Cambio de bujia</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mp5" name="mp5">
                                                                          <label class="custom-control-label" for="mp5">Cambio de amortiguadores</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mp6" name="mp6">
                                                                          <label class="custom-control-label" for="mp6">Cambio de filtro de combustible</label>
                                                                        </div>
                                                                      </div>  
                                                          </div>
                                                          <div class="col-md-12 col-sm-12 row">
                                                                      <label class="weight-600 col-md-12">Mantenimiento Programado</label>
                                                                      

                                                                      <div class="col-md-12">
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mpr1" name="mpr1">
                                                                          <label class="custom-control-label" for="mpr1">Limpieza tecnica de equipos</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mpr2" name="mpr2">
                                                                          <label class="custom-control-label" for="mpr2">Sustitucion de elementos a desgaste (Rodetes, Rodamientos, Cojinetes, Culata)</label>
                                                                        </div>  
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mpr3" name="mpr3">
                                                                          <label class="custom-control-label" for="mpr3">Kilometraje</label>
                                                                        </div>
                                                                      </div>   
                                                          </div>
                                                          <div class="col-md-12 col-sm-12 row">
                                                                      <label class="weight-600 col-md-12">Mantenimiento Predictivo</label>
                                                                      

                                                                      <div class="col-md-6">
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mpre1" name="mpre1">
                                                                          <label class="custom-control-label" for="mpre1">Cambiar el aceite de motor</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mpre2" name="mpre2">
                                                                          <label class="custom-control-label" for="mpre2">Cambiar el filtro de aire</label>
                                                                        </div>  
                                                                         
                                                                      </div>   
                                                                      <div class="col-md-6">
                                                                         
                                                                         <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mpre3" name="mpre3">
                                                                          <label class="custom-control-label" for="mpre3">Cambiar el filtro de aceite</label>
                                                                        </div> 
                                                                        <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                          <input type="checkbox" class="custom-control-input" id="mpre4" name="mpre4">
                                                                          <label class="custom-control-label" for="mpre4">Presion de llantas</label>
                                                                        </div>
                                                                      </div> 
                                                          </div>
                                      <div class="col-md-12">
                                      <div class="form-group">
                                          <label class="weight-600 col-md-12">Diagnóstico :</label> 
                                          <textarea class="form-control" style="height: auto;" rows="4"  name="diag" placeholder="Ingrese un diagnostico" required></textarea>
                                        </div>
                                      </div>
                                    </div>
 
                                  </section>
                                  <!-- Step 2 -->
                                  <h5>Recepción</h5>
                                  <section>
                                     <!-- ///////////////////////////////////// -->
                                                      <div class="form-group">
                                                        <div class="row">
                                                        <div class="col-md-12 col-sm-12" >
                                                        <div  style="    background-image: url(src/images/auto2.jpg);
                                                                  background-position: center;
                                                                  background-repeat: no-repeat;
                                                                  background-size: contain;
                                                                  height: 357px;    
                                                                  border: 1px solid gray;
                                                                  border-radius: 15px;
                                                                  margin-bottom: 18px;
                                                                  width: 76%;
                                                                  margin-left: auto;
                                                                  margin-right: auto;">

                                                            </div>
                                                            </div>
                                                          <div class="col-md-6 col-sm-12">
                                                             
                                                               <div class="col-md-12 col-sm-12">
                                                                      <label class="weight-600">Recepcion entrante Vehicular</label>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev1"  name="rev1">
                                                                        <label class="custom-control-label" for="rev1">Luces principales</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev2"  name="rev2">
                                                                        <label class="custom-control-label" for="rev2">Luces stop - guiñadores</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev3"  name="rev3">
                                                                        <label class="custom-control-label" for="rev3">Antena de radio</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev4"  name="rev4">
                                                                        <label class="custom-control-label" for="rev4">Limpia parabrisas</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev5"  name="rev5">
                                                                        <label class="custom-control-label" for="rev5">Espejo lateral derecho</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev6"  name="rev6">
                                                                        <label class="custom-control-label" for="rev6">Espejo lateral izquierdo</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev7"  name="rev7">
                                                                        <label class="custom-control-label" for="rev7">Vidrios laterales</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev8"  name="rev8">
                                                                        <label class="custom-control-label" for="rev8">Tapon de gasolina</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="rev9"  name="rev9">
                                                                        <label class="custom-control-label" for="rev9">Placas delantera y trasera</label>
                                                                      </div>
                                                                </div>
                                                                <div class="col-md-12 col-sm-12">
                                                                      <label class="weight-600">Otros</label>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="otro1"  name="otro1">
                                                                        <label class="custom-control-label" for="otro1">Soat</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="otro2"  name="otro2">
                                                                        <label class="custom-control-label" for="otro2">Inspeccion tecnica</label>
                                                                      </div>
                                                                        
                                                                </div>

                                                                <div class="col-md-12 col-sm-12">
                                                                <label class="weight-600">Observación</label> 
                                                                  <textarea class="form-control" style="height: auto;" rows="6"  name="obsrec" placeholder="Ingrese una observacion" ></textarea>
                                                                 </div>

                                                           </div>
                                                          
                                                          
                                                          
                                                           <div class="col-md-6 col-sm-12">
                                                                <div class="col-md-12 col-sm-12">
                                                                      <label class="weight-600">Exteriores</label>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="ul" name="ul">
                                                                        <label class="custom-control-label" for="ul">Unidad de luces</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="an" name="an">
                                                                        <label class="custom-control-label" for="an">Antenas</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="el" name="el">
                                                                        <label class="custom-control-label" for="el">Parasoles</label>
                                                                      </div> 

                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="llan" name="llan">
                                                                        <label class="custom-control-label" for="llan">Llanta de auxilio</label>
                                                                      </div> 
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="bx" name="bx">
                                                                        <label class="custom-control-label" for="bx">Bocinas claxon</label>
                                                                      </div> 
                                                                      
                                                                </div>
                                                                <div class="col-md-12 col-sm-12">
                                                                      <label class="weight-600">Accesorios</label>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="ga"  name="ga">
                                                                        <label class="custom-control-label" for="ga">Gata</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="llar"   name="llar">
                                                                        <label class="custom-control-label" for="llar">Llave de rueda</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="llac"  name="llac">
                                                                        <label class="custom-control-label" for="llac">Llave de contacto</label>
                                                                      </div> 

                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="eh"  name="eh">
                                                                        <label class="custom-control-label" for="eh">Estuche herramienta</label>
                                                                      </div> 
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="ts"  name="ts">
                                                                        <label class="custom-control-label" for="ts">Triangulo de seguridad</label>
                                                                      </div> 
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="llax"  name="llax">
                                                                        <label class="custom-control-label" for="llax">Extinguidor</label>
                                                                      </div> 
                                                                </div>
                                                                <div class="col-md-12 col-sm-12">
                                                                      <label class="weight-600">Interiores</label>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="ra" name="ra">
                                                                        <label class="custom-control-label" for="ra">Radio</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="it" name="it">
                                                                        <label class="custom-control-label" for="it">Instrumentos de tablero</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="en" name="en">
                                                                        <label class="custom-control-label" for="en">Encendedor</label>
                                                                      </div> 

                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="ca" name="ca">
                                                                        <label class="custom-control-label" for="ca">Calefaccion</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="ce" name="ce">
                                                                        <label class="custom-control-label" for="ce">Cenicero</label>
                                                                      </div>
                                                                      <div class="custom-control custom-checkbox mb-5" style="font-size: 12px;">
                                                                        <input type="checkbox" class="custom-control-input" id="to" name="to">
                                                                        <label class="custom-control-label" for="to">Tapetes</label>
                                                                      </div>
                                                                </div>
                                                                </div> 
                                                        </div>
                                                      </div>
                                     <!-- ///////////////////////////////////// -->
                                  </section> 
                                </form>
                              </div>
                               <!-- ///////////////////////////////////////////////////////////////////////////////////////            -->
                                       
        
                           </div> 
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
           	</div>
					</div>
				</div>
          <!-- //////////////////// -->
                    <div class="pd-20 card-box mb-30">
                          <div class="clearfix mb-20">
                                      <div class="pull-left">
                                        <h4 class="text-blue h4">Listado de recepciones</h4>
                                       </div>
                                   <form  action="{{ route('busquedarecep') }}" method="POST" >
                                                 @csrf   
                                          <input  name="menu" type="hidden" value="{{session('menu')}}">
                                        <input  name="submenu" type="hidden" value="{{session('submenu')}}">   
                                            <div class="col-md-12 col-sm-12 text-right">
                                            <div class="row" style="display: inline-flex;">  
                                                    <div style="margin-right: 10px;">
                                                            <input class="form-control" type="text" name="buscar" placeholder="Ingrese el texto a buscar ">
                                                     </div>    
                                                    <button type="submit" class="btn btn-info"><i class="fa fa-plus-square"></i> Buscar</button> 
                                            </div>
                                      </div>
                                        </form>               
                          </div>
                          <div class="table-responsive">
                          <?php  
                                if (session('buscarrecep')){
                                  $recepciones = RecepcionController::busrecepwhere(session('buscarrecep')); 
                                }else{
                                  if (session('pos')){
                                    $recepciones = RecepcionController::listarecepciones(session('pos')); 
                                  }else{
                                    $recepciones = RecepcionController::listarecepciones(1); 
                                  } 
                                } 
                                                                                                                                                                  
                              ?>
 
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th style="font-size: 12px !important;" scope="col">#</th>
                                          <th style="font-size: 12px !important;min-width: 110px;" scope="col">Fecha registro</th>
                                          <th style="font-size: 12px !important;" scope="col">Vehículo</th>

                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento correctivo</th>  
                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento Periodico</th>  
                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento Programado</th>  
                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Mantenimiento Predictivo</th>  

                                          <th style="font-size: 12px !important;" scope="col">Diagnóstico</th> 

                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Recepcion entrante Vehicular</th>  
                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Exteriores</th>  
                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Accesorios</th>   
                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Interiores</th>   
                                          <th style="font-size: 12px !important;min-width: 150px;" scope="col">Otros</th>   

                                          <th style="font-size: 12px !important;" scope="col">Observación</th>
                                          <th style="font-size: 12px !important;" scope="col">Opciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @php
                                      $pos = 1;
                                      @endphp
                                      @foreach ($recepciones as $recepcionn) 
                                        <tr>
                                          <th style="font-size: 12px !important;" scope="row">{{ $recepcionn->idrec }}</th>
                                          <td style="font-size: 12px !important;    text-align: center;">{{ str_replace(' ',"\n",$recepcionn->created_at)}}</td>
                                          <td style="font-size: 12px !important;">{{ $recepcionn->placa}} - {{ $recepcionn->marca}} - {{ $recepcionn->modelo}} </td>


                                          <td style="font-size: 10px !important;"> 
                                              <table style="width: 100%;"> 
                                                  <tr class="{{ $recepcionn->mc1==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Correcion en el motor</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mc1==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mc2==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Arreglo de la suspension</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mc2==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mc3==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambio de llanta</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mc3==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mc4==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Reparacion de transmision y direccion</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mc4==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mc5==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Revision del sistema de refrigeracion</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mc5==0?'No':'Si'}}</td></tr> 
                                                  <tr class="{{ $recepcionn->mc6==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambio de bateria</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mc6==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td> 
                                        <td style="font-size: 10px !important;"> 
                                              <table style="width: 100%;"> 
                                                  <tr class="{{ $recepcionn->mp1==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Revision de la presion de los neumaticos</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mp1==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mp2==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Revisar el liquido de transmicion</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mp2==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mp3==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Liquido de freno</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mp3==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mp4==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambio de bujia</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mp4==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mp5==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambio de amortiguadores</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mp5==0?'No':'Si'}}</td></tr> 
                                                  <tr class="{{ $recepcionn->mp6==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambio de filtro de combustible</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mp6==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td> 
                                        <td style="font-size: 10px !important;"> 
                                              <table style="width: 100%;"> 
                                                  <tr class="{{ $recepcionn->mpr1==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Limpieza tecnica de equipos</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mpr1==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mpr2==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Sustitucion de elementos a desgaste (Rodetes, Rodamientos, Cojinetes, Culata)</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mpr2==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mpr3==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Kilometraje</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mpr3==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td> 
                                        <td style="font-size: 10px !important;"> 
                                              <table style="width: 100%;"> 
                                                  <tr class="{{ $recepcionn->mpre1==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambiar el aceite de motor</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mpre1==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mpre2==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambiar el filtro de aire</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mpre2==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mpre3==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cambiar el filtro de aceite</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mpre3==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->mpre4==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Presion de llantas</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->mpre4==0?'No':'Si'}}</td></tr>
                                                </table> 
                                        </td> 


                                          <td style="font-size: 12px !important;">{{ $recepcionn->diag}}</td> 

                                          <td style="font-size: 10px !important;">
                                              <table style="width: 100%;"> 
                                                <tr class="{{ $recepcionn->rev1==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Luces principales</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev1==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->rev2==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Luces stop - guiñadores</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev2==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->rev3==0?' ':'bag'}}"vvv><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Antena de radio</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev3==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->rev4==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Limpia parabrisas</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev4==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->rev5==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Espejo lateral derecho</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev5==0?'No':'Si'}}</td></tr> 
                                                <tr class="{{ $recepcionn->rev6==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Espejo lateral izquierdo</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev6==0?'No':'Si'}}</td></tr> 
                                                <tr class="{{ $recepcionn->rev7==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Vidrios laterales</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev7==0?'No':'Si'}}</td></tr> 
                                                <tr class="{{ $recepcionn->rev8==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Tapon de gasolina</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev8==0?'No':'Si'}}</td></tr> 
                                                <tr class="{{ $recepcionn->rev9==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Placas delantera y trasera</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->rev9==0?'No':'Si'}}</td></tr> 
                                              </table> 
                                        </td> 
                                          <td style="font-size: 10px !important;">
                                              <table style="width: 100%;"> 
                                                <tr class="{{ $recepcionn->ul==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Unidad de luces</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->ul==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->an==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Antenas</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->an==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->el==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Espejo lateral</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->el==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->llan==0?' ':'bag'}}" ><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Llantas (4)</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->llan==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->bx==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Bocinas claxon</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->bx==0?'No':'Si'}}</td></tr> 
                                              </table> 
                                        </td> 

                                          <td style="font-size: 10px !important;"> 
                                              <table style="width: 100%;"> 
                                                  <tr class="{{ $recepcionn->ga==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Gata</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->ga==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->llar==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Llave de rueda</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->llar==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->llac==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Llave de contacto</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->llac==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->eh==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Estuche herramienta</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->eh==0?'No':'Si'}}</td></tr>
                                                  <tr class="{{ $recepcionn->ts==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Triangulo de seguridad</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->ts==0?'No':'Si'}}</td></tr> 
                                                  <tr class="{{ $recepcionn->llax==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Llanta de auxilio</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->llax==0?'No':'Si'}}</td></tr> 
                                                </table> 
                                        </td>  

                                        <td style="font-size: 10px !important;">
                                            <table style="width: 100%;"> 
                                                <tr class="{{ $recepcionn->ra==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Radio</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->ra==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->it==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Instrumentos tablero</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->it==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->en==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Encendedor</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->en==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->ca==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Calefaccion</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->ca==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->ce==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Cenicero</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->ce==0?'No':'Si'}}</td></tr> 
                                                <tr class="{{ $recepcionn->to==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Tapetes</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->to==0?'No':'Si'}}</td></tr> 
                                            </table>
                                        </td>
                                        <td style="font-size: 10px !important;">
                                            <table style="width: 100%;"> 
                                                <tr class="{{ $recepcionn->otro1==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Soat</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->otro1==0?'No':'Si'}}</td></tr>
                                                <tr class="{{ $recepcionn->otro2==0?' ':'bag'}}"><td style="border-bottom: 1px solid gray;font-size: 10px !important;padding:0px;padding-right: 5px;">Inspeccion tecnica</td><td style="font-size: 10px !important;padding:0px">{{ $recepcionn->otro2==0?'No':'Si'}}</td></tr>
                                            </table>
                                        </td>

                                          <td style="font-size: 12px !important;">{{ $recepcionn->obsrec}}</td> 
                                          <td style="font-size: 12px !important;"> <a class="badge badge-danger"  style="cursor: pointer;width: 100%;  color: white;" onclick="event.preventDefault();
                                                     mensa(()=>{document.getElementById('desactivarrecp{{$recepcionn->idrec}}').submit()});
                                                     "> <i class="fa fa fa-remove"></i> Eliminar</a>
                                       
                                        <form id="desactivarrecp{{$recepcionn->idrec}}" action="{{ route('recepdesactivar') }}" method="POST" class="d-none">
                                          @csrf
                                            <input  name="menu" type="hidden" value="{{session('menu')}}">
                                            <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                            <input  name="idrec" type="hidden" value="{{$recepcionn->idrec}}">  
                                       </form>   
                                        
                                        </td>
                                        </tr>
                                        <tr style="height: 15px;"></tr>
                                      @php
                                      $pos++;
                                      @endphp
                                      @endforeach 
                                      </tbody>
                                    </table>
                                    @if ($recepciones->lastPage()>1)
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center;"> 
                                       @if ($recepciones->currentPage()-1>0)
                                       <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$recepciones->currentPage()-1]) }}"  class="btn btn-light"> Anterior </a>
                                      </div>
                                        @endif
                                                  <div class="btn-group mb-15"> 
                                                      @for ($i = 1; $i <= $recepciones->lastPage(); $i++) 
                                                          @if ($recepciones->currentPage()==$i)
                                                          <a class="btn btn-success ">{{$i}}</a>
                                                          @else
                                                          <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$i]) }}"  class="btn btn-light ">{{$i}}</a>
                                                          @endif 
                                                      @endfor
                                                  </div>
                                      
                                      @if ($recepciones->currentPage()+1<=$recepciones->lastPage())
                                      <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$recepciones->currentPage()+1]) }}"  class="btn btn-light"> Siguiente </a>
                                      </div> 
                                        @endif
                                    </div>
                                    @endif
                                   
                                     
                          </div> 
                  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>