<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Clientes</h4>
							</div>
							  
						</div>
						<div class="col-md-6 col-sm-12 text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevocliente"><i class="fa fa-plus-square"></i> Nuevo cliente</button>
            <!-- /////////////////////// -->
                  <div class="modal fade" id="nuevocliente" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Nuevo cliente</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                         <form  action="{{ route('regclientes') }}" method="POST" enctype="multipart/form-data">
                         @csrf
                         <input  name="menu" type="hidden" value="{{session('menu')}}">
                         <input  name="submenu" type="hidden" value="{{session('submenu')}}">        
                                  <div class="modal-body" style="text-align: left;"> 

                                        <div class="form-group">
                                            <label>Fotografia :</label>
                                              <div id="previewiamge" class="mb-15" style="text-align: center;"></div> 
                                              <input type="file" class="form-control" accept="image/*" capture="camera" name="imagecel" onchange="ffoto(this.files[0])">
                                        </div>
                                        <div class="form-group">
                                          <label>Nombres :</label>
                                          <input class="form-control" name="nombrecli" type="text" placeholder="Ingrese los nombres" required autofocus>
                                        </div>
                                      
                                        <div class="form-group">
                                          <label>Apellidos :</label>
                                          <input class="form-control" name="apcli" type="text" placeholder="Ingrese los apellidos" required>
                                        </div>
                                      
                                        <div class="form-group">
                                          <label>Email :</label>
                                          <input class="form-control" name="emailcli" placeholder="Ingrese el email" type="email" required>
                                        </div>

                                        <div class="form-group">
                                          <label>Teléfonos :</label>
                                          <input class="form-control" name="telcli" placeholder="Ingrese el Teléfonos" max="99999999" type="number" required>
                                        </div>
                                        
                                        <div class="form-group">
                                          <label>C.I :</label>
                                          <input class="form-control" name="ci" placeholder="Ingrese su C.I" max="9999999999" min="0" type="number" required>
                                        </div>
                                        <div class="form-group">
                                          <label>Dirección :</label> 
                                          <textarea class="form-control" style="height: auto;" rows="3" name="dircli"  placeholder="Ingrese la dirección" required></textarea>
                                        </div>
        
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Registrar</button>
                                  </div>
                          </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
           	</div>
					</div>
				</div>
          <!-- //////////////////// -->
                    <div class="pd-20 card-box mb-30">
                          <div class="clearfix mb-20">
                                      <div class="pull-left">
                                        <h4 class="text-blue h4">Listado de clientes</h4>
                                       </div>
                                   <form  action="{{ route('busquedacli') }}" method="POST" >
                                                 @csrf   
                                          <input  name="menu" type="hidden" value="{{session('menu')}}">
                                        <input  name="submenu" type="hidden" value="{{session('submenu')}}">   
                                            <div class="col-md-12 col-sm-12 text-right">
                                            <div class="row" style="display: inline-flex;">  
                                                    <div style="margin-right: 10px;">
                                                            <input class="form-control" type="text" name="buscar" placeholder="Ingrese el texto a buscar ">
                                                     </div>    
                                                    <button type="submit" class="btn btn-info"><i class="fa fa-plus-square"></i> Buscar</button> 
                                            </div>
                                      </div>
                                        </form>               
                          </div>
                          <div class="table-responsive">
                          <?php 
                              use App\Http\Controllers\ClienteController;  
                                if (session('buscarcli')){
                                  $clientes = ClienteController::listarclienteswhere(session('buscarcli')); 
                                }else{
                                  if (session('pos')){
                                    $clientes = ClienteController::listarclientesp(session('pos')); 
                                  }else{
                                    $clientes = ClienteController::listarclientesp(1); 
                                  } 
                                } 
                                                                                                                                                                  
                              ?>
 
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th style="min-width: 110px;" scope="col">Fecha registro</th>
                                          <th scope="col" style="width: 200px;">Nombre</th>
                                          <th scope="col">Apellidos</th>
                                          <th scope="col">Email</th>
                                          <th scope="col">Dirección</th>
                                          <th scope="col">Teléfono</th>
                                          <th scope="col">C.I</th>
                                          <th scope="col">Opciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @php
                                      $pos = 1;
                                      @endphp
                                      @foreach ($clientes as $cliente) 
                                        <tr>
                                          <th scope="row">{{ $cliente->idcli }}</th>
                                          <td style="text-align: center;">{{ str_replace(' ',"\n",$cliente->created_at)}}</td>
                                          
                                          @if ($cliente->foto)
                                          <td class="row">
                                          <div class="col-md-6 my-auto">
                                          <img src="{{ url('storage/'.$cliente->foto) }}" alt="" title="" />  
                                          </div>
                                          <div class="col-md-6 my-auto">{{ $cliente->nomcli}}</div> 
                                          </td>
                                          @else 
                                          <td>
                                          <div class="col-md-12 my-auto">{{ $cliente->nomcli}}</div> 
                                          </td>
                                           @endif 
                                          
                                        
                                          <td>{{ $cliente->apcli}}</td>
                                          <td>{{ $cliente->emailcli}}</td>
                                          <td>{{ $cliente->dircli}}</td>
                                          <td>{{ $cliente->telcli}}</td>
                                          <td>{{ $cliente->ci}}</td> 
                                          <td><span class="badge badge-warning" style="cursor: pointer;width: 100%; " data-toggle="modal" data-target="#cliente{{$cliente->idcli}}"> <i class="fa fa fa-edit"></i> Editar</span>
                                       
                                          <a class="badge badge-danger"  style="cursor: pointer;width: 100%;  color: white;" onclick="event.preventDefault();
                                                     mensa(()=>{document.getElementById('desactivar{{$cliente->idcli}}').submit()});
                                                     "> <i class="fa fa fa-remove"></i> Eliminar</a>
                                       
                                        <form id="desactivar{{$cliente->idcli}}" action="{{ route('clientesdesactivar') }}" method="POST" class="d-none">
                                          @csrf
                                            <input  name="menu" type="hidden" value="{{session('menu')}}">
                                            <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                            <input  name="idcli" type="hidden" value="{{$cliente->idcli}}">  
                                       </form>  
                                          
                                       
                                          <!-- /////////////////////////////////////////////////    -->
                                                      <div class="modal fade" id="cliente{{$cliente->idcli}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog  modal-dialog-centered">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h4 class="modal-title" id="myLargeModalLabel">Editar cliente</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                          </div>
                                                        <form  action="{{ route('editclientes') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <input  name="menu" type="hidden" value="{{session('menu')}}">
                                                        <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                                        <input  name="idcli" type="hidden" value="{{$cliente->idcli}}">      
                                                                  <div class="modal-body" style="text-align: left;"> 
                                                                          <div class="form-group">
                                                                              <label>Fotografia :</label>
                                                                                <div id="previewiamgedit" class="mb-15" style="text-align: center;"> 
                                                                               @if ($cliente->foto)
                                                                               <img src="{{ url('storage/'.$cliente->foto) }}" alt="" style="width: 170px;
                                                                                      border: 4px solid gray;
                                                                                      height: 170px;
                                                                                      object-fit: cover;
                                                                                      border-radius: 50%;" >
                                                                                @endif 
                                                                               </div> 
                                                                                <input type="file" class="form-control" accept="image/*" capture="camera" name="imageceledit"  onchange="ff(this.files[0])">
                                                                               
                                                                          </div>
                                                                        <div class="form-group">
                                                                          <label>Nombres :</label>
                                                                          <input class="form-control" value="{{ $cliente->nomcli}}" name="nombrecli" type="text" placeholder="Ingrese los nombres" required>
                                                                        </div> 
                                                                        <div class="form-group">
                                                                          <label>Apellidos :</label>
                                                                          <input class="form-control" value="{{ $cliente->apcli}}" name="apcli" type="text" placeholder="Ingrese los apellidos" required>
                                                                        </div> 
                                                                        <div class="form-group">
                                                                          <label>Email :</label>
                                                                          <input class="form-control" value="{{ $cliente->emailcli}}" name="emailcli" type="text" placeholder="Ingrese el email" required>
                                                                        </div> 
                                                                        <div class="form-group">
                                                                          <label>Telefono :</label>
                                                                          <input class="form-control" value="{{ $cliente->telcli}}" name="telcli" placeholder="Ingrese el telefono" type="number" required>
                                                                        </div> 
                                                                        <div class="form-group">
                                                                          <label>C.i :</label>
                                                                          <input class="form-control" value="{{ $cliente->ci}}" name="ci" placeholder="Ingrese el C.I." type="number" required>
                                                                        </div> 
                                                                        
                                                                        <div class="form-group">
                                                                          <label>Dirección :</label> 
                                                                          <textarea class="form-control" style="height: auto;" rows="3"  name="dircli" placeholder="Ingrese la dirrección" required>{{ $cliente->dircli}}</textarea>
                                                                        </div>
                                        
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-primary">Modificar</button>
                                                                  </div>
                                                          </form>
                                                        </div>
                                                      </div>
                                                    </div>
                                        <!-- /////////////////////////////////////////////////    -->
                                        </td>
                                        </tr>
                                        <tr style="height: 15px;"></tr>
                                      @php
                                      $pos++;
                                      @endphp
                                      @endforeach 
                                      </tbody>
                                    </table>
                                    @if ($clientes->lastPage()>1)
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center;"> 
                                       @if ($clientes->currentPage()-1>0)
                                       <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$clientes->currentPage()-1]) }}"  class="btn btn-light"> Anterior </a>
                                      </div>
                                        @endif
                                                  <div class="btn-group mb-15"> 
                                                      @for ($i = 1; $i <= $clientes->lastPage(); $i++) 
                                                          @if ($clientes->currentPage()==$i)
                                                          <a class="btn btn-success ">{{$i}}</a>
                                                          @else
                                                          <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$i]) }}"  class="btn btn-light ">{{$i}}</a>
                                                          @endif 
                                                      @endfor
                                                  </div>
                                      
                                      @if ($clientes->currentPage()+1<=$clientes->lastPage())
                                      <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$clientes->currentPage()+1]) }}"  class="btn btn-light"> Siguiente </a>
                                      </div> 
                                        @endif
                                    </div>
                                    @endif
                                   
                                     
                          </div> 
                  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>
