<?php 
                            
                            use App\Http\Controllers\OrdentrabajoController;  
                            use App\Http\Controllers\ReparacionController;  
                            $listaordenesapro = OrdentrabajoController::ordenescombo();                                                                                                                             
                              ?>
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
                <div class="row">
                      <div class="col-md-6 col-sm-12">
                        <div class="title">
                          <h4>Reportes de Pagos</h4>
                        </div> 
                      </div> 
                </div>
			  	</div>
          <!-- //////////////////// -->
          <div class="row clearfix">
					 

              <div class="col-lg-6 col-md-6 col-sm-12 pl-30 d-flex align-items-stretch">
                    <div class="da-card row" row>
                                      <div class="col-md-6">
                                      <div class="da-card-photo">
                                        <img src="src/images/pdf5pp.jpg" alt="">
                                              <div class="da-overlay">
                                                    <div class="da-social">
                                                      <ul class="clearfix"> 
                                                        <li><a href="{{route('reportepagoo')}}"><i class="fa fa-print"></i></a></li> 
                                                      </ul>
                                                    </div>
                                              </div>
                                      </div>
                                      </div>
                                      <div class="col-md-6" style="background-color: #0b132b;color: white;">
                                          <div class="da-card-content" style="background-color: #0b132b;color: white;">
                                          <h5 class="h5 mb-10" style="color: white;">Reporte de Pagos en general</h5>
                                          <p class="mb-0">Listado de pagos de ordenes de servicio en el sistema.</p>
                                        </div>
                                      </div>
                                
                    </div>
              </div>
 
              <div class="col-lg-6 col-md-6 col-sm-12 pl-30 d-flex align-items-stretch">
                    <div class="da-card row">
                                   <div class="col-md-6">
                                      <div class="da-card-photo">
                                            <img src="src/images/pdf7.jpg" alt="">
                                                  <div class="da-overlay">
                                                        <div class="da-social">
                                                          <ul class="clearfix" style="text-align: center;"> 
                                                            
                                                            <form   action="{{ route('reportepagoorden') }}" method="GET" >
                                                            @csrf 
                                                            <li style="width: 100%;padding-top: 10px;">
                                                            <!-- <input class="form-control" type="date" name="daterange" value="@php echo date('Y-m-d');@endphp" required> -->
                                                            <div class="form-group">
                                                            <label>Codigo de orden de trabajo :</label>
                                                            <select class="form-control" name="idorden"> 
                                                              @foreach ($listaordenesapro as $orden)
                                                                  <option value="{{$orden->idorden}}">{{$orden->placa}} : {{$orden->marca}} - {{$orden->modelo}}</option> 
                                                              @endforeach
                                                            </select>
                                                          </div> 
                                                          </li> 
                                                            <li style="width: 100%;padding-top: 10px;"><button type="submit" class="btn btn-light btn-block"><i class="fa fa-print"></i> Generar pdf</button></li> 
                                                        </form>  
                                                          </ul>
                                                        </div>
                                                  </div>
                                          </div>
                                   </div>
                                   <div class="col-md-6" style="background-color: #0b132b;color: white;">
                                        <div class="da-card-content" style="background-color: #0b132b;color: white;">
                                            <h5 class="h5 mb-10" style="color: white;">Reporte de pagos segun Orden de Trabajo</h5>
                                            <p class="mb-0">Listado de pagos de acuerdo al codigo de orden asignado por el sistema.</p>
                                          </div>
                                   </div>
                                
                    </div>
              </div>



				  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>