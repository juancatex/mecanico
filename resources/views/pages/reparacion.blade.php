<?php 
                            
                            use App\Http\Controllers\OrdentrabajoController;  
                            use App\Http\Controllers\ReparacionController;  
                            $listaordenesapro = OrdentrabajoController::ordenescombo();                                                                                                                             
                              ?>
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Reparaciones</h4>
							</div>
							  
						</div>
						<div class="col-md-6 col-sm-12 text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevareapara"><i class="fa fa-plus-square"></i> Nuevo Registro</button>
            <!-- /////////////////////// -->
                  <div class="modal fade" id="nuevareapara" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Nueva Reparacion</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                         <form  action="{{ route('regreparacion') }}" method="POST" >
                         @csrf
                         <input  name="menu" type="hidden" value="{{session('menu')}}">
                         <input  name="submenu" type="hidden" value="{{session('submenu')}}">        
                                  <div class="modal-body" style="text-align: left;"> 

                                        <div class="form-group">
                                          <label>Codigo de orden de trabajo :</label>
                                          <select class="form-control" name="idorden"> 
                                            @foreach ($listaordenesapro as $orden)
                                                <option value="{{$orden->idorden}}">COD: {{ str_pad($orden->idorden,6,"0", STR_PAD_LEFT)}}</option> 
                                            @endforeach
                                           </select>
                                        </div>  
                                        <div class="form-group">
                                          <label>Detalle :</label> 
                                          <textarea class="form-control" style="height: auto;" rows="4" name="detalle"  placeholder="Ingrese detalle de lo trabajado" ></textarea>
                                        </div>
        
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Registrar</button>
                                  </div>
                          </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
           	</div>
					</div>
				</div>
          <!-- //////////////////// -->
                    <div class="pd-20 card-box mb-30">
                          <div class="clearfix mb-20">
                                      <div class="pull-left">
                                        <h4 class="text-blue h4">Listado de Reparaciónes</h4>
                                       </div> 
                          </div>
                          <div class="table-responsive">
                          <?php  
                                 
                                  if (session('pos')){
                                    $reparaciones = ReparacionController::reparaciones(session('pos')); 
                                  }else{
                                    $reparaciones = ReparacionController::reparaciones(1); 
                                  } 
                                                                                                                                                            
                              ?>
 
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Codigo de orden de trabajo</th> 
                                          <th scope="col">Reparacion Cumplidas</th>
                                          <th scope="col">Opciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @php
                                      $pos = 1;
                                      @endphp
                                      @foreach ($reparaciones as $vehi) 
                                        <tr>
                                          <th scope="row">{{ $pos }}</th>
                                          <td style="text-align: center;">{{ str_pad($vehi->idorden,6,"0", STR_PAD_LEFT)}}</td>  
                                          <td>{!! nl2br(e($vehi->detalle)) !!}</td>  
                                          <td><span class="badge badge-warning" style="cursor: pointer;width: 100%; " data-toggle="modal" data-target="#vehiculosm{{$vehi->idrepa}}"> <i class="fa fa fa-edit"></i> Editar</span>
                                       
                                          <a class="badge badge-danger"  style="cursor: pointer;width: 100%;  color: white;" onclick="event.preventDefault();
                                                     mensa(()=>{document.getElementById('desactivarvehi{{$vehi->idrepa}}').submit()});
                                                     "> <i class="fa fa fa-remove"></i> Eliminar</a>
                                       
                                        <form id="desactivarvehi{{$vehi->idrepa}}" action="{{ route('repadesactivar') }}" method="POST" class="d-none">
                                          @csrf
                                            <input  name="menu" type="hidden" value="{{session('menu')}}">
                                            <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                            <input  name="idrepa" type="hidden" value="{{$vehi->idrepa}}">  
                                       </form>  
                                          
                                       
                                          <!-- /////////////////////////////////////////////////    -->
                                                      <div class="modal fade" id="vehiculosm{{$vehi->idrepa}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog  modal-dialog-centered">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h4 class="modal-title" id="myLargeModalLabel">Editar Reparacion</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                          </div>
                                                        <form  action="{{ route('editree') }}" method="POST" >
                                                        @csrf
                                                        <input  name="menu" type="hidden" value="{{session('menu')}}">
                                                        <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                                        <input  name="idrepa" type="hidden" value="{{$vehi->idrepa}}">      
                                                                  <div class="modal-body" style="text-align: left;"> 
                                                                    
                                                                    <div class="form-group">
                                                                      <label>Detalle :</label> 
                                                                      <textarea class="form-control" style="height: auto;" rows="4" name="detalle"  placeholder="Ingrese detalle de lo trabajado" >{{ $vehi->detalle}}</textarea>
                                                                    </div>
                                                                        
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-primary">Modificar</button>
                                                                  </div>
                                                          </form>
                                                        </div>
                                                      </div>
                                                    </div>
                                        <!-- /////////////////////////////////////////////////    -->
                                        </td>
                                        </tr>
                                        <tr style="height: 15px;"></tr>
                                      @php
                                      $pos++;
                                      @endphp
                                      @endforeach 
                                      </tbody>
                                    </table>
                                    @if ($reparaciones->lastPage()>1)
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center;"> 
                                       @if ($reparaciones->currentPage()-1>0)
                                       <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$reparaciones->currentPage()-1]) }}"  class="btn btn-light"> Anterior </a>
                                      </div>
                                        @endif
                                                  <div class="btn-group mb-15"> 
                                                      @for ($i = 1; $i <= $reparaciones->lastPage(); $i++) 
                                                          @if ($reparaciones->currentPage()==$i)
                                                          <a class="btn btn-success ">{{$i}}</a>
                                                          @else
                                                          <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$i]) }}"  class="btn btn-light ">{{$i}}</a>
                                                          @endif 
                                                      @endfor
                                                  </div>
                                      
                                      @if ($reparaciones->currentPage()+1<=$reparaciones->lastPage())
                                      <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$reparaciones->currentPage()+1]) }}"  class="btn btn-light"> Siguiente </a>
                                      </div> 
                                        @endif
                                    </div>
                                    @endif
                                   
                                     
                          </div> 
                  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>
