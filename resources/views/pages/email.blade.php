<?php 
                            
                            use App\Http\Controllers\OrdentrabajoController;     
                            $listaordenesapro = OrdentrabajoController::ordenescombo();                                                                                                                             
                              ?>
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Notificaciones</h4>
							</div>
							  
						</div>
						 
					</div>
				</div>
          <!-- //////////////////// -->
                    <div class="pd-20 card-box mb-30">
                          <div class="clearfix mb-20">
                                      <div class="pull-left">
                                        <h4 class="text-blue h4">Listado de clientes con order de trabajo</h4>
                                       </div> 
                          </div>
                          <div class="table-responsive">
                          <?php  
                                 
                                  if (session('pos')){
                                    $rdenescli = OrdentrabajoController::listaordenesclientes(session('pos')); 
                                  }else{
                                    $rdenescli = OrdentrabajoController::listaordenesclientes(1); 
                                  } 
                                                                                                                                                            
                              ?>
 
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th scope="col">Codigo de orden de trabajo</th> 
                                          <th scope="col">Vehículo</th> 
                                          <th scope="col">cliente</th> 
                                          <th scope="col">Email</th> 
                                          <th scope="col">Fecha registro</th>
                                          <th scope="col">Opciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @php
                                      $pos = 1;
                                      @endphp
                                      @foreach ($rdenescli as $vehi) 
                                        <tr>
                                          <th scope="row">{{ $pos }}</th>
                                          <td style="text-align: center;">{{ str_pad($vehi->idorden,6,"0", STR_PAD_LEFT)}}</td> 
                                          <td>{{ $vehi->placa}} - {{ $vehi->marca}} - {{ $vehi->modelo}}</td> 
                                          <td>{{ $vehi->nomcli}} {{ $vehi->apcli}}</td>  
                                          <td>{{ $vehi->emailcli}}</td> 
                                          <td>{{ $vehi->created_at}}</td> 
                                          <td>  
                                         <div class="row">
                                         <a class="badge badge-light ml-1"  style="display: inline-flex;cursor: pointer; height: 40px;width: 40px; color: white;border: 0.5px solid #8b8b8b6e;" data-toggle="modal" data-target="#emailsend{{$vehi->idorden}}"> <img src="images/gmail.png" style="margin: auto;"></a>
                                          <a class="badge badge-light ml-1"  style="display: inline-flex;cursor: pointer; height: 40px;width: 40px; color: white;border: 0.5px solid #8b8b8b6e;" data-toggle="modal" data-target="#emailsendex{{$vehi->idorden}}"> <img src="images/hotmail.png" style="margin: auto;"></a>
                                          <a class="badge badge-light ml-1"  style="display: inline-flex;cursor: pointer; height: 40px;width: 40px; color: white;border: 0.5px solid #8b8b8b6e;" data-toggle="modal" data-target="#whatssend{{$vehi->idorden}}"> <img src="images/what.png" style="margin: auto;"></a>
                                         </div>
            <!-- /////////////////////// -->
                  <div class="modal fade" id="emailsend{{$vehi->idorden}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Nueva Notificacion</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                         <form  action="{{ route('envioemail') }}" method="POST" >
                         @csrf
                         <input  name="menu" type="hidden" value="{{session('menu')}}">
                         <input  name="submenu" type="hidden" value="{{session('submenu')}}">        
                         <input  name="email" type="hidden" value="{{$vehi->emailcli}}">        
                                  <div class="modal-body" style="text-align: left;"> 
                            
                                         <div class="form-group">
                                                  <label>Mensaje :</label>
                                                  <input class="form-control"  name="mensaje" placeholder="Ingrese el mensaje" type="text" required>
                                        </div>   
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                  </div>
                          </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
            <!-- /////////////////////// -->
            <div class="modal fade" id="emailsendex{{$vehi->idorden}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Notificacion con adjuntos</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                         <form  action="{{ route('envioemailextracto') }}" method="POST" >
                         @csrf
                         <input  name="menu" type="hidden" value="{{session('menu')}}">
                         <input  name="submenu" type="hidden" value="{{session('submenu')}}">        
                         <input  name="email" type="hidden" value="{{$vehi->emailcli}}">        
                         <input  name="idorden" type="hidden" value="{{$vehi->idorden}}">        
                                  <div class="modal-body" style="text-align: left;"> 
                                 
                                  <div class="form-group">
                                    <label class="weight-600">Adjuntar :</label>
                                          <div class="col-md-12"> 
                                            <div class="custom-control custom-radio mb-5">
                                              <input type="radio" id="customRadio1{{$vehi->idorden}}" name="adjunto" class="custom-control-input" value='extracto' checked>
                                              <label class="custom-control-label" for="customRadio1{{$vehi->idorden}}">Extracto de pagos</label>
                                            </div>
                                            <div class="custom-control custom-radio mb-5">
                                              <input type="radio" id="customRadio2{{$vehi->idorden}}" name="adjunto" class="custom-control-input" value='orden'>
                                              <label class="custom-control-label" for="customRadio2{{$vehi->idorden}}">Orden de trabajo</label>
                                            </div> 
                                         </div>
                                  </div>

                                         <div class="form-group">
                                                  <label>Mensaje :</label>
                                                  <input class="form-control"  name="mensaje" placeholder="Ingrese el mensaje" type="text" required>
                                        </div>   
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                  </div>
                                  </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
            <!-- /////////////////////// -->
            <div class="modal fade" id="whatssend{{$vehi->idorden}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Notificacion via Whatsapp</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                         <!-- <form  action="{{ route('enviowhatsapp') }}" method="POST" >
                         @csrf -->
                         <form id="sendwhats" onsubmit="event.preventDefault(); return sendwhats();">        
                         <input  name="telcli" id="numw" type="hidden" value="{{$vehi->telcli}}">        
                         <input  name="idorden" type="hidden" value="{{$vehi->idorden}}">        
                                  <div class="modal-body" style="text-align: left;"> 
                                         <div class="form-group">
                                                  <label>Mensaje :</label>
                                                  <input class="form-control"  name="mensaje" id="mensajew" placeholder="Ingrese el mensaje" type="text" required>
                                        </div>   
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Enviar</button>
                                  </div>
                                  </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
                                        </td>
                                        </tr>
                                        <tr style="height: 15px;"></tr>
                                      @php
                                      $pos++;
                                      @endphp
                                      @endforeach 
                                      </tbody>
                                    </table>
                                    @if ($rdenescli->lastPage()>1)
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center;"> 
                                       @if ($rdenescli->currentPage()-1>0)
                                       <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$rdenescli->currentPage()-1]) }}"  class="btn btn-light"> Anterior </a>
                                      </div>
                                        @endif
                                                  <div class="btn-group mb-15"> 
                                                      @for ($i = 1; $i <= $rdenescli->lastPage(); $i++) 
                                                          @if ($rdenescli->currentPage()==$i)
                                                          <a class="btn btn-success ">{{$i}}</a>
                                                          @else
                                                          <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$i]) }}"  class="btn btn-light ">{{$i}}</a>
                                                          @endif 
                                                      @endfor
                                                  </div>
                                      
                                      @if ($rdenescli->currentPage()+1<=$rdenescli->lastPage())
                                      <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$rdenescli->currentPage()+1]) }}"  class="btn btn-light"> Siguiente </a>
                                      </div> 
                                        @endif
                                    </div>
                                    @endif
                                   
                                     
                          </div> 
                  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>
<script>
 function sendwhats(){
  $('.modal').modal('hide');
    var win = window.open(`https://wa.me/${$("#numw").val()}?text=${encodeURI($("#mensajew").val())}`, '_blank');
    win.focus(); 
  return false;
} 
</script>