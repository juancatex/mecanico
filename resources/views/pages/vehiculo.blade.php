<?php 
                              use App\Http\Controllers\ClienteController;  
                              use App\Http\Controllers\marcasController;  
                              use App\Http\Controllers\VehiculoController;  
                              use App\Http\Controllers\colorsController;  
                              $clientescombo = ClienteController::listarclientescombo(); 
                              $marcas = marcasController::marcas(); 
                              $colores = colorsController::listarcolorescombo(); 
                                                                                                                                                                  
                              ?>
<div class="main-container">
    <div class="pd-ltr-20 xs-pd-20-10">
          <div class="min-height-200px">

          <div class="page-header">
					<div class="row">
						<div class="col-md-6 col-sm-12">
							<div class="title">
								<h4>Vehiculos</h4>
							</div>
							  
						</div>
						<div class="col-md-6 col-sm-12 text-right">
            <button type="button" class="btn btn-success" data-toggle="modal" data-target="#nuevovehiculo"><i class="fa fa-plus-square"></i> Nuevo vehiculo</button>
            <!-- /////////////////////// -->
                  <div class="modal fade" id="nuevovehiculo" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                      <div class="modal-dialog  modal-dialog-centered">
                        <div class="modal-content">
                          <div class="modal-header">
                            <h4 class="modal-title" id="myLargeModalLabel">Nuevo Vehículo</h4>
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                          </div>
                         <form  action="{{ route('regvehiculo') }}" method="POST" enctype="multipart/form-data">
                         @csrf
                         <input  name="menu" type="hidden" value="{{session('menu')}}">
                         <input  name="submenu" type="hidden" value="{{session('submenu')}}">        
                                  <div class="modal-body" style="text-align: left;"> 

                                        <div class="form-group">
                                          <label>Cliente :</label>
                                          <select class="form-control" name="idcliente"> 
                                            @foreach ($clientescombo as $clientec)
                                                <option value="{{$clientec->idcli}}">{{$clientec->apcli}} {{$clientec->nomcli }}</option> 
                                            @endforeach
                                           </select>
                                        </div>
                                      
                                        <div class="form-group row">
                                          <label class="col-md-12">Marca y Modelo del vehiculo :</label>
                                          
                                           
                                           <div class="col-md-12"><select class="selectpicker"  width="auto" data-size="5" name="idmarca"  data-show-subtext="true" data-live-search="true"> 
                                          @foreach ($marcas as $marca)   
                                                @if ($marca->foto)
                                                      <option   data-content='<img src="{{url($marca->foto)}}" style="width: 30px;"/> {{$marca->marca }} - {{$marca->modelo }}'
                                                    value="{{$marca->idma}}|{{$marca->idmod}}"> 
                                                      {{$marca->marca }} - {{$marca->modelo }}</option> 
                                                @else 
                                                <option   data-content='<img src="images/nologo.jpg" style="width: 30px;"/> {{$marca->marca }} - {{$marca->modelo }}'
                                                    value="{{$marca->idma}}|{{$marca->idmod}}"> 
                                                      {{$marca->marca }} - {{$marca->modelo }}</option>
                                                @endif 
                                          @endforeach
                                           </select> </div>
                                        </div>
                                      



                                        <div class="form-group row">
                                          <label class="col-md-12">Color del Vehículo :</label>
                                           <select class="selectpicker col-md-12" name="idcolor" data-size="5" data-show-subtext="true">  
                                                @foreach ($colores as $color)
                                                <option
                                                data-content='<div style="display: inline-flex;"><span style="min-width: 80px;">{{$color->nombreco }}</span><div style="background-color:{{$color->code}}; margin-left: 10px;height: 28px;width: 134px;border: 1px solid gray;"></div></div>' 
                                                value="{{$color->idco}}">{{$color->nombreco }}</option> 
                                                @endforeach
                                           </select>
                                        </div>

                                        <div class="form-group">
                                          <label>Año del vehículo :</label>
                                          <input class="form-control" name="idanio" placeholder="Ingrese el año" type="number" required>
                                        </div>
                                        
                                        <div class="form-group">
                                          <label>Placa :</label> 
                                          <input class="form-control" name="idplaca" type="text" placeholder="Ingrese la placa del vehiculo" required>
                                        </div>
                                        <div class="form-group">
                                            <label>Fotografia del vehiculo:</label>
                                              <div id="previewiamge" class="mb-15" style="text-align: center;"></div> 
                                              <input type="file" class="form-control" accept="image/*" capture="camera" name="imagecelv" onchange="ffoto(this.files[0])">
                                        </div>
                                        <div class="form-group">
                                          <label>Observación :</label> 
                                          <textarea class="form-control" style="height: auto;" rows="3" name="obsve"  placeholder="Ingrese observaciones" ></textarea>
                                        </div>
        
                                  </div>
                                  <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                    <button type="submit" class="btn btn-primary">Registrar</button>
                                  </div>
                          </form>
                        </div>
                      </div>
                    </div>
            <!-- /////////////////////// -->
           	</div>
					</div>
				</div>
          <!-- //////////////////// -->
                    <div class="pd-20 card-box mb-30">
                          <div class="clearfix mb-20">
                                      <div class="pull-left">
                                        <h4 class="text-blue h4">Listado de Vehículo</h4>
                                       </div>
                                   <form  action="{{ route('busquedave') }}" method="POST" >
                                                 @csrf   
                                          <input  name="menu" type="hidden" value="{{session('menu')}}">
                                        <input  name="submenu" type="hidden" value="{{session('submenu')}}">   
                                            <div class="col-md-12 col-sm-12 text-right">
                                            <div class="row" style="display: inline-flex;">  
                                                    <div style="margin-right: 10px;">
                                                            <input class="form-control" type="text" name="buscar" placeholder="Ingrese el texto a buscar ">
                                                     </div>    
                                                    <button type="submit" class="btn btn-info"><i class="fa fa-plus-square"></i> Buscar</button> 
                                            </div>
                                      </div>
                                        </form>               
                          </div>
                          <div class="table-responsive">
                          <?php  
                                if (session('buscarvehi')){
                                  $vehiculoss = VehiculoController::vehiculoswhere(session('buscarvehi')); 
                                }else{
                                  if (session('pos')){
                                    $vehiculoss = VehiculoController::vehiculos(session('pos')); 
                                  }else{
                                    $vehiculoss = VehiculoController::vehiculos(1); 
                                  } 
                                }                                                                                                                               
                              ?> 
                                    <table class="table table-striped">
                                      <thead>
                                        <tr>
                                          <th scope="col">#</th>
                                          <th style="min-width: 110px;" scope="col">Fecha registro</th>
                                          <th scope="col">Cliente</th>
                                          <th scope="col">Marca</th>
                                          <th scope="col">Modelo</th>
                                          <th scope="col">Color</th>
                                          <th scope="col">Año</th>
                                          <th scope="col">Placa</th>
                                          <th scope="col">Foto</th>
                                          <th scope="col">Observación</th>
                                          <th scope="col">Opciones</th>
                                        </tr>
                                      </thead>
                                      <tbody>
                                      @php
                                      $pos = 1;
                                      @endphp
                                      @foreach ($vehiculoss as $vehi) 
                                        <tr>
                                          <th scope="row">{{ $vehi->idv }}</th>
                                          <td style="text-align: center;">{{ str_replace(' ',"\n",$vehi->created_at)}}</td>
                                          <td>{{ $vehi->apcli}}  {{ $vehi->nomcli}}</td>
                                          <td> 
                                              <div style="text-align: center;" class="my-auto"> 
                                                @if($vehi->foto)
                                                <img src="{{url($vehi->foto)}}" style="width: 30px;" class="my-auto"/>
                                                  @else
                                                <img src="images/nologo.jpg" style="width: 30px;" class="my-auto"/>
                                                @endif
                                                <span style="margin-left: 7px;" class="my-auto">{{ $vehi->marca}}</span>
                                              </div>
                                          </td>
                                          <td>{{ $vehi->modelo}}</td>
                                          <td> 
                                          <div style="text-align: center;"><span >{{$vehi->nombreco }}</span>
                                          <div style="background-color:{{$vehi->code}};  height: 28px;width: 80px;border: 1px solid gray;"></div></div>
                                          </td>
                                          <td>{{ $vehi->anio}}</td>
                                          <td>{{ $vehi->placa}}</td>
                                          @if ($vehi->fotov)
                                          <td > 
                                          <img src="{{ url('storage/'.$vehi->fotov) }}"  style="width:80px;  padding:2px;     " />   
                                          </td>
                                          @else 
                                          <td></td>
                                           @endif 
                                          <td>{{ $vehi->obs}}</td> 
                                          <td><span class="badge badge-warning" style="cursor: pointer;width: 100%; " data-toggle="modal" data-target="#vehiculosm{{$vehi->idv}}"> <i class="fa fa fa-edit"></i> Editar</span>
                                       
                                          <a class="badge badge-danger"  style="cursor: pointer;width: 100%;  color: white;" onclick="event.preventDefault();
                                                     mensa(()=>{document.getElementById('desactivarvehi{{$vehi->idv}}').submit()});
                                                     "> <i class="fa fa fa-remove"></i> Eliminar</a>
                                       
                                        <form id="desactivarvehi{{$vehi->idv}}" action="{{ route('vehidesactivar') }}" method="POST" class="d-none">
                                          @csrf
                                            <input  name="menu" type="hidden" value="{{session('menu')}}">
                                            <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                            <input  name="idv" type="hidden" value="{{$vehi->idv}}">  
                                       </form>  
                                          
                                       
                                          <!-- /////////////////////////////////////////////////    -->
                                                      <div class="modal fade" id="vehiculosm{{$vehi->idv}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
                                                      <div class="modal-dialog  modal-dialog-centered">
                                                        <div class="modal-content">
                                                          <div class="modal-header">
                                                            <h4 class="modal-title" id="myLargeModalLabel">Editar vehículo</h4>
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                                                          </div>
                                                        <form  action="{{ route('editvehi') }}" method="POST" enctype="multipart/form-data">
                                                        @csrf
                                                        <input  name="menu" type="hidden" value="{{session('menu')}}">
                                                        <input  name="submenu" type="hidden" value="{{session('submenu')}}">    
                                                        <input  name="idv" type="hidden" value="{{$vehi->idv}}">      
                                                                  <div class="modal-body" style="text-align: left;"> 

                                                                  <div class="form-group">
                                                                        <label>Cliente :</label>
                                                                        <select class="form-control" name="idcliente"> 
                                                                          @foreach ($clientescombo as $clientec) 
                                                                              @if($vehi->idcli==$clientec->idcli)
                                                                              <option value="{{$clientec->idcli}}" selected>{{$clientec->apcli}} {{$clientec->nomcli }}</option> 
                                                                              @else
                                                                              <option value="{{$clientec->idcli}}">{{$clientec->apcli}} {{$clientec->nomcli }}</option> 
                                                                              @endif
                                                                          @endforeach
                                                                        </select>
                                                                      </div>


                                                                      <div class="form-group row">
                                                                       
                                                                        <label class="col-md-12">Marca y Modelo del vehiculo :</label>
                                                                        <div class="col-md-12">
                                                                        <select class="selectpicker"  width="auto" data-size="5" name="idmarca"  data-show-subtext="true" data-live-search="true"> 
                                                                        @foreach ($marcas as $marca) 
                                                                              @if($vehi->idma==$marca->idma&&$vehi->idmod==$marca->idmod)  
                                                                                        @if ($marca->foto)
                                                                                              <option   data-content='<img src="{{url($marca->foto)}}" style="width: 30px;"/> {{$marca->marca }} - {{$marca->modelo }}'
                                                                                            value="{{$marca->idma}}|{{$marca->idmod}}" selected> 
                                                                                              {{$marca->marca }} - {{$marca->modelo }}</option> 
                                                                                        @else 
                                                                                        <option   data-content='<img src="images/nologo.jpg" style="width: 30px;"/> {{$marca->marca }} - {{$marca->modelo }}'
                                                                                            value="{{$marca->idma}}|{{$marca->idmod}}" selected> 
                                                                                              {{$marca->marca }} - {{$marca->modelo }}</option>
                                                                                        @endif 
                                                                              @else
                                                                                        @if ($marca->foto)
                                                                                              <option   data-content='<img src="{{url($marca->foto)}}" style="width: 30px;"/> {{$marca->marca }} - {{$marca->modelo }}'
                                                                                            value="{{$marca->idma}}|{{$marca->idmod}}" > 
                                                                                              {{$marca->marca }} - {{$marca->modelo }}</option> 
                                                                                        @else 
                                                                                        <option   data-content='<img src="images/nologo.jpg" style="width: 30px;"/> {{$marca->marca }} - {{$marca->modelo }}'
                                                                                            value="{{$marca->idma}}|{{$marca->idmod}}" > 
                                                                                              {{$marca->marca }} - {{$marca->modelo }}</option>
                                                                                        @endif 
                                                                              @endif
                                                                        @endforeach
                                                                        </select>
                                                                        </div> 
                                                                        
                                                                      </div> 
                                                                      <div class="form-group row">
                                                                        <label class="col-md-12">Color del Vehículo :</label>
                                                                        <select class="selectpicker col-md-12" name="idcolor" data-size="5">  
                                                                              @foreach ($colores as $color) 
                                                                                  @if($vehi->color==$color->idco)  
                                                                                  <option
                                                data-content='<div style="display: inline-flex;"><span style="min-width: 80px;">{{$color->nombreco }}</span><div style="background-color:{{$color->code}}; margin-left: 10px;height: 28px;width: 134px;border: 1px solid gray;"></div></div>' 
                                                value="{{$color->idco}}" selected>{{$color->nombreco }}</option> 
                                                                                  @else
                                                                                  <option
                                                data-content='<div style="display: inline-flex;"><span style="min-width: 80px;">{{$color->nombreco }}</span><div style="background-color:{{$color->code}}; margin-left: 10px;height: 28px;width: 134px;border: 1px solid gray;"></div></div>' 
                                                value="{{$color->idco}}">{{$color->nombreco }}</option> 
                                                                                  @endif
                                                                              @endforeach
                                                                        </select>
                                                                      </div>


                                                                      <div class="form-group">
                                                                        <label>Año del vehículo :</label>
                                                                        <input class="form-control" value="{{ $vehi->anio}}"  name="idanio" placeholder="Ingrese el año" type="number" required>
                                                                      </div>
                                                                      <div class="form-group">
                                                                        <label>Placa :</label> 
                                                                        <input class="form-control" value="{{ $vehi->placa}}" name="idplaca" type="text" placeholder="Ingrese la placa del vehiculo" required>
                                                                      </div>
                                                                      <div class="form-group">
                                                                      <label>Fotografia del vehiculo:</label>
                                                                                <div id="previewiamgedit" class="mb-15" style="text-align: center;"> 
                                                                               @if ($vehi->fotov)
                                                                               <img src="{{ url('storage/'.$vehi->fotov) }}" alt="" style="width: 170px;
                                                                                      border: 4px solid gray;
                                                                                      height: 170px;
                                                                                      object-fit: cover;
                                                                                      border-radius: 50%;" >
                                                                                @endif 
                                                                               </div> 
                                                                                <input type="file" class="form-control" accept="image/*" capture="camera" name="imageceleditv"  onchange="ff(this.files[0])">
                                                                               
                                                                          </div>
                                                                      <div class="form-group">
                                                                        <label>Observación :</label> 
                                                                        <textarea class="form-control" style="height: auto;" rows="3" name="obsve"  placeholder="Ingrese observaciones" >{{ $vehi->obs}}</textarea>
                                                                      </div>
  
                                        
                                                                  </div>
                                                                  <div class="modal-footer">
                                                                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                                                                    <button type="submit" class="btn btn-primary">Modificar</button>
                                                                  </div>
                                                          </form>
                                                        </div>
                                                      </div>
                                                    </div>
                                        <!-- /////////////////////////////////////////////////    -->
                                        </td>
                                        </tr>
                                        <tr style="height: 15px;"></tr>
                                      @php
                                      $pos++;
                                      @endphp
                                      @endforeach 
                                      </tbody>
                                    </table>
                                    @if ($vehiculoss->lastPage()>1)
                                    <div class="col-lg-12 col-md-12 col-sm-12" style="text-align: center;"> 
                                       @if ($vehiculoss->currentPage()-1>0)
                                       <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$vehiculoss->currentPage()-1]) }}"  class="btn btn-light"> Anterior </a>
                                      </div>
                                        @endif
                                                  <div class="btn-group mb-15"> 
                                                      @for ($i = 1; $i <= $vehiculoss->lastPage(); $i++) 
                                                          @if ($vehiculoss->currentPage()==$i)
                                                          <a class="btn btn-success ">{{$i}}</a>
                                                          @else
                                                          <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$i]) }}"  class="btn btn-light ">{{$i}}</a>
                                                          @endif 
                                                      @endfor
                                                  </div>
                                      
                                      @if ($vehiculoss->currentPage()+1<=$vehiculoss->lastPage())
                                      <div class="btn-group mb-15"> 
                                        <a href="{{ route('menus',['idmenu' =>session('menu'),'idsubmenu' =>session('submenu'),'pos' =>$vehiculoss->currentPage()+1]) }}"  class="btn btn-light"> Siguiente </a>
                                      </div> 
                                        @endif
                                    </div>
                                    @endif
                                   
                                     
                          </div> 
                  </div>
          <!-- //////////////////// -->
          </div>
    </div>
</div>