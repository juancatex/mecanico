<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
  Route::get('/migrate', function () { 
    Artisan::call('migrate');
  });
  Route::get('/seed', function () { 
    Artisan::call('db:seed');
  });
  
Route::get('/', function () { 
    if (Auth::check()) {
        return view('dashboard');
    }else{
        return redirect('/login');
    }
});

Auth::routes(['register' => false,'reset' => false]);

Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard');
Route::get('/menus/{idmenu}/{idsubmenu}/{pos}', [App\Http\Controllers\MenuController::class, 'menus'])->name('menus');
Route::post('/regclientes', [App\Http\Controllers\ClienteController::class, 'registro'])->name('regclientes');
Route::post('/busquedacli', [App\Http\Controllers\ClienteController::class, 'busquedacli'])->name('busquedacli');
Route::post('/editclientes', [App\Http\Controllers\ClienteController::class, 'editclientes'])->name('editclientes');
Route::post('/clientesdesactivar', [App\Http\Controllers\ClienteController::class, 'clientesdesactivar'])->name('clientesdesactivar');
Route::get('/reporte1', [App\Http\Controllers\ClienteController::class, 'reporte1'])->name('reporte1');

Route::post('/regvehiculo', [App\Http\Controllers\VehiculoController::class, 'registro'])->name('regvehiculo');
Route::post('/busquedave', [App\Http\Controllers\VehiculoController::class, 'busquedave'])->name('busquedave'); 
Route::post('/vehidesactivar', [App\Http\Controllers\VehiculoController::class, 'vehidesactivar'])->name('vehidesactivar');
Route::post('/editvehi', [App\Http\Controllers\VehiculoController::class, 'editvehi'])->name('editvehi');
Route::get('/reporteve', [App\Http\Controllers\VehiculoController::class, 'reporteve'])->name('reporteve');

Route::post('/regrecepcion', [App\Http\Controllers\RecepcionController::class, 'regrecepcion'])->name('regrecepcion');
Route::post('/busquedarecep', [App\Http\Controllers\RecepcionController::class, 'busquedarecep'])->name('busquedarecep'); 
Route::post('/recepdesactivar', [App\Http\Controllers\RecepcionController::class, 'recepdesactivar'])->name('recepdesactivar');
Route::get('/reporterecep', [App\Http\Controllers\RecepcionController::class, 'reporterecep'])->name('reporterecep');
Route::get('/reporterecepall', [App\Http\Controllers\RecepcionController::class, 'reporterecepall'])->name('reporterecepall');

Route::post('/regorden', [App\Http\Controllers\OrdentrabajoController::class, 'regorden'])->name('regorden');
Route::post('/busquedarorden', [App\Http\Controllers\OrdentrabajoController::class, 'busquedarorden'])->name('busquedarorden'); 
Route::post('/ordendesactivar', [App\Http\Controllers\OrdentrabajoController::class, 'ordendesactivar'])->name('ordendesactivar');
Route::post('/consolidarorden', [App\Http\Controllers\OrdentrabajoController::class, 'consolidarorden'])->name('consolidarorden');
Route::get('/verordendoc/{idorden}', [App\Http\Controllers\OrdentrabajoController::class, 'verordendoc'])->name('verordendoc');

Route::post('/regordenmano', [App\Http\Controllers\ManodeobraController::class, 'regordenmano'])->name('regordenmano');

Route::post('/regorderepues', [App\Http\Controllers\RepuestosController::class, 'regorderepues'])->name('regorderepues');


Route::post('/regreparacion', [App\Http\Controllers\ReparacionController::class, 'regreparacion'])->name('regreparacion');
Route::post('/repadesactivar', [App\Http\Controllers\ReparacionController::class, 'repadesactivar'])->name('repadesactivar');
Route::post('/editree', [App\Http\Controllers\ReparacionController::class, 'editree'])->name('editree');

Route::post('/regpago', [App\Http\Controllers\PagosController::class, 'regpago'])->name('regpago');
Route::post('/desactivarpagoo', [App\Http\Controllers\PagosController::class, 'desactivarpagoo'])->name('desactivarpagoo');
Route::get('/reportepagoo', [App\Http\Controllers\PagosController::class, 'reportepagoo'])->name('reportepagoo');
Route::get('/reportepagoorden', [App\Http\Controllers\PagosController::class, 'reportepagoorden'])->name('reportepagoorden'); 

Route::post('/envioemail', [App\Http\Controllers\emailController::class, 'envioemail'])->name('envioemail');
Route::post('/envioemailextracto', [App\Http\Controllers\emailController::class, 'envioemailextracto'])->name('envioemailextracto');
Route::post('/enviowhatsapp', [App\Http\Controllers\emailController::class, 'enviowhatsapp'])->name('enviowhatsapp');


