<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\pagos;
Use Alert; 
use Illuminate\Support\Facades\Storage; 
use PDF;


class PagosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function regpago(Request $request) {  
           
        $pago = new pagos();  
        $pago->idorden = $request->idorden; 
        $pago->detalle = $request->detalle;   
        $pago->monto = $request->monto;   
        $pago->save(); 
           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function pagosorden($pos) {        
        $data1 = pagos::select('pagos.*', 'marcas.marca','modelos.modelo','vehiculos.placa')   
        ->join('ordentrabajos', 'pagos.idorden', '=', 'ordentrabajos.idorden') 
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1')
        ->where('pagos.activo','=','1') 
        ->orderby ('pagos.idorden')
        ->orderby ('pagos.created_at')
        ->paginate(10, ['*'], 'page', $pos);
  
        return ($data1);    
    }
    public static function desactivarpagoo(Request $request) {        
        $repp = pagos::findOrFail($request->idpago);
        $repp->activo = 0; 
        $repp->save();
           Alert::success('Se elimino los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function reportepagoo(Request $request){
        $pagos = pagos::select('pagos.*', 'marcas.marca','modelos.modelo','vehiculos.placa')   
        ->join('ordentrabajos', 'pagos.idorden', '=', 'ordentrabajos.idorden') 
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1')
        ->where('pagos.activo','=','1') 
        ->orderby ('pagos.idorden')
        ->orderby ('pagos.created_at')  
        ->orderby ('created_at')->get();
              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf = PDF::loadView('reportes/pagos', ['pagos'=>$pagos,
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]);  
            return $pdf->stream('reportepagos.pdf'); 
    }
    public static function reportepagoorden(Request $request){
        $pagos = pagos::select('pagos.*', 'marcas.marca','modelos.modelo','vehiculos.placa')   
        ->join('ordentrabajos', 'pagos.idorden', '=', 'ordentrabajos.idorden') 
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1')
        ->where('pagos.activo','=','1') 
        ->orderby ('pagos.idorden')
        ->orderby ('pagos.created_at')  
        ->where('pagos.idorden','=',$request->idorden)  
        ->orderby ('created_at')->get();
              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf = PDF::loadView('reportes/pagospororden', ['pagos'=>$pagos, 'codigo'=>$request->idorden,
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]);  
            return $pdf->stream('reportepagos.pdf'); 
    }
}
