<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\manodeobra;
use App\Models\ordentrabajo;
Use Alert;
use PDF;
class ManodeobraController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function regordenmano(Request $request) {   
        $orden = new manodeobra();  
        $orden->idorden = $request->idorden;  
        $orden->detalle = $request->desmano;  
        $orden->monto = $request->montomano;  
        $orden->save();

        $ordentrabajo = ordentrabajo::findOrFail($request->idorden);
        $ordentrabajo->summano = round($ordentrabajo->summano + $request->montomano,2); 
        $ordentrabajo->total = round($ordentrabajo->sumrepuestos+$ordentrabajo->summano,2); 
        $ordentrabajo->save();
           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
}
