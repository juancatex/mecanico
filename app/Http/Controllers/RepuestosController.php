<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\repuestos;
use App\Models\ordentrabajo;
Use Alert;
use PDF;

class RepuestosController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function regorderepues(Request $request) {   
        $orden = new repuestos();  
        $orden->idorden = $request->idorden;  
        $orden->detalle = $request->desrep;  
        $orden->cant = $request->cant;  
        $orden->monto = $request->montorep;  
        $orden->save();

        $ordentrabajo = ordentrabajo::findOrFail($request->idorden);
        $ordentrabajo->sumrepuestos = round($ordentrabajo->sumrepuestos + $request->montorep,2); 
        $ordentrabajo->total = round($ordentrabajo->sumrepuestos+$ordentrabajo->summano,2); 
        $ordentrabajo->save();

           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
}
