<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\recepcion;
Use Alert;
use PDF;

class RecepcionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function regrecepcion(Request $request) {  
             
        $recep = new recepcion();  
        $recep->idv = $request->idv; 
        $recep->diag = $request->diag;   
        $recep->ul = is_null($request->ul)?0:1; 
        $recep->an = is_null($request->an)?0:1;  
        $recep->el = is_null($request->el)?0:1; 
        $recep->llan = is_null($request->llan)?0:1; 
        $recep->bx = is_null($request->bx)?0:1; 

        $recep->ga = is_null($request->ga)?0:1;  
        $recep->llar = is_null($request->llar)?0:1;  
        $recep->llac = is_null($request->llac)?0:1; 
        $recep->eh = is_null($request->eh)?0:1; 
        $recep->ts = is_null($request->ts)?0:1; 
        $recep->llax = is_null($request->llax)?0:1; 

        $recep->ra = is_null($request->ra)?0:1; 
        $recep->it = is_null($request->it)?0:1; 
        $recep->en = is_null($request->en)?0:1; 
        $recep->ca = is_null($request->ca)?0:1; 
        $recep->ce = is_null($request->ce)?0:1; 
        $recep->to = is_null($request->to)?0:1; 
 
        $recep->mc1 = is_null($request->mc1)?0:1;
        $recep->mc2 = is_null($request->mc2)?0:1;
        $recep->mc3 = is_null($request->mc3)?0:1;
        $recep->mc4 = is_null($request->mc4)?0:1;
        $recep->mc5 = is_null($request->mc5)?0:1; 
        $recep->mc6 = is_null($request->mc6)?0:1;

        $recep->mp1 = is_null($request->mp1)?0:1;
        $recep->mp2 = is_null($request->mp2)?0:1;
        $recep->mp3 = is_null($request->mp3)?0:1;
        $recep->mp4 = is_null($request->mp4)?0:1;
        $recep->mp5 = is_null($request->mp5)?0:1; 
        $recep->mp6 = is_null($request->mp6)?0:1;

        $recep->mpr1 = is_null($request->mpr1)?0:1;
        $recep->mpr2 = is_null($request->mpr2)?0:1;
        $recep->mpr3 = is_null($request->mpr3)?0:1;

        $recep->mpre1 = is_null($request->mpre1)?0:1;
        $recep->mpre2 = is_null($request->mpre2)?0:1;
        $recep->mpre3 = is_null($request->mpre3)?0:1;
        $recep->mpre4 = is_null($request->mpre4)?0:1;

        $recep->rev1 = is_null($request->rev1)?0:1;
        $recep->rev2 = is_null($request->rev2)?0:1;
        $recep->rev3 = is_null($request->rev3)?0:1;
        $recep->rev4 = is_null($request->rev4)?0:1;
        $recep->rev5 = is_null($request->rev5)?0:1;
        $recep->rev6 = is_null($request->rev6)?0:1;
        $recep->rev7 = is_null($request->rev7)?0:1;
        $recep->rev8 = is_null($request->rev8)?0:1;
        $recep->rev9 = is_null($request->rev9)?0:1;
        
        $recep->otro1 = is_null($request->otro1)?0:1;
        $recep->otro2 = is_null($request->otro2)?0:1;


        $recep->obsrec = $request->obsrec;   
        $recep->save();
  
           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function busquedarecep(Request $request) {   
        if (strlen($request->buscar)>0) {
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('buscarrecep',$request->buscar)->with('mensaje',true);     
        }else{
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);     
        }
        
    }

    public static function busrecepwhere($buscar) {  
        $buscar='%'.$buscar.'%';      
        $data1 = recepcion::select('recepcions.*', 'marcas.marca','modelos.modelo','vehiculos.placa')
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where(function($query) use ($buscar) {
            $query->where('marcas.marca','like', $buscar)  
                ->orWhere('recepcions.diag','like', $buscar) 
                ->orWhere('modelos.modelo','like', $buscar)
                ->orWhere('vehiculos.placa','like', $buscar);
        })->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->paginate(100);
        return ($data1);    
    }
    public static function listarecepciones($pos) {        
        $data1 = recepcion::select('recepcions.*', 'marcas.marca','modelos.modelo','vehiculos.placa')
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->orderby ('recepcions.idrec','desc')
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->paginate(10, ['*'], 'page', $pos);
        return ($data1);    
    }
    public static function listarecepcionescombo() {        
        $data1 = recepcion::select('recepcions.*', 'marcas.marca','modelos.modelo','vehiculos.placa','vehiculos.fotov')
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->get();
        return ($data1);    
    }
    public static function recepdesactivar(Request $request) {        
        $recep = recepcion::findOrFail($request->idrec);
        $recep->activo = 0; 
        $recep->save();
           Alert::success('Se elimino los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function reporterecep(Request $request){
        $vehiculos = recepcion::select('recepcions.*', 'marcas.marca','modelos.modelo','vehiculos.placa')
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->whereDate('recepcions.created_at',$request->daterange) 
        ->orderby ('recepcions.idrec','desc')
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')->get();
              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf = PDF::loadView('reportes/vehiculosrecepcionadosfecha', ['vehiculos'=>$vehiculos, 'fecha'=>$request->daterange,
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]); 
            $pdf->setPaper('a4', 'landscape');
            return $pdf->stream('reportevehiculosre.pdf'); 
    }
    public static function reporterecepall(Request $request){
        $vehiculos = recepcion::select('recepcions.*', 'marcas.marca','modelos.modelo','vehiculos.placa')
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1')  
        ->orderby ('recepcions.idrec','desc')
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')->get();
              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf = PDF::loadView('reportes/vehiculosrecepcionados', ['vehiculos'=>$vehiculos, 
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]); 
            $pdf->setPaper('a4', 'landscape');
            return $pdf->stream('reportevehiculosre.pdf'); 
    }
}
