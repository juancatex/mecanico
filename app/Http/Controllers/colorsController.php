<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\color;

class colorsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public static function listarcolorescombo() {        
        $data1 = color::where('activo','=','1') 
        ->orderby ('nombreco')->get();
        return ($data1);    
    }
}
