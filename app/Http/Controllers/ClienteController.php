<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\cliente;
Use Alert;
use PDF;
class ClienteController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function listarclientescombo() {        
        $data1 = cliente::where('activo','=','1') 
        ->orderby ('apcli')->get();
        return ($data1);    
    }
    public static function listarclientesp($pos) {        
        $data1 = cliente::where('activo','=','1') 
        ->orderby ('idcli','desc')->paginate(10, ['*'], 'page', $pos);
        return ($data1);    
    }
    public static function listarclienteswhere($buscar) {  
        $buscar='%'.$buscar.'%';      
        $data1 = cliente::where('activo','=','1') 
        ->where(function($query) use ($buscar) {
            $query->where('nomcli','like', $buscar)
                ->orWhere('apcli','like', $buscar)
                ->orWhere('emailcli','like', $buscar)
                ->orWhere('dircli','like', $buscar)
                ->orWhere('telcli','like', $buscar);
        })->orderby ('idcli','desc')->paginate(100);
        return ($data1);    
    }

    public static function busquedacli(Request $request) {   
        if (strlen($request->buscar)>0) {
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('buscarcli',$request->buscar)->with('mensaje',true);     
        }else{
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);     
        }
        
    }
    public static function registro(Request $request) {        
        $cliente = new cliente();  
        $cliente->ci = $request->ci;
        $cliente->nomcli = $request->nombrecli;
        $cliente->apcli = $request->apcli;
        $cliente->telcli = $request->telcli; 
        $cliente->emailcli = $request->emailcli; 
        $cliente->dircli = $request->dircli; 
        if($request->file('imagecel')) {
            $fileName = time().'_'.$request->file('imagecel')->getClientOriginalName();
            $filePath = $request->file('imagecel')->storeAs('images', $fileName, 'public');          
            // $filePath = $request->file('imagecel')->store('public/images'); 
            $cliente->foto = $filePath; 
        }
        $cliente->save();
           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function editclientes(Request $request) {        
        $cliente = cliente::findOrFail($request->idcli);
        $cliente->ci = $request->ci;
        $cliente->nomcli = $request->nombrecli;
        $cliente->apcli = $request->apcli;
        $cliente->telcli = $request->telcli; 
        $cliente->emailcli = $request->emailcli; 
        $cliente->dircli = $request->dircli; 
        if($request->file('imageceledit')) {
            $fileName = time().'_'.$request->file('imageceledit')->getClientOriginalName();
            $filePath = $request->file('imageceledit')->storeAs('images', $fileName, 'public');      
            $cliente->foto = $filePath; 
        }
        $cliente->save();
           Alert::success('Se modifico los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function clientesdesactivar(Request $request) {        
        $cliente = cliente::findOrFail($request->idcli);
        $cliente->activo = 0; 
        $cliente->save();
           Alert::success('Se elimino los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
   
    public static function reporte1(Request $request){
        $clientes = cliente::where('activo','=','1') 
        ->orderby ('idcli')->get();
              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf = PDF::loadView('reportes/clientes1', ['clientes'=>$clientes, 
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]); 
            return $pdf->stream('reporteclientes.pdf'); 
    }
}
