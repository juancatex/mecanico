<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\menu;
use Auth;

class MenuController extends Controller
{
      
    public function menus($idmenu, $idsubmenu, $pos)
    {
        if (Auth::check()) { 
            return redirect('dashboard')->with('menu',$idmenu)->with('submenu',$idsubmenu)->with('pos',$pos);  
        }else{
            return redirect('/login');
        }
    }  
    public static function listarmenus() {        
        $data1 = menu::where ('activo','=','1') 
        ->orderby ('idmenu')->get();
        return ($data1);    
    }
}
