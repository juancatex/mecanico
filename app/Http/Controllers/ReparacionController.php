<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\reparacion;
Use Alert; 

class ReparacionController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function regreparacion(Request $request) {  
           
        $reparacion = new reparacion();  
        $reparacion->idorden = $request->idorden; 
        $reparacion->detalle = $request->detalle;   
        $reparacion->save(); 
           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function reparaciones($pos) {        
        $data1 = reparacion:: where('activo','=','1')   
        ->orderby ('created_at')
        ->paginate(10, ['*'], 'page', $pos);
        return ($data1);    
    }
    public static function repadesactivar(Request $request) {        
        $repp = reparacion::findOrFail($request->idrepa);
        $repp->activo = 0; 
        $repp->save();
           Alert::success('Se elimino los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }

    public static function editree(Request $request) {    
        $cliente = reparacion::findOrFail($request->idrepa); 
        $cliente->detalle = $request->detalle;  
        $cliente->save();
           Alert::success('Se modifico los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
}
