<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\marca;
Use Alert;
use PDF;
class marcasController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function marcas() {        
        $data1 = marca::select('marcas.idma','marcas.marca','marcas.foto','modelos.idmod','modelos.modelo')
        ->join('modelos', 'marcas.idma', '=', 'modelos.marca')
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1') 
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->get();
        return ($data1);    
    }
}
