<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\ordentrabajo;
use App\Models\manodeobra;
use App\Models\repuestos;
Use Alert;
use PDF;

class OrdentrabajoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function regorden(Request $request) {  
          
        $orden = new ordentrabajo();  
        $orden->idrec = $request->idrec;  
        $orden->save();
           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function busquedarorden(Request $request) {   
        if (strlen($request->buscar)>0) {
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('buscarorden',$request->buscar)->with('mensaje',true);     
        }else{
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);     
        } 
    }
    public static function busordenwhere($buscar) {  
        $buscar='%'.$buscar.'%';      
        $data1 = ordentrabajo::select('ordentrabajos.*', 'marcas.marca','modelos.modelo','vehiculos.placa')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->where(function($query) use ($buscar) {
            $query->where('marcas.marca','like', $buscar)   
                ->orWhere('modelos.modelo','like', $buscar)
                ->orWhere('vehiculos.placa','like', $buscar);
        })
        ->orderby ('ordentrabajos.estado','desc')
        ->orderby ('recepcions.created_at')
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->paginate(100);
        return ($data1);    
    }
    public static function listaordenes($pos) {        
        $data1 = ordentrabajo::select('ordentrabajos.*', 'marcas.marca','modelos.modelo','vehiculos.placa')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->orderby ('ordentrabajos.idorden','asc')
        ->orderby ('recepcions.created_at')
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->paginate(10, ['*'], 'page', $pos);
        return ($data1);    
    }
    public static function listaordenesclientes($pos) {        
        $data1 = ordentrabajo::select('ordentrabajos.*','clientes.nomcli','clientes.apcli','clientes.dircli','clientes.telcli', 'clientes.emailcli',
        'marcas.marca','modelos.modelo','vehiculos.placa','vehiculos.anio')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->where('ordentrabajos.estado','=','2')  
        ->orderby ('recepcions.created_at')
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->paginate(10, ['*'], 'page', $pos);
        return ($data1);    
    }
    public static function ordenescombo() {        
        $data1 = ordentrabajo::select('ordentrabajos.*', 'marcas.marca','modelos.modelo','vehiculos.placa','vehiculos.fotov')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->where('ordentrabajos.estado','=','2')  
        ->orderby ('recepcions.created_at')
        ->orderby ('vehiculos.placa')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->get();
        return ($data1);    
    }
    public static function ordendesactivar(Request $request) {        
        $ordentrabajo = ordentrabajo::findOrFail($request->idorden);
        $ordentrabajo->activo = 0; 
        $ordentrabajo->save();
           Alert::success('Se elimino los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function consolidarorden(Request $request) {        
        $ordentrabajo = ordentrabajo::findOrFail($request->idorden);
        $ordentrabajo->estado = 2; 
        $ordentrabajo->save();
           Alert::success('Se consolido el dato correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }

    public static function verordendoc($idorden){
        $ordendoc = ordentrabajo::select('ordentrabajos.*','recepcions.*','clientes.nomcli','clientes.apcli','clientes.dircli','clientes.telcli','clientes.foto', 'marcas.marca','modelos.modelo','vehiculos.placa','vehiculos.fotov','vehiculos.anio')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->where('ordentrabajos.idorden',$idorden) 
        ->first();

        $manoobra = manodeobra::where ('activo','=','1') 
        ->where ('idorden',$idorden)
        ->orderby ('idmano')->get();

        $repuestos = repuestos::where ('activo','=','1') 
        ->where ('idorden',$idorden)
        ->orderby ('idrepu')->get();

              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf = PDF::loadView('reportes/ordentrabajo', ['orden'=>$ordendoc, 
            'mano'=>$manoobra, 'repu'=>$repuestos, 
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]); 
            return $pdf->stream('reporteordentrabajo.pdf'); 
    }
}
