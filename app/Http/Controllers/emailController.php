<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Mail;
Use Alert; 
use App\Models\pagos;
use App\Models\ordentrabajo; 
use App\Models\manodeobra;
use App\Models\repuestos;
use Illuminate\Support\Facades\Storage; 
use Twilio\Rest\Client;
use Exception;
use PDF;

class emailController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public static function envioemail(Request $request) { 
        $emails=$request->email;
        $explode = explode("@", $emails); 
        if ($explode[1] == "hotmail.com") {
            $template='emailtemplate';
        } else {
            $template='emailtemplategmail';
        }
        $name="Fama Motors";  
        Mail::send($template, ['mensaje'=>$request->mensaje], 
        function($message) use ($emails,$name)
        {   $message->to($emails)->subject($name);    });
           Alert::success('Envio exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function envioemailextracto(Request $request){
        $pagos = pagos:: where('activo','=','1')  
        ->where('idorden','=',$request->idorden)  
        ->orderby ('created_at')->get();

        $datos =ordentrabajo::select('ordentrabajos.total','clientes.nomcli','clientes.apcli','clientes.dircli','clientes.telcli', 'clientes.emailcli',
        'marcas.marca','modelos.modelo','vehiculos.placa','vehiculos.anio')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->where('ordentrabajos.estado','=','2')  
        ->where('ordentrabajos.idorden','=',$request->idorden)->first();

              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
              $pdf = PDF::loadView('reportes/pagosporordenemail', ['pagos'=>$pagos, 'codigo'=>$request->idorden,'socio'=>$datos,
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]);   
        $emails=$request->email;
        $name="Fama Motors";  
        $explode = explode("@", $emails); 
        if ($explode[1] == "hotmail.com") {
            $template='emailtemplate';
        } else {
            $template='emailtemplategmail';
        }







        /////////////////////////////////////////////////////
        $ordendoc = ordentrabajo::select('ordentrabajos.*','recepcions.*','clientes.nomcli','clientes.apcli','clientes.dircli','clientes.telcli','clientes.foto', 'marcas.marca','modelos.modelo','vehiculos.placa','vehiculos.fotov','vehiculos.anio')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->where('ordentrabajos.idorden',$request->idorden) 
        ->first();

        $manoobra = manodeobra::where ('activo','=','1') 
        ->where ('idorden',$request->idorden)
        ->orderby ('idmano')->get();

        $repuestos = repuestos::where ('activo','=','1') 
        ->where ('idorden',$request->idorden)
        ->orderby ('idrepu')->get();

              $logo1 = Storage::path('fotos/logomotorr.png');
              $logo641 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf1 = PDF::loadView('reportes/ordentrabajo', ['orden'=>$ordendoc, 
            'mano'=>$manoobra, 'repu'=>$repuestos, 
                                'foto'=>'data:'.mime_content_type($logo1) . ';base64,' . $logo641]); 
          
        /////////////////////////////////////////////////////
       

           switch($request->adjunto){
            case('extracto'):
                Mail::send($template, ['mensaje'=>$request->mensaje],  
                function($message) use ($emails,$name,$pdf)
                {   $message->to($emails)->subject($name)
                    ->attachData($pdf->output(), "extracto_de_pagos.pdf");    
                }
                 );
               Alert::success('Envio exitoso', '');
                break;
            case('orden'):
                Mail::send($template, ['mensaje'=>$request->mensaje],  
                function($message) use ($emails,$name,$pdf1)
                {   $message->to($emails)->subject($name)
                    ->attachData($pdf1->output(), "orden_trabajo.pdf");    
                }
                 );
               Alert::success('Envio exitoso', '');
                break;
           }

         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true); 
    }

    public static function enviowhatsapp(Request $request){
        $pagos = pagos:: where('activo','=','1')  
        ->where('idorden','=',$request->idorden)  
        ->orderby ('created_at')->get();

        $datos =ordentrabajo::select('ordentrabajos.total','clientes.nomcli','clientes.apcli','clientes.dircli','clientes.telcli', 'clientes.emailcli',
        'marcas.marca','modelos.modelo','vehiculos.placa','vehiculos.anio')
        ->join('recepcions', 'ordentrabajos.idrec', '=', 'recepcions.idrec') 
        ->join('vehiculos', 'recepcions.idv', '=', 'vehiculos.idv') 
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')  
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1')  
        ->where('vehiculos.activo','=','1') 
        ->where('recepcions.activo','=','1') 
        ->where('ordentrabajos.activo','=','1') 
        ->where('ordentrabajos.estado','=','2')  
        ->where('ordentrabajos.idorden','=',$request->idorden)->first();

              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
              $pdf = PDF::loadView('reportes/pagosporordenemail', ['pagos'=>$pagos, 'codigo'=>$request->idorden,'socio'=>$datos,
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]);   
        
        $name="Fama Motors";  
        // $receiverNumber = "591".$request->telcli;
        $receiverNumber = "whatsapp:+59177549539";
        $twilio_number = "whatsapp:+59177549539"; 
        $message = $request->mensaje;
  
        try {
  
            $account_sid = getenv("TWILIO_SID");
            $auth_token = getenv("TWILIO_TOKEN"); 
  
            $twilio = new Client($account_sid, $auth_token);
            // $client->messages->create($receiverNumber, [
            //     'from' => $twilio_number, 
            //     'body' => $message]);
        //    $message = $twilio->messages
        //           ->create("whatsapp:+59177549539",["from" => "whatsapp:+59169712335","body" => "Hello there!"]); 

        $message = $twilio->messages
        ->create("+59167099830", // to
                 [
                     "body" => "This is the ship that made the Kessel Run in fourteen parsecs?",
                     "from" => "+14094032858"
                 ]
        );
        Alert::success('Envio exitoso', '');
  
        } catch (Exception $e) {
            // dd("Error: ". $e->getMessage());
            Alert::error('Error',$e->getMessage());
        }

 
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true); 
    }
}
