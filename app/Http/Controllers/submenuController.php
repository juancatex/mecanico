<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\submenu;

class submenuController extends Controller
{
    public static function listarsubmenus($id) {        
        $data1 = submenu::where ('menu','=',$id)->where ('activo','=','1') 
        ->orderby ('idsubmenu')->get();
        return ($data1);    
    }
}
