<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use App\Models\vehiculo;
Use Alert;
use PDF;

class VehiculoController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    public static function vehiculoswhere($buscar) {  
        $buscar='%'.$buscar.'%';      
        $data1 = vehiculo::select('vehiculos.color','vehiculos.fotov','colors.nombreco','colors.code','vehiculos.placa','vehiculos.anio','vehiculos.obs', 'clientes.idcli','marcas.idma','marcas.foto', 'modelos.idmod'
        , 'clientes.nomcli', 'clientes.apcli', 'marcas.marca','modelos.modelo','vehiculos.idv')
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')
        ->join('colors', 'vehiculos.color', '=', 'colors.idco')
        ->where('clientes.activo','=','1') 
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1') 
        ->where('colors.activo','=','1') 
        ->where('vehiculos.activo','=','1') 
        ->where(function($query) use ($buscar) {
            $query->where('clientes.nomcli','like', $buscar)
                ->orWhere('clientes.apcli','like', $buscar)
                ->orWhere('marcas.marca','like', $buscar)
                ->orWhere('modelos.modelo','like', $buscar)
                ->orWhere('vehiculos.placa','like', $buscar);
        })
        ->orderby ('vehiculos.idv','desc')
        ->orderby ('clientes.apcli')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->orderby ('vehiculos.anio')->paginate(100);
        return ($data1);    
    }
    public static function vehiculoscombo() {        
        $data1 = vehiculo::select('vehiculos.color','colors.nombreco','vehiculos.placa','vehiculos.anio','vehiculos.obs', 'clientes.idcli','marcas.idma', 'modelos.idmod'
        , 'clientes.nomcli', 'clientes.apcli', 'marcas.marca','modelos.modelo','vehiculos.idv')
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')
        ->join('colors', 'vehiculos.color', '=', 'colors.idco')
        ->where('clientes.activo','=','1') 
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1') 
        ->where('colors.activo','=','1') 
        ->where('vehiculos.activo','=','1') 
        ->orderby ('clientes.apcli')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->orderby ('vehiculos.anio')
        ->get();
        return ($data1);    
    }
    public static function vehiculos($pos) {        
        $data1 = vehiculo::select('vehiculos.color','vehiculos.fotov','colors.nombreco','colors.code','vehiculos.placa','vehiculos.anio','vehiculos.obs', 'clientes.idcli','marcas.idma','marcas.foto', 'modelos.idmod'
        , 'clientes.nomcli', 'clientes.apcli', 'marcas.marca','modelos.modelo','vehiculos.idv','vehiculos.created_at')
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')
        ->join('colors', 'vehiculos.color', '=', 'colors.idco')
        ->where('clientes.activo','=','1') 
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1') 
        ->where('colors.activo','=','1') 
        ->where('vehiculos.activo','=','1') 
        ->orderby ('vehiculos.idv','desc')
        ->orderby ('clientes.apcli')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->orderby ('vehiculos.anio')
        ->paginate(10, ['*'], 'page', $pos);
        return ($data1);    
    }

    public static function registro(Request $request) {  
        $datosmarca=explode("|",$request->idmarca);      
        $cliente = new vehiculo();  
        $cliente->idcli = $request->idcliente;
        $cliente->idma = $datosmarca[0];
        $cliente->idmod = $datosmarca[1];
        $cliente->color = $request->idcolor; 
        $cliente->placa = strtoupper($request->idplaca); 
        $cliente->anio = $request->idanio; 
        $cliente->obs = $request->obsve;  
        if($request->file('imagecelv')) {
            $fileName = time().'_'.$request->file('imagecelv')->getClientOriginalName();
            $filePath = $request->file('imagecelv')->storeAs('images', $fileName, 'public');      
            $cliente->fotov = $filePath; 
        }
        $cliente->save();
           Alert::success('Registro exitoso', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function busquedave(Request $request) {   
        if (strlen($request->buscar)>0) {
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('buscarvehi',$request->buscar)->with('mensaje',true);     
        }else{
            return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);     
        }
        
    }
    public static function vehidesactivar(Request $request) {        
        $cliente = vehiculo::findOrFail($request->idv);
        $cliente->activo = 0; 
        $cliente->save();
           Alert::success('Se elimino los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function editvehi(Request $request) {   
        $datosmarca=explode("|",$request->idmarca);      
        $cliente = vehiculo::findOrFail($request->idv);
        $cliente->idcli = $request->idcliente;
        $cliente->idma = $datosmarca[0];
        $cliente->idmod = $datosmarca[1];
        $cliente->color = $request->idcolor; 
        $cliente->placa = strtoupper($request->idplaca); 
        $cliente->anio = $request->idanio; 
        $cliente->obs = $request->obsve;  
        if($request->file('imageceleditv')) {
            $fileName = time().'_'.$request->file('imageceleditv')->getClientOriginalName();
            $filePath = $request->file('imageceleditv')->storeAs('images', $fileName, 'public');          
            // $filePath = $request->file('imagecel')->store('public/images'); 
            $cliente->fotov = $filePath; 
        }
        $cliente->save();
           Alert::success('Se modifico los datos correctamente', '');
         return redirect('dashboard')->with('menu',$request->menu)->with('submenu',$request->submenu)->with('pos',1)->with('mensaje',true);  
    }
    public static function reporteve(Request $request){
        $vehiculos = vehiculo::select('vehiculos.color','colors.nombreco','vehiculos.placa','vehiculos.fotov','vehiculos.anio','vehiculos.obs', 'clientes.idcli','marcas.idma', 'modelos.idmod'
        , 'clientes.nomcli', 'clientes.apcli', 'marcas.marca','modelos.modelo','vehiculos.idv')
        ->join('clientes', 'vehiculos.idcli', '=', 'clientes.idcli')
        ->join('marcas', 'vehiculos.idma', '=', 'marcas.idma')
        ->join('modelos', 'vehiculos.idmod', '=', 'modelos.idmod')
        ->join('colors', 'vehiculos.color', '=', 'colors.idco')
        ->where('clientes.activo','=','1') 
        ->where('marcas.activo','=','1') 
        ->where('modelos.activo','=','1') 
        ->where('colors.activo','=','1') 
        ->where('vehiculos.activo','=','1') 
        ->orderby ('clientes.apcli')
        ->orderby ('marcas.marca')
        ->orderby ('modelos.modelo')
        ->orderby ('vehiculos.anio')->get();
              $logo = Storage::path('fotos/logomotorr.png');
              $logo64 = base64_encode(Storage::get('fotos/logomotorr.png'));  
            $pdf = PDF::loadView('reportes/vehiculos', ['vehiculos'=>$vehiculos, 
                                'foto'=>'data:'.mime_content_type($logo) . ';base64,' . $logo64]); 
            return $pdf->stream('reportevehiculos.pdf'); 
    }
}
