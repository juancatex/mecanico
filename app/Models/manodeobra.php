<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class manodeobra extends Model
{
    use HasFactory;
    protected $table='manodeobras';
    protected $primaryKey='idmano';
    protected $fillable = ['idorden','detalle','monto','activo'];
 
}
