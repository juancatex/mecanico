<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class reparacion extends Model
{
    use HasFactory;
    protected $table='reparacions';
    protected $primaryKey='idrepa';
    protected $fillable = ['idorden','detalle','activo'];
}
