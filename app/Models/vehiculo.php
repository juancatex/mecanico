<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class vehiculo extends Model
{
    use HasFactory;
    protected $table='vehiculos';
    protected $primaryKey='idv';
    protected $fillable = ['idcli','fotov','idma','idmod','color','placa','obs','activo','anio'];
}
