<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ordentrabajo extends Model
{
    use HasFactory;
    protected $table='ordentrabajos';
    protected $primaryKey='idorden';
    protected $fillable = ['idrec','sumrepuestos','summano','total','estado','activo'];
}
