<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tipo_user extends Model
{
    use HasFactory;
    protected $table='tipo_users';
    protected $primaryKey='idtipo';
    protected $fillable = ['detalle'];
}
