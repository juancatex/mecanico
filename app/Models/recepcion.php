<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class recepcion extends Model
{
    use HasFactory;
    protected $table='recepcions';
    protected $primaryKey='idrec';
    protected $fillable = ['idv','diag','ul','an','el','ga','llar','llac','ra','it','en','obsrec','activo','mc1','mc2','mc3','mc4','mc5','mc6','mp1','mp2','mp3','mp4','mp5','mp6','mpr1','mpr2','mpr3','mpre1','mpre2','mpre3','mpre4',
'rev1','rev2','rev3','rev4','rev5','rev6','rev7','rev8','rev9','otro1','otro2'
];
}
