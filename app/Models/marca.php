<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class marca extends Model
{
    use HasFactory;
    protected $table='marcas';
    protected $primaryKey='idma';
    protected $fillable = ['marca','foto','activo'];
}
