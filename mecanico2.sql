-- MariaDB dump 10.19  Distrib 10.4.24-MariaDB, for Win64 (AMD64)
--
-- Host: localhost    Database: mecanico
-- ------------------------------------------------------
-- Server version	10.4.24-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `clientes`
--

DROP TABLE IF EXISTS `clientes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `clientes` (
  `idcli` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nomcli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `apcli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `telcli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ci` bigint(11) DEFAULT 0,
  `emailcli` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dircli` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idcli`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `clientes`
--

LOCK TABLES `clientes` WRITE;
/*!40000 ALTER TABLE `clientes` DISABLE KEYS */;
INSERT INTO `clientes` VALUES (1,'Cielo','Swift','78688553',99123411,'glehner@hotmail.com','Av. Rodrigue \"B\"',1,NULL,'2022-05-09 19:09:34'),(2,'Ismael','Casillas','9915974',10002232,'maigarrat@hotmail.com','Av.Union Nro.2300',1,'2022-04-26 14:06:05','2022-05-09 19:09:59'),(3,'Andy','Vargas','77495395',18288834,'villa1234@hotmail.com','Av.Sanches de Lozada',1,'2022-04-27 14:04:10','2022-05-09 19:10:16'),(4,'Jhonny','Apaza','77133424',98901799,'123r@hotmail.com','Av. Estructurante',1,'2022-04-27 14:10:09','2022-05-09 19:15:00'),(5,'Edgar','Fernades','99122342',12345678,'63dwe@hotmail.com','Av.G',1,'2022-04-27 14:11:28','2022-04-27 19:42:36'),(6,'Andres','Villa','73023123',3327148,'angel_amvh@hotmail.com','Av.Arce Nro.2300',1,'2022-04-27 21:13:23','2022-04-27 21:13:23'),(7,'Eduardo Angel','Federico Villa','76561233',99326233,'samueljacimto@gmail.com','Av. Oro Negro',1,'2022-05-09 19:12:11','2022-05-09 19:12:11'),(8,'Rosalia Nydia','Vargas Gutierrez','77124456',100227333,'maiaarratia@gmail.com','Av. Unión Zona. Kenko Nro.100',1,'2022-05-09 19:14:03','2022-05-09 19:14:03'),(9,'Marcelo Takesy','Viza Paco','77271771',9912008,'maiaarratia@gmail.com','Av. Rio Selva',1,'2022-05-09 19:45:08','2022-05-09 19:45:08'),(10,'Juan Daniel','Paco Salgado','77123466',99776444,'maiaarratia@gmail.com','Av. Pando Sur',1,'2022-05-09 19:47:47','2022-05-09 19:47:47'),(11,'Carlos Angel','Pérez Uriarte','77213321',10023433,'samueljacimto@hotmail.com','Av. Horizontes II',1,'2022-05-09 19:49:38','2022-05-09 19:49:38');
/*!40000 ALTER TABLE `clientes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `colors`
--

DROP TABLE IF EXISTS `colors`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `colors` (
  `idco` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombreco` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idco`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `colors`
--

LOCK TABLES `colors` WRITE;
/*!40000 ALTER TABLE `colors` DISABLE KEYS */;
INSERT INTO `colors` VALUES (1,'Blanco',1,NULL,NULL),(2,'Negro',1,NULL,NULL),(3,'Gris',1,NULL,NULL),(4,'Plata',1,NULL,NULL),(5,'Rojo',1,NULL,NULL),(6,'Azul',1,NULL,NULL),(7,'Dorado',1,NULL,NULL),(8,'Café',1,NULL,NULL),(9,'Verde',1,NULL,NULL);
/*!40000 ALTER TABLE `colors` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `failed_jobs`
--

DROP TABLE IF EXISTS `failed_jobs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `failed_jobs` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `failed_jobs`
--

LOCK TABLES `failed_jobs` WRITE;
/*!40000 ALTER TABLE `failed_jobs` DISABLE KEYS */;
/*!40000 ALTER TABLE `failed_jobs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manodeobras`
--

DROP TABLE IF EXISTS `manodeobras`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `manodeobras` (
  `idmano` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idorden` int(10) unsigned NOT NULL COMMENT 'es el idorden orden',
  `detalle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monto` int(11) NOT NULL DEFAULT 0,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idmano`),
  KEY `manodeobras_idorden_foreign` (`idorden`),
  CONSTRAINT `manodeobras_idorden_foreign` FOREIGN KEY (`idorden`) REFERENCES `ordentrabajos` (`idorden`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manodeobras`
--

LOCK TABLES `manodeobras` WRITE;
/*!40000 ALTER TABLE `manodeobras` DISABLE KEYS */;
INSERT INTO `manodeobras` VALUES (1,1,'Desarmo la trasmicion y cambio de kit de hermientas',1800,1,'2022-04-26 14:14:25','2022-04-26 14:14:25'),(2,1,'El cambio de retenes',300,1,'2022-04-26 14:15:02','2022-04-26 14:15:02'),(3,2,'Se arreglo los frenos de vehiculo correctamente',100,1,'2022-04-27 21:24:18','2022-04-27 21:24:18');
/*!40000 ALTER TABLE `manodeobras` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `marcas`
--

DROP TABLE IF EXISTS `marcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `marcas` (
  `idma` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marca` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idma`)
) ENGINE=InnoDB AUTO_INCREMENT=80 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `marcas`
--

LOCK TABLES `marcas` WRITE;
/*!40000 ALTER TABLE `marcas` DISABLE KEYS */;
INSERT INTO `marcas` VALUES (1,'Abarth',1,NULL,NULL),(2,'Alfa Romeo',1,NULL,NULL),(3,'Aro',1,NULL,NULL),(4,'Asia',1,NULL,NULL),(5,'Asia Motors',1,NULL,NULL),(6,'Aston Martin',1,NULL,NULL),(7,'Audi',1,NULL,NULL),(8,'Austin',1,NULL,NULL),(9,'Auverland',1,NULL,NULL),(10,'Bentley',1,NULL,NULL),(11,'Bertone',1,NULL,NULL),(12,'Bmw',1,NULL,NULL),(13,'Cadillac',1,NULL,NULL),(14,'Chevrolet',1,NULL,NULL),(15,'Chrysler',1,NULL,NULL),(16,'Citroen',1,NULL,NULL),(17,'Corvette',1,NULL,NULL),(18,'Dacia',1,NULL,NULL),(19,'Daewoo',1,NULL,NULL),(20,'Daf',1,NULL,NULL),(21,'Daihatsu',1,NULL,NULL),(22,'Daimler',1,NULL,NULL),(23,'Dodge',1,NULL,NULL),(24,'Ferrari',1,NULL,NULL),(25,'Fiat',1,NULL,NULL),(26,'Ford',1,NULL,NULL),(27,'Galloper',1,NULL,NULL),(28,'Gmc',1,NULL,NULL),(29,'Honda',1,NULL,NULL),(30,'Hummer',1,NULL,NULL),(31,'Hyundai',1,NULL,NULL),(32,'Infiniti',1,NULL,NULL),(33,'Innocenti',1,NULL,NULL),(34,'Isuzu',1,NULL,NULL),(35,'Iveco',1,NULL,NULL),(36,'Iveco-pegaso',1,NULL,NULL),(37,'Jaguar',1,NULL,NULL),(38,'Jeep',1,NULL,NULL),(39,'Kia',1,NULL,NULL),(40,'Lada',1,NULL,NULL),(41,'Lamborghini',1,NULL,NULL),(42,'Lancia',1,NULL,NULL),(43,'Land-rover',1,NULL,NULL),(44,'Ldv',1,NULL,NULL),(45,'Lexus',1,NULL,NULL),(46,'Lotus',1,NULL,NULL),(47,'Mahindra',1,NULL,NULL),(48,'Maserati',1,NULL,NULL),(49,'Maybach',1,NULL,NULL),(50,'Mazda',1,NULL,NULL),(51,'Mercedes-benz',1,NULL,NULL),(52,'Mg',1,NULL,NULL),(53,'Mini',1,NULL,NULL),(54,'Mitsubishi',1,NULL,NULL),(55,'Morgan',1,NULL,NULL),(56,'Nissan',1,NULL,NULL),(57,'Opel',1,NULL,NULL),(58,'Peugeot',1,NULL,NULL),(59,'Pontiac',1,NULL,NULL),(60,'Porsche',1,NULL,NULL),(61,'Renault',1,NULL,NULL),(62,'Rolls-royce',1,NULL,NULL),(63,'Rover',1,NULL,NULL),(64,'Saab',1,NULL,NULL),(65,'Santana',1,NULL,NULL),(66,'Seat',1,NULL,NULL),(67,'Skoda',1,NULL,NULL),(68,'Smart',1,NULL,NULL),(69,'Ssangyong',1,NULL,NULL),(70,'Subaru',1,NULL,NULL),(71,'Suzuki',1,NULL,NULL),(72,'Talbot',1,NULL,NULL),(73,'Tata',1,NULL,NULL),(74,'Toyota',1,NULL,NULL),(75,'Umm',1,NULL,NULL),(76,'Vaz',1,NULL,NULL),(77,'Volkswagen',1,NULL,NULL),(78,'Volvo',1,NULL,NULL),(79,'Wartburg',1,NULL,NULL);
/*!40000 ALTER TABLE `marcas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `menus`
--

DROP TABLE IF EXISTS `menus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `menus` (
  `idmenu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nombremenu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idmenu`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `menus`
--

LOCK TABLES `menus` WRITE;
/*!40000 ALTER TABLE `menus` DISABLE KEYS */;
INSERT INTO `menus` VALUES (1,'Clientes','dw-user2',1,NULL,NULL),(2,'Vehiculos','dw-car',1,NULL,NULL),(3,'Servicios','dw-layers',1,NULL,NULL),(4,'Pagos','dw-money-2',1,NULL,NULL);
/*!40000 ALTER TABLE `menus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES (1,'2014_03_20_162247_create_tipo_users_table',1),(2,'2014_10_12_000000_create_users_table',1),(3,'2014_10_12_100000_create_password_resets_table',1),(4,'2019_08_19_000000_create_failed_jobs_table',1),(5,'2019_12_14_000001_create_personal_access_tokens_table',1),(6,'2021_03_21_220009_create_menus_table',1),(7,'2021_03_21_220019_create_submenus_table',1),(8,'2022_03_21_214822_create_clientes_table',1),(9,'2022_03_23_022115_create_colors_table',1),(10,'2022_03_24_000519_create_marcas_table',1),(11,'2022_03_24_000539_create_modelos_table',1),(12,'2022_03_24_012740_create_vehiculos_table',1),(13,'2022_03_24_041247_create_recepcions_table',1),(14,'2022_03_24_131049_create_ordentrabajos_table',1),(15,'2022_03_24_140804_create_manodeobras_table',1),(16,'2022_03_24_140819_create_repuestos_table',1),(17,'2022_03_24_162003_create_reparacions_table',1),(18,'2022_03_24_165746_create_pagos_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `modelos`
--

DROP TABLE IF EXISTS `modelos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `modelos` (
  `idmod` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `marca` int(10) unsigned NOT NULL COMMENT 'es el idma marca',
  `modelo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idmod`),
  KEY `modelos_marca_foreign` (`marca`),
  CONSTRAINT `modelos_marca_foreign` FOREIGN KEY (`marca`) REFERENCES `marcas` (`idma`)
) ENGINE=InnoDB AUTO_INCREMENT=1009 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `modelos`
--

LOCK TABLES `modelos` WRITE;
/*!40000 ALTER TABLE `modelos` DISABLE KEYS */;
INSERT INTO `modelos` VALUES (1,1,'500',1,NULL,NULL),(2,1,'Grande Punto',1,NULL,NULL),(3,1,'Punto Evo',1,NULL,NULL),(4,1,'500c',1,NULL,NULL),(5,1,'695',1,NULL,NULL),(6,1,'Punto',1,NULL,NULL),(7,2,'155',1,NULL,NULL),(8,2,'156',1,NULL,NULL),(9,2,'159',1,NULL,NULL),(10,2,'164',1,NULL,NULL),(11,2,'145',1,NULL,NULL),(12,2,'147',1,NULL,NULL),(13,2,'146',1,NULL,NULL),(14,2,'Gtv',1,NULL,NULL),(15,2,'Spider',1,NULL,NULL),(16,2,'166',1,NULL,NULL),(17,2,'Gt',1,NULL,NULL),(18,2,'Crosswagon',1,NULL,NULL),(19,2,'Brera',1,NULL,NULL),(20,2,'90',1,NULL,NULL),(21,2,'75',1,NULL,NULL),(22,2,'33',1,NULL,NULL),(23,2,'Giulietta',1,NULL,NULL),(24,2,'Sprint',1,NULL,NULL),(25,2,'Mito',1,NULL,NULL),(26,3,'Expander',1,NULL,NULL),(27,3,'10',1,NULL,NULL),(28,3,'24',1,NULL,NULL),(29,3,'Dacia',1,NULL,NULL),(30,4,'Rocsta',1,NULL,NULL),(31,5,'Rocsta',1,NULL,NULL),(32,6,'Db7',1,NULL,NULL),(33,6,'V8',1,NULL,NULL),(34,6,'Db9',1,NULL,NULL),(35,6,'Vanquish',1,NULL,NULL),(36,6,'V8 Vantage',1,NULL,NULL),(37,6,'Vantage',1,NULL,NULL),(38,6,'Dbs',1,NULL,NULL),(39,6,'Volante',1,NULL,NULL),(40,6,'Virage',1,NULL,NULL),(41,6,'Vantage V8',1,NULL,NULL),(42,6,'Vantage V12',1,NULL,NULL),(43,6,'Rapide',1,NULL,NULL),(44,6,'Cygnet',1,NULL,NULL),(45,7,'80',1,NULL,NULL),(46,7,'A4',1,NULL,NULL),(47,7,'A6',1,NULL,NULL),(48,7,'S6',1,NULL,NULL),(49,7,'Coupe',1,NULL,NULL),(50,7,'S2',1,NULL,NULL),(51,7,'Rs2',1,NULL,NULL),(52,7,'A8',1,NULL,NULL),(53,7,'Cabriolet',1,NULL,NULL),(54,7,'S8',1,NULL,NULL),(55,7,'A3',1,NULL,NULL),(56,7,'S4',1,NULL,NULL),(57,7,'Tt',1,NULL,NULL),(58,7,'S3',1,NULL,NULL),(59,7,'Allroad Quattro',1,NULL,NULL),(60,7,'Rs4',1,NULL,NULL),(61,7,'A2',1,NULL,NULL),(62,7,'Rs6',1,NULL,NULL),(63,7,'Q7',1,NULL,NULL),(64,7,'R8',1,NULL,NULL),(65,7,'A5',1,NULL,NULL),(66,7,'S5',1,NULL,NULL),(67,7,'V8',1,NULL,NULL),(68,7,'200',1,NULL,NULL),(69,7,'100',1,NULL,NULL),(70,7,'90',1,NULL,NULL),(71,7,'Tts',1,NULL,NULL),(72,7,'Q5',1,NULL,NULL),(73,7,'A4 Allroad Quattro',1,NULL,NULL),(74,7,'Tt Rs',1,NULL,NULL),(75,7,'Rs5',1,NULL,NULL),(76,7,'A1',1,NULL,NULL),(77,7,'A7',1,NULL,NULL),(78,7,'Rs3',1,NULL,NULL),(79,7,'Q3',1,NULL,NULL),(80,7,'A6 Allroad Quattro',1,NULL,NULL),(81,7,'S7',1,NULL,NULL),(82,7,'Sq5',1,NULL,NULL),(83,8,'Mini',1,NULL,NULL),(84,8,'Montego',1,NULL,NULL),(85,8,'Maestro',1,NULL,NULL),(86,8,'Metro',1,NULL,NULL),(87,8,'Mini Moke',1,NULL,NULL),(88,9,'Diesel',1,NULL,NULL),(89,10,'Brooklands',1,NULL,NULL),(90,10,'Turbo',1,NULL,NULL),(91,10,'Continental',1,NULL,NULL),(92,10,'Azure',1,NULL,NULL),(93,10,'Arnage',1,NULL,NULL),(94,10,'Continental Gt',1,NULL,NULL),(95,10,'Continental Flying Spur',1,NULL,NULL),(96,10,'Turbo R',1,NULL,NULL),(97,10,'Mulsanne',1,NULL,NULL),(98,10,'Eight',1,NULL,NULL),(99,10,'Continental Gtc',1,NULL,NULL),(100,10,'Continental Supersports',1,NULL,NULL),(101,11,'Freeclimber Diesel',1,NULL,NULL),(102,12,'Serie 3',1,NULL,NULL),(103,12,'Serie 5',1,NULL,NULL),(104,12,'Compact',1,NULL,NULL),(105,12,'Serie 7',1,NULL,NULL),(106,12,'Serie 8',1,NULL,NULL),(107,12,'Z3',1,NULL,NULL),(108,12,'Z4',1,NULL,NULL),(109,12,'Z8',1,NULL,NULL),(110,12,'X5',1,NULL,NULL),(111,12,'Serie 6',1,NULL,NULL),(112,12,'X3',1,NULL,NULL),(113,12,'Serie 1',1,NULL,NULL),(114,12,'Z1',1,NULL,NULL),(115,12,'X6',1,NULL,NULL),(116,12,'X1',1,NULL,NULL),(117,13,'Seville',1,NULL,NULL),(118,13,'Sts',1,NULL,NULL),(119,13,'El Dorado',1,NULL,NULL),(120,13,'Cts',1,NULL,NULL),(121,13,'Xlr',1,NULL,NULL),(122,13,'Srx',1,NULL,NULL),(123,13,'Escalade',1,NULL,NULL),(124,13,'Bls',1,NULL,NULL),(125,14,'Corvette',1,NULL,NULL),(126,14,'Blazer',1,NULL,NULL),(127,14,'Astro',1,NULL,NULL),(128,14,'Nubira',1,NULL,NULL),(129,14,'Evanda',1,NULL,NULL),(130,14,'Trans Sport',1,NULL,NULL),(131,14,'Camaro',1,NULL,NULL),(132,14,'Matiz',1,NULL,NULL),(133,14,'Alero',1,NULL,NULL),(134,14,'Tahoe',1,NULL,NULL),(135,14,'Tacuma',1,NULL,NULL),(136,14,'Trailblazer',1,NULL,NULL),(137,14,'Kalos',1,NULL,NULL),(138,14,'Aveo',1,NULL,NULL),(139,14,'Lacetti',1,NULL,NULL),(140,14,'Epica',1,NULL,NULL),(141,14,'Captiva',1,NULL,NULL),(142,14,'Hhr',1,NULL,NULL),(143,14,'Cruze',1,NULL,NULL),(144,14,'Spark',1,NULL,NULL),(145,14,'Orlando',1,NULL,NULL),(146,14,'Volt',1,NULL,NULL),(147,14,'Malibu',1,NULL,NULL),(148,15,'Vision',1,NULL,NULL),(149,15,'300m',1,NULL,NULL),(150,15,'Grand Voyager',1,NULL,NULL),(151,15,'Viper',1,NULL,NULL),(152,15,'Neon',1,NULL,NULL),(153,15,'Voyager',1,NULL,NULL),(154,15,'Stratus',1,NULL,NULL),(155,15,'Sebring',1,NULL,NULL),(156,15,'Sebring 200c',1,NULL,NULL),(157,15,'New Yorker',1,NULL,NULL),(158,15,'Pt Cruiser',1,NULL,NULL),(159,15,'Crossfire',1,NULL,NULL),(160,15,'300c',1,NULL,NULL),(161,15,'Le Baron',1,NULL,NULL),(162,15,'Saratoga',1,NULL,NULL),(163,16,'Xantia',1,NULL,NULL),(164,16,'Xm',1,NULL,NULL),(165,16,'Ax',1,NULL,NULL),(166,16,'Zx',1,NULL,NULL),(167,16,'Evasion',1,NULL,NULL),(168,16,'C8',1,NULL,NULL),(169,16,'Saxo',1,NULL,NULL),(170,16,'C2',1,NULL,NULL),(171,16,'Xsara',1,NULL,NULL),(172,16,'C4',1,NULL,NULL),(173,16,'Xsara Picasso',1,NULL,NULL),(174,16,'C5',1,NULL,NULL),(175,16,'C3',1,NULL,NULL),(176,16,'C3 Pluriel',1,NULL,NULL),(177,16,'C1',1,NULL,NULL),(178,16,'C6',1,NULL,NULL),(179,16,'Grand C4 Picasso',1,NULL,NULL),(180,16,'C4 Picasso',1,NULL,NULL),(181,16,'Ccrosser',1,NULL,NULL),(182,16,'C15',1,NULL,NULL),(183,16,'Jumper',1,NULL,NULL),(184,16,'Jumpy',1,NULL,NULL),(185,16,'Berlingo',1,NULL,NULL),(186,16,'Bx',1,NULL,NULL),(187,16,'C25',1,NULL,NULL),(188,16,'Cx',1,NULL,NULL),(189,16,'Gsa',1,NULL,NULL),(190,16,'Visa',1,NULL,NULL),(191,16,'Lna',1,NULL,NULL),(192,16,'2cv',1,NULL,NULL),(193,16,'Nemo',1,NULL,NULL),(194,16,'C4 Sedan',1,NULL,NULL),(195,16,'Berlingo First',1,NULL,NULL),(196,16,'C3 Picasso',1,NULL,NULL),(197,16,'Ds3',1,NULL,NULL),(198,16,'Czero',1,NULL,NULL),(199,16,'Ds4',1,NULL,NULL),(200,16,'Ds5',1,NULL,NULL),(201,16,'C4 Aircross',1,NULL,NULL),(202,16,'Celysee',1,NULL,NULL),(203,17,'Corvette',1,NULL,NULL),(204,18,'Contac',1,NULL,NULL),(205,18,'Logan',1,NULL,NULL),(206,18,'Sandero',1,NULL,NULL),(207,18,'Duster',1,NULL,NULL),(208,18,'Lodgy',1,NULL,NULL),(209,19,'Nexia',1,NULL,NULL),(210,19,'Aranos',1,NULL,NULL),(211,19,'Lanos',1,NULL,NULL),(212,19,'Nubira',1,NULL,NULL),(213,19,'Compact',1,NULL,NULL),(214,19,'Nubira Compact',1,NULL,NULL),(215,19,'Leganza',1,NULL,NULL),(216,19,'Evanda',1,NULL,NULL),(217,19,'Matiz',1,NULL,NULL),(218,19,'Tacuma',1,NULL,NULL),(219,19,'Kalos',1,NULL,NULL),(220,19,'Lacetti',1,NULL,NULL),(221,21,'Applause',1,NULL,NULL),(222,21,'Charade',1,NULL,NULL),(223,21,'Rocky',1,NULL,NULL),(224,21,'Feroza',1,NULL,NULL),(225,21,'Terios',1,NULL,NULL),(226,21,'Sirion',1,NULL,NULL),(227,22,'Serie Xj',1,NULL,NULL),(228,22,'Xj',1,NULL,NULL),(229,22,'Double Six',1,NULL,NULL),(230,22,'Six',1,NULL,NULL),(231,22,'Series Iii',1,NULL,NULL),(232,23,'Viper',1,NULL,NULL),(233,23,'Caliber',1,NULL,NULL),(234,23,'Nitro',1,NULL,NULL),(235,23,'Avenger',1,NULL,NULL),(236,23,'Journey',1,NULL,NULL),(237,24,'F355',1,NULL,NULL),(238,24,'360',1,NULL,NULL),(239,24,'F430',1,NULL,NULL),(240,24,'F512 M',1,NULL,NULL),(241,24,'550 Maranello',1,NULL,NULL),(242,24,'575m Maranello',1,NULL,NULL),(243,24,'599',1,NULL,NULL),(244,24,'456',1,NULL,NULL),(245,24,'456m',1,NULL,NULL),(246,24,'612',1,NULL,NULL),(247,24,'F50',1,NULL,NULL),(248,24,'Enzo',1,NULL,NULL),(249,24,'Superamerica',1,NULL,NULL),(250,24,'430',1,NULL,NULL),(251,24,'348',1,NULL,NULL),(252,24,'Testarossa',1,NULL,NULL),(253,24,'512',1,NULL,NULL),(254,24,'355',1,NULL,NULL),(255,24,'F40',1,NULL,NULL),(256,24,'412',1,NULL,NULL),(257,24,'Mondial',1,NULL,NULL),(258,24,'328',1,NULL,NULL),(259,24,'California',1,NULL,NULL),(260,24,'458',1,NULL,NULL),(261,24,'Ff',1,NULL,NULL),(262,25,'Croma',1,NULL,NULL),(263,25,'Cinquecento',1,NULL,NULL),(264,25,'Seicento',1,NULL,NULL),(265,25,'Punto',1,NULL,NULL),(266,25,'Grande Punto',1,NULL,NULL),(267,25,'Panda',1,NULL,NULL),(268,25,'Tipo',1,NULL,NULL),(269,25,'Coupe',1,NULL,NULL),(270,25,'Uno',1,NULL,NULL),(271,25,'Ulysse',1,NULL,NULL),(272,25,'Tempra',1,NULL,NULL),(273,25,'Marea',1,NULL,NULL),(274,25,'Barchetta',1,NULL,NULL),(275,25,'Bravo',1,NULL,NULL),(276,25,'Stilo',1,NULL,NULL),(277,25,'Brava',1,NULL,NULL),(278,25,'Palio Weekend',1,NULL,NULL),(279,25,'600',1,NULL,NULL),(280,25,'Multipla',1,NULL,NULL),(281,25,'Idea',1,NULL,NULL),(282,25,'Sedici',1,NULL,NULL),(283,25,'Linea',1,NULL,NULL),(284,25,'500',1,NULL,NULL),(285,25,'Fiorino',1,NULL,NULL),(286,25,'Ducato',1,NULL,NULL),(287,25,'Doblo Cargo',1,NULL,NULL),(288,25,'Doblo',1,NULL,NULL),(289,25,'Strada',1,NULL,NULL),(290,25,'Regata',1,NULL,NULL),(291,25,'Talento',1,NULL,NULL),(292,25,'Argenta',1,NULL,NULL),(293,25,'Ritmo',1,NULL,NULL),(294,25,'Punto Classic',1,NULL,NULL),(295,25,'Qubo',1,NULL,NULL),(296,25,'Punto Evo',1,NULL,NULL),(297,25,'500c',1,NULL,NULL),(298,25,'Freemont',1,NULL,NULL),(299,25,'Panda Classic',1,NULL,NULL),(300,25,'500l',1,NULL,NULL),(301,26,'Maverick',1,NULL,NULL),(302,26,'Escort',1,NULL,NULL),(303,26,'Focus',1,NULL,NULL),(304,26,'Mondeo',1,NULL,NULL),(305,26,'Scorpio',1,NULL,NULL),(306,26,'Fiesta',1,NULL,NULL),(307,26,'Probe',1,NULL,NULL),(308,26,'Explorer',1,NULL,NULL),(309,26,'Galaxy',1,NULL,NULL),(310,26,'Ka',1,NULL,NULL),(311,26,'Puma',1,NULL,NULL),(312,26,'Cougar',1,NULL,NULL),(313,26,'Focus Cmax',1,NULL,NULL),(314,26,'Fusion',1,NULL,NULL),(315,26,'Streetka',1,NULL,NULL),(316,26,'Cmax',1,NULL,NULL),(317,26,'Smax',1,NULL,NULL),(318,26,'Transit',1,NULL,NULL),(319,26,'Courier',1,NULL,NULL),(320,26,'Ranger',1,NULL,NULL),(321,26,'Sierra',1,NULL,NULL),(322,26,'Orion',1,NULL,NULL),(323,26,'Pick Up',1,NULL,NULL),(324,26,'Capri',1,NULL,NULL),(325,26,'Granada',1,NULL,NULL),(326,26,'Kuga',1,NULL,NULL),(327,26,'Grand Cmax',1,NULL,NULL),(328,26,'Bmax',1,NULL,NULL),(329,26,'Tourneo Custom',1,NULL,NULL),(330,27,'Exceed',1,NULL,NULL),(331,27,'Santamo',1,NULL,NULL),(332,27,'Super Exceed',1,NULL,NULL),(333,29,'Accord',1,NULL,NULL),(334,29,'Civic',1,NULL,NULL),(335,29,'Crx',1,NULL,NULL),(336,29,'Prelude',1,NULL,NULL),(337,29,'Nsx',1,NULL,NULL),(338,29,'Legend',1,NULL,NULL),(339,29,'Crv',1,NULL,NULL),(340,29,'Hrv',1,NULL,NULL),(341,29,'Logo',1,NULL,NULL),(342,29,'S2000',1,NULL,NULL),(343,29,'Stream',1,NULL,NULL),(344,29,'Jazz',1,NULL,NULL),(345,29,'Frv',1,NULL,NULL),(346,29,'Concerto',1,NULL,NULL),(347,29,'Insight',1,NULL,NULL),(348,29,'Crz',1,NULL,NULL),(349,30,'H2',1,NULL,NULL),(350,30,'H3',1,NULL,NULL),(351,30,'H3t',1,NULL,NULL),(352,31,'Lantra',1,NULL,NULL),(353,31,'Sonata',1,NULL,NULL),(354,31,'Elantra',1,NULL,NULL),(355,31,'Accent',1,NULL,NULL),(356,31,'Scoupe',1,NULL,NULL),(357,31,'Coupe',1,NULL,NULL),(358,31,'Atos',1,NULL,NULL),(359,31,'H1',1,NULL,NULL),(360,31,'Atos Prime',1,NULL,NULL),(361,31,'Xg',1,NULL,NULL),(362,31,'Trajet',1,NULL,NULL),(363,31,'Santa Fe',1,NULL,NULL),(364,31,'Terracan',1,NULL,NULL),(365,31,'Matrix',1,NULL,NULL),(366,31,'Getz',1,NULL,NULL),(367,31,'Tucson',1,NULL,NULL),(368,31,'I30',1,NULL,NULL),(369,31,'Pony',1,NULL,NULL),(370,31,'Grandeur',1,NULL,NULL),(371,31,'I10',1,NULL,NULL),(372,31,'I800',1,NULL,NULL),(373,31,'Sonata Fl',1,NULL,NULL),(374,31,'Ix55',1,NULL,NULL),(375,31,'I20',1,NULL,NULL),(376,31,'Ix35',1,NULL,NULL),(377,31,'Ix20',1,NULL,NULL),(378,31,'Genesis',1,NULL,NULL),(379,31,'I40',1,NULL,NULL),(380,31,'Veloster',1,NULL,NULL),(381,32,'G',1,NULL,NULL),(382,32,'Ex',1,NULL,NULL),(383,32,'Fx',1,NULL,NULL),(384,32,'M',1,NULL,NULL),(385,33,'Elba',1,NULL,NULL),(386,33,'Minitre',1,NULL,NULL),(387,34,'Trooper',1,NULL,NULL),(388,34,'Pick Up',1,NULL,NULL),(389,34,'D Max',1,NULL,NULL),(390,34,'Rodeo',1,NULL,NULL),(391,34,'Dmax',1,NULL,NULL),(392,34,'Trroper',1,NULL,NULL),(393,35,'Daily',1,NULL,NULL),(394,35,'Massif',1,NULL,NULL),(395,36,'Daily',1,NULL,NULL),(396,36,'Duty',1,NULL,NULL),(397,37,'Serie Xj',1,NULL,NULL),(398,37,'Serie Xk',1,NULL,NULL),(399,37,'Xj',1,NULL,NULL),(400,37,'Stype',1,NULL,NULL),(401,37,'Xf',1,NULL,NULL),(402,37,'Xtype',1,NULL,NULL),(403,38,'Wrangler',1,NULL,NULL),(404,38,'Cherokee',1,NULL,NULL),(405,38,'Grand Cherokee',1,NULL,NULL),(406,38,'Commander',1,NULL,NULL),(407,38,'Compass',1,NULL,NULL),(408,38,'Wrangler Unlimited',1,NULL,NULL),(409,38,'Patriot',1,NULL,NULL),(410,39,'Sportage',1,NULL,NULL),(411,39,'Sephia',1,NULL,NULL),(412,39,'Sephia Ii',1,NULL,NULL),(413,39,'Pride',1,NULL,NULL),(414,39,'Clarus',1,NULL,NULL),(415,39,'Shuma',1,NULL,NULL),(416,39,'Carnival',1,NULL,NULL),(417,39,'Joice',1,NULL,NULL),(418,39,'Magentis',1,NULL,NULL),(419,39,'Carens',1,NULL,NULL),(420,39,'Rio',1,NULL,NULL),(421,39,'Cerato',1,NULL,NULL),(422,39,'Sorento',1,NULL,NULL),(423,39,'Opirus',1,NULL,NULL),(424,39,'Picanto',1,NULL,NULL),(425,39,'Ceed',1,NULL,NULL),(426,39,'Ceed Sporty Wagon',1,NULL,NULL),(427,39,'Proceed',1,NULL,NULL),(428,39,'K2500 Frontier',1,NULL,NULL),(429,39,'K2500',1,NULL,NULL),(430,39,'Soul',1,NULL,NULL),(431,39,'Venga',1,NULL,NULL),(432,39,'Optima',1,NULL,NULL),(433,39,'Ceed Sportswagon',1,NULL,NULL),(434,40,'Samara',1,NULL,NULL),(435,40,'Niva',1,NULL,NULL),(436,40,'Sagona',1,NULL,NULL),(437,40,'Stawra 2110',1,NULL,NULL),(438,40,'214',1,NULL,NULL),(439,40,'Kalina',1,NULL,NULL),(440,40,'Serie 2100',1,NULL,NULL),(441,40,'Priora',1,NULL,NULL),(442,41,'Gallardo',1,NULL,NULL),(443,41,'Murcielago',1,NULL,NULL),(444,41,'Aventador',1,NULL,NULL),(445,42,'Delta',1,NULL,NULL),(446,42,'K',1,NULL,NULL),(447,42,'Y10',1,NULL,NULL),(448,42,'Dedra',1,NULL,NULL),(449,42,'Lybra',1,NULL,NULL),(450,42,'Z',1,NULL,NULL),(451,42,'Y',1,NULL,NULL),(452,42,'Ypsilon',1,NULL,NULL),(453,42,'Thesis',1,NULL,NULL),(454,42,'Phedra',1,NULL,NULL),(455,42,'Musa',1,NULL,NULL),(456,42,'Thema',1,NULL,NULL),(457,42,'Zeta',1,NULL,NULL),(458,42,'Kappa',1,NULL,NULL),(459,42,'Trevi',1,NULL,NULL),(460,42,'Prisma',1,NULL,NULL),(461,42,'A112',1,NULL,NULL),(462,42,'Ypsilon Elefantino',1,NULL,NULL),(463,42,'Voyager',1,NULL,NULL),(464,43,'Range Rover',1,NULL,NULL),(465,43,'Defender',1,NULL,NULL),(466,43,'Discovery',1,NULL,NULL),(467,43,'Freelander',1,NULL,NULL),(468,43,'Range Rover Sport',1,NULL,NULL),(469,43,'Discovery 4',1,NULL,NULL),(470,43,'Range Rover Evoque',1,NULL,NULL),(471,44,'Maxus',1,NULL,NULL),(472,45,'Ls400',1,NULL,NULL),(473,45,'Ls430',1,NULL,NULL),(474,45,'Gs300',1,NULL,NULL),(475,45,'Is200',1,NULL,NULL),(476,45,'Rx300',1,NULL,NULL),(477,45,'Gs430',1,NULL,NULL),(478,45,'Gs460',1,NULL,NULL),(479,45,'Sc430',1,NULL,NULL),(480,45,'Is300',1,NULL,NULL),(481,45,'Is250',1,NULL,NULL),(482,45,'Rx400h',1,NULL,NULL),(483,45,'Is220d',1,NULL,NULL),(484,45,'Rx350',1,NULL,NULL),(485,45,'Gs450h',1,NULL,NULL),(486,45,'Ls460',1,NULL,NULL),(487,45,'Ls600h',1,NULL,NULL),(488,45,'Ls',1,NULL,NULL),(489,45,'Gs',1,NULL,NULL),(490,45,'Is',1,NULL,NULL),(491,45,'Sc',1,NULL,NULL),(492,45,'Rx',1,NULL,NULL),(493,45,'Ct',1,NULL,NULL),(494,46,'Elise',1,NULL,NULL),(495,46,'Exige',1,NULL,NULL),(496,47,'Bolero Pickup',1,NULL,NULL),(497,47,'Goa Pickup',1,NULL,NULL),(498,47,'Goa',1,NULL,NULL),(499,47,'Cj',1,NULL,NULL),(500,47,'Pikup',1,NULL,NULL),(501,47,'Thar',1,NULL,NULL),(502,48,'Ghibli',1,NULL,NULL),(503,48,'Shamal',1,NULL,NULL),(504,48,'Quattroporte',1,NULL,NULL),(505,48,'3200 Gt',1,NULL,NULL),(506,48,'Coupe',1,NULL,NULL),(507,48,'Spyder',1,NULL,NULL),(508,48,'Gransport',1,NULL,NULL),(509,48,'Granturismo',1,NULL,NULL),(510,48,'430',1,NULL,NULL),(511,48,'Biturbo',1,NULL,NULL),(512,48,'228',1,NULL,NULL),(513,48,'224',1,NULL,NULL),(514,48,'Grancabrio',1,NULL,NULL),(515,49,'Maybach',1,NULL,NULL),(516,50,'Xedos 6',1,NULL,NULL),(517,50,'626',1,NULL,NULL),(518,50,'121',1,NULL,NULL),(519,50,'Xedos 9',1,NULL,NULL),(520,50,'323',1,NULL,NULL),(521,50,'Mx3',1,NULL,NULL),(522,50,'Rx7',1,NULL,NULL),(523,50,'Mx5',1,NULL,NULL),(524,50,'Mazda3',1,NULL,NULL),(525,50,'Mpv',1,NULL,NULL),(526,50,'Demio',1,NULL,NULL),(527,50,'Premacy',1,NULL,NULL),(528,50,'Tribute',1,NULL,NULL),(529,50,'Mazda6',1,NULL,NULL),(530,50,'Mazda2',1,NULL,NULL),(531,50,'Rx8',1,NULL,NULL),(532,50,'Mazda5',1,NULL,NULL),(533,50,'Cx7',1,NULL,NULL),(534,50,'Serie B',1,NULL,NULL),(535,50,'B2500',1,NULL,NULL),(536,50,'Bt50',1,NULL,NULL),(537,50,'Mx6',1,NULL,NULL),(538,50,'929',1,NULL,NULL),(539,50,'Cx5',1,NULL,NULL),(540,51,'Clase C',1,NULL,NULL),(541,51,'Clase E',1,NULL,NULL),(542,51,'Clase Sl',1,NULL,NULL),(543,51,'Clase S',1,NULL,NULL),(544,51,'Clase Cl',1,NULL,NULL),(545,51,'Clase G',1,NULL,NULL),(546,51,'Clase Slk',1,NULL,NULL),(547,51,'Clase V',1,NULL,NULL),(548,51,'Viano',1,NULL,NULL),(549,51,'Clase Clk',1,NULL,NULL),(550,51,'Clase A',1,NULL,NULL),(551,51,'Clase M',1,NULL,NULL),(552,51,'Vaneo',1,NULL,NULL),(553,51,'Slklasse',1,NULL,NULL),(554,51,'Slr Mclaren',1,NULL,NULL),(555,51,'Clase Cls',1,NULL,NULL),(556,51,'Clase R',1,NULL,NULL),(557,51,'Clase Gl',1,NULL,NULL),(558,51,'Clase B',1,NULL,NULL),(559,51,'100d',1,NULL,NULL),(560,51,'140d',1,NULL,NULL),(561,51,'180d',1,NULL,NULL),(562,51,'Sprinter',1,NULL,NULL),(563,51,'Vito',1,NULL,NULL),(564,51,'Transporter',1,NULL,NULL),(565,51,'280',1,NULL,NULL),(566,51,'220',1,NULL,NULL),(567,51,'200',1,NULL,NULL),(568,51,'190',1,NULL,NULL),(569,51,'600',1,NULL,NULL),(570,51,'400',1,NULL,NULL),(571,51,'Clase Sl R129',1,NULL,NULL),(572,51,'300',1,NULL,NULL),(573,51,'500',1,NULL,NULL),(574,51,'420',1,NULL,NULL),(575,51,'260',1,NULL,NULL),(576,51,'230',1,NULL,NULL),(577,51,'Clase Clc',1,NULL,NULL),(578,51,'Clase Glk',1,NULL,NULL),(579,51,'Sls Amg',1,NULL,NULL),(580,52,'Mgf',1,NULL,NULL),(581,52,'Tf',1,NULL,NULL),(582,52,'Zr',1,NULL,NULL),(583,52,'Zs',1,NULL,NULL),(584,52,'Zt',1,NULL,NULL),(585,52,'Ztt',1,NULL,NULL),(586,52,'Mini',1,NULL,NULL),(587,52,'Countryman',1,NULL,NULL),(588,52,'Paceman',1,NULL,NULL),(589,54,'Montero',1,NULL,NULL),(590,54,'Galant',1,NULL,NULL),(591,54,'Colt',1,NULL,NULL),(592,54,'Space Wagon',1,NULL,NULL),(593,54,'Space Runner',1,NULL,NULL),(594,54,'Space Gear',1,NULL,NULL),(595,54,'3000 Gt',1,NULL,NULL),(596,54,'Carisma',1,NULL,NULL),(597,54,'Eclipse',1,NULL,NULL),(598,54,'Space Star',1,NULL,NULL),(599,54,'Montero Sport',1,NULL,NULL),(600,54,'Montero Io',1,NULL,NULL),(601,54,'Outlander',1,NULL,NULL),(602,54,'Lancer',1,NULL,NULL),(603,54,'Grandis',1,NULL,NULL),(604,54,'L200',1,NULL,NULL),(605,54,'Canter',1,NULL,NULL),(606,54,'300 Gt',1,NULL,NULL),(607,54,'Asx',1,NULL,NULL),(608,54,'Imiev',1,NULL,NULL),(609,55,'44',1,NULL,NULL),(610,55,'Plus 8',1,NULL,NULL),(611,55,'Aero 8',1,NULL,NULL),(612,55,'V6',1,NULL,NULL),(613,55,'Roadster',1,NULL,NULL),(614,55,'4',1,NULL,NULL),(615,55,'Plus 4',1,NULL,NULL),(616,56,'Terrano Ii',1,NULL,NULL),(617,56,'Terrano',1,NULL,NULL),(618,56,'Micra',1,NULL,NULL),(619,56,'Sunny',1,NULL,NULL),(620,56,'Primera',1,NULL,NULL),(621,56,'Serena',1,NULL,NULL),(622,56,'Patrol',1,NULL,NULL),(623,56,'Maxima Qx',1,NULL,NULL),(624,56,'200 Sx',1,NULL,NULL),(625,56,'300 Zx',1,NULL,NULL),(626,56,'Patrol Gr',1,NULL,NULL),(627,56,'100 Nx',1,NULL,NULL),(628,56,'Almera',1,NULL,NULL),(629,56,'Pathfinder',1,NULL,NULL),(630,56,'Almera Tino',1,NULL,NULL),(631,56,'Xtrail',1,NULL,NULL),(632,56,'350z',1,NULL,NULL),(633,56,'Murano',1,NULL,NULL),(634,56,'Note',1,NULL,NULL),(635,56,'Qashqai',1,NULL,NULL),(636,56,'Tiida',1,NULL,NULL),(637,56,'Vanette',1,NULL,NULL),(638,56,'Trade',1,NULL,NULL),(639,56,'Vanette Cargo',1,NULL,NULL),(640,56,'Pickup',1,NULL,NULL),(641,56,'Navara',1,NULL,NULL),(642,56,'Cabstar E',1,NULL,NULL),(643,56,'Cabstar',1,NULL,NULL),(644,56,'Maxima',1,NULL,NULL),(645,56,'Camion',1,NULL,NULL),(646,56,'Prairie',1,NULL,NULL),(647,56,'Bluebird',1,NULL,NULL),(648,56,'Np300 Pick Up',1,NULL,NULL),(649,56,'Qashqai2',1,NULL,NULL),(650,56,'Pixo',1,NULL,NULL),(651,56,'Gtr',1,NULL,NULL),(652,56,'370z',1,NULL,NULL),(653,56,'Cube',1,NULL,NULL),(654,56,'Juke',1,NULL,NULL),(655,56,'Leaf',1,NULL,NULL),(656,56,'Evalia',1,NULL,NULL),(657,57,'Astra',1,NULL,NULL),(658,57,'Vectra',1,NULL,NULL),(659,57,'Calibra',1,NULL,NULL),(660,57,'Corsa',1,NULL,NULL),(661,57,'Omega',1,NULL,NULL),(662,57,'Frontera',1,NULL,NULL),(663,57,'Tigra',1,NULL,NULL),(664,57,'Monterey',1,NULL,NULL),(665,57,'Sintra',1,NULL,NULL),(666,57,'Zafira',1,NULL,NULL),(667,57,'Agila',1,NULL,NULL),(668,57,'Speedster',1,NULL,NULL),(669,57,'Signum',1,NULL,NULL),(670,57,'Meriva',1,NULL,NULL),(671,57,'Antara',1,NULL,NULL),(672,57,'Gt',1,NULL,NULL),(673,57,'Combo',1,NULL,NULL),(674,57,'Movano',1,NULL,NULL),(675,57,'Vivaro',1,NULL,NULL),(676,57,'Kadett',1,NULL,NULL),(677,57,'Monza',1,NULL,NULL),(678,57,'Senator',1,NULL,NULL),(679,57,'Rekord',1,NULL,NULL),(680,57,'Manta',1,NULL,NULL),(681,57,'Ascona',1,NULL,NULL),(682,57,'Insignia',1,NULL,NULL),(683,57,'Zafira Tourer',1,NULL,NULL),(684,57,'Ampera',1,NULL,NULL),(685,57,'Mokka',1,NULL,NULL),(686,57,'Adam',1,NULL,NULL),(687,58,'306',1,NULL,NULL),(688,58,'605',1,NULL,NULL),(689,58,'106',1,NULL,NULL),(690,58,'205',1,NULL,NULL),(691,58,'405',1,NULL,NULL),(692,58,'406',1,NULL,NULL),(693,58,'806',1,NULL,NULL),(694,58,'807',1,NULL,NULL),(695,58,'407',1,NULL,NULL),(696,58,'307',1,NULL,NULL),(697,58,'206',1,NULL,NULL),(698,58,'607',1,NULL,NULL),(699,58,'308',1,NULL,NULL),(700,58,'307 Sw',1,NULL,NULL),(701,58,'206 Sw',1,NULL,NULL),(702,58,'407 Sw',1,NULL,NULL),(703,58,'1007',1,NULL,NULL),(704,58,'107',1,NULL,NULL),(705,58,'207',1,NULL,NULL),(706,58,'4007',1,NULL,NULL),(707,58,'Boxer',1,NULL,NULL),(708,58,'Partner',1,NULL,NULL),(709,58,'J5',1,NULL,NULL),(710,58,'604',1,NULL,NULL),(711,58,'505',1,NULL,NULL),(712,58,'309',1,NULL,NULL),(713,58,'Bipper',1,NULL,NULL),(714,58,'Partner Origin',1,NULL,NULL),(715,58,'3008',1,NULL,NULL),(716,58,'5008',1,NULL,NULL),(717,58,'Rcz',1,NULL,NULL),(718,58,'508',1,NULL,NULL),(719,58,'Ion',1,NULL,NULL),(720,58,'208',1,NULL,NULL),(721,58,'4008',1,NULL,NULL),(722,59,'Trans Sport',1,NULL,NULL),(723,59,'Firebird',1,NULL,NULL),(724,59,'Trans Am',1,NULL,NULL),(725,60,'911',1,NULL,NULL),(726,60,'Boxster',1,NULL,NULL),(727,60,'Cayenne',1,NULL,NULL),(728,60,'Carrera Gt',1,NULL,NULL),(729,60,'Cayman',1,NULL,NULL),(730,60,'928',1,NULL,NULL),(731,60,'968',1,NULL,NULL),(732,60,'944',1,NULL,NULL),(733,60,'924',1,NULL,NULL),(734,60,'Panamera',1,NULL,NULL),(735,60,'918',1,NULL,NULL),(736,61,'Megane',1,NULL,NULL),(737,61,'Safrane',1,NULL,NULL),(738,61,'Laguna',1,NULL,NULL),(739,61,'Clio',1,NULL,NULL),(740,61,'Twingo',1,NULL,NULL),(741,61,'Nevada',1,NULL,NULL),(742,61,'Espace',1,NULL,NULL),(743,61,'Spider',1,NULL,NULL),(744,61,'Scenic',1,NULL,NULL),(745,61,'Grand Espace',1,NULL,NULL),(746,61,'Avantime',1,NULL,NULL),(747,61,'Vel Satis',1,NULL,NULL),(748,61,'Grand Scenic',1,NULL,NULL),(749,61,'Clio Campus',1,NULL,NULL),(750,61,'Modus',1,NULL,NULL),(751,61,'Express',1,NULL,NULL),(752,61,'Trafic',1,NULL,NULL),(753,61,'Master',1,NULL,NULL),(754,61,'Kangoo',1,NULL,NULL),(755,61,'Mascott',1,NULL,NULL),(756,61,'Master Propulsion',1,NULL,NULL),(757,61,'Maxity',1,NULL,NULL),(758,61,'R19',1,NULL,NULL),(759,61,'R25',1,NULL,NULL),(760,61,'R5',1,NULL,NULL),(761,61,'R21',1,NULL,NULL),(762,61,'R4',1,NULL,NULL),(763,61,'Alpine',1,NULL,NULL),(764,61,'Fuego',1,NULL,NULL),(765,61,'R18',1,NULL,NULL),(766,61,'R11',1,NULL,NULL),(767,61,'R9',1,NULL,NULL),(768,61,'R6',1,NULL,NULL),(769,61,'Grand Modus',1,NULL,NULL),(770,61,'Kangoo Combi',1,NULL,NULL),(771,61,'Koleos',1,NULL,NULL),(772,61,'Fluence',1,NULL,NULL),(773,61,'Wind',1,NULL,NULL),(774,61,'Latitude',1,NULL,NULL),(775,61,'Grand Kangoo Combi',1,NULL,NULL),(776,62,'Siver Dawn',1,NULL,NULL),(777,62,'Silver Spur',1,NULL,NULL),(778,62,'Park Ward',1,NULL,NULL),(779,62,'Silver Seraph',1,NULL,NULL),(780,62,'Corniche',1,NULL,NULL),(781,62,'Phantom',1,NULL,NULL),(782,62,'Touring',1,NULL,NULL),(783,62,'Silvier',1,NULL,NULL),(784,63,'800',1,NULL,NULL),(785,63,'600',1,NULL,NULL),(786,63,'100',1,NULL,NULL),(787,63,'200',1,NULL,NULL),(788,63,'Coupe',1,NULL,NULL),(789,63,'400',1,NULL,NULL),(790,63,'45',1,NULL,NULL),(791,63,'Cabriolet',1,NULL,NULL),(792,63,'25',1,NULL,NULL),(793,63,'Mini',1,NULL,NULL),(794,63,'75',1,NULL,NULL),(795,63,'Streetwise',1,NULL,NULL),(796,63,'Sd',1,NULL,NULL),(797,64,'900',1,NULL,NULL),(798,64,'93',1,NULL,NULL),(799,64,'9000',1,NULL,NULL),(800,64,'95',1,NULL,NULL),(801,64,'93x',1,NULL,NULL),(802,64,'94x',1,NULL,NULL),(803,65,'300',1,NULL,NULL),(804,65,'350',1,NULL,NULL),(805,65,'Anibal',1,NULL,NULL),(806,65,'Anibal Pick Up',1,NULL,NULL),(807,66,'Ibiza',1,NULL,NULL),(808,66,'Cordoba',1,NULL,NULL),(809,66,'Toledo',1,NULL,NULL),(810,66,'Marbella',1,NULL,NULL),(811,66,'Alhambra',1,NULL,NULL),(812,66,'Arosa',1,NULL,NULL),(813,66,'Leon',1,NULL,NULL),(814,66,'Altea',1,NULL,NULL),(815,66,'Altea Xl',1,NULL,NULL),(816,66,'Altea Freetrack',1,NULL,NULL),(817,66,'Terra',1,NULL,NULL),(818,66,'Inca',1,NULL,NULL),(819,66,'Malaga',1,NULL,NULL),(820,66,'Ronda',1,NULL,NULL),(821,66,'Exeo',1,NULL,NULL),(822,66,'Mii',1,NULL,NULL),(823,67,'Felicia',1,NULL,NULL),(824,67,'Forman',1,NULL,NULL),(825,67,'Octavia',1,NULL,NULL),(826,67,'Octavia Tour',1,NULL,NULL),(827,67,'Fabia',1,NULL,NULL),(828,67,'Superb',1,NULL,NULL),(829,67,'Roomster',1,NULL,NULL),(830,67,'Scout',1,NULL,NULL),(831,67,'Pickup',1,NULL,NULL),(832,67,'Favorit',1,NULL,NULL),(833,67,'130',1,NULL,NULL),(834,67,'S',1,NULL,NULL),(835,67,'Yeti',1,NULL,NULL),(836,67,'Citigo',1,NULL,NULL),(837,67,'Rapid',1,NULL,NULL),(838,68,'Smart',1,NULL,NULL),(839,68,'Citycoupe',1,NULL,NULL),(840,68,'Fortwo',1,NULL,NULL),(841,68,'Cabrio',1,NULL,NULL),(842,68,'Crossblade',1,NULL,NULL),(843,68,'Roadster',1,NULL,NULL),(844,68,'Forfour',1,NULL,NULL),(845,69,'Korando',1,NULL,NULL),(846,69,'Family',1,NULL,NULL),(847,69,'K4d',1,NULL,NULL),(848,69,'Musso',1,NULL,NULL),(849,69,'Korando Kj',1,NULL,NULL),(850,69,'Rexton',1,NULL,NULL),(851,69,'Rexton Ii',1,NULL,NULL),(852,69,'Rodius',1,NULL,NULL),(853,69,'Kyron',1,NULL,NULL),(854,69,'Actyon',1,NULL,NULL),(855,69,'Sports Pick Up',1,NULL,NULL),(856,69,'Actyon Sports Pick Up',1,NULL,NULL),(857,69,'Kodando',1,NULL,NULL),(858,70,'Legacy',1,NULL,NULL),(859,70,'Impreza',1,NULL,NULL),(860,70,'Svx',1,NULL,NULL),(861,70,'Justy',1,NULL,NULL),(862,70,'Outback',1,NULL,NULL),(863,70,'Forester',1,NULL,NULL),(864,70,'G3x Justy',1,NULL,NULL),(865,70,'B9 Tribeca',1,NULL,NULL),(866,70,'Xt',1,NULL,NULL),(867,70,'1800',1,NULL,NULL),(868,70,'Tribeca',1,NULL,NULL),(869,70,'Wrx Sti',1,NULL,NULL),(870,70,'Trezia',1,NULL,NULL),(871,70,'Xv',1,NULL,NULL),(872,70,'Brz',1,NULL,NULL),(873,71,'Maruti',1,NULL,NULL),(874,71,'Swift',1,NULL,NULL),(875,71,'Vitara',1,NULL,NULL),(876,71,'Baleno',1,NULL,NULL),(877,71,'Samurai',1,NULL,NULL),(878,71,'Alto',1,NULL,NULL),(879,71,'Wagon R',1,NULL,NULL),(880,71,'Jimny',1,NULL,NULL),(881,71,'Grand Vitara',1,NULL,NULL),(882,71,'Ignis',1,NULL,NULL),(883,71,'Liana',1,NULL,NULL),(884,71,'Grand Vitara Xl7',1,NULL,NULL),(885,71,'Sx4',1,NULL,NULL),(886,71,'Splash',1,NULL,NULL),(887,71,'Kizashi',1,NULL,NULL),(888,72,'Samba',1,NULL,NULL),(889,72,'Tagora',1,NULL,NULL),(890,72,'Solara',1,NULL,NULL),(891,72,'Horizon',1,NULL,NULL),(892,73,'Telcosport',1,NULL,NULL),(893,73,'Telco',1,NULL,NULL),(894,73,'Sumo',1,NULL,NULL),(895,73,'Safari',1,NULL,NULL),(896,73,'Indica',1,NULL,NULL),(897,73,'Indigo',1,NULL,NULL),(898,73,'Grand Safari',1,NULL,NULL),(899,73,'Tl Pick Up',1,NULL,NULL),(900,73,'Xenon Pick Up',1,NULL,NULL),(901,73,'Vista',1,NULL,NULL),(902,73,'Xenon',1,NULL,NULL),(903,73,'Aria',1,NULL,NULL),(904,74,'Carina E',1,NULL,NULL),(905,74,'4runner',1,NULL,NULL),(906,74,'Camry',1,NULL,NULL),(907,74,'Rav4',1,NULL,NULL),(908,74,'Celica',1,NULL,NULL),(909,74,'Supra',1,NULL,NULL),(910,74,'Paseo',1,NULL,NULL),(911,74,'Land Cruiser 80',1,NULL,NULL),(912,74,'Land Cruiser 100',1,NULL,NULL),(913,74,'Land Cruiser',1,NULL,NULL),(914,74,'Land Cruiser 90',1,NULL,NULL),(915,74,'Corolla',1,NULL,NULL),(916,74,'Auris',1,NULL,NULL),(917,74,'Avensis',1,NULL,NULL),(918,74,'Picnic',1,NULL,NULL),(919,74,'Yaris',1,NULL,NULL),(920,74,'Yaris Verso',1,NULL,NULL),(921,74,'Mr2',1,NULL,NULL),(922,74,'Previa',1,NULL,NULL),(923,74,'Prius',1,NULL,NULL),(924,74,'Avensis Verso',1,NULL,NULL),(925,74,'Corolla Verso',1,NULL,NULL),(926,74,'Corolla Sedan',1,NULL,NULL),(927,74,'Aygo',1,NULL,NULL),(928,74,'Hilux',1,NULL,NULL),(929,74,'Dyna',1,NULL,NULL),(930,74,'Land Cruiser 200',1,NULL,NULL),(931,74,'Verso',1,NULL,NULL),(932,74,'Iq',1,NULL,NULL),(933,74,'Urban Cruiser',1,NULL,NULL),(934,74,'Gt86',1,NULL,NULL),(935,75,'100',1,NULL,NULL),(936,75,'121',1,NULL,NULL),(937,76,'214',1,NULL,NULL),(938,76,'110 Stawra',1,NULL,NULL),(939,76,'111 Stawra',1,NULL,NULL),(940,76,'215',1,NULL,NULL),(941,76,'112 Stawra',1,NULL,NULL),(942,77,'Passat',1,NULL,NULL),(943,77,'Golf',1,NULL,NULL),(944,77,'Vento',1,NULL,NULL),(945,77,'Polo',1,NULL,NULL),(946,77,'Corrado',1,NULL,NULL),(947,77,'Sharan',1,NULL,NULL),(948,77,'Lupo',1,NULL,NULL),(949,77,'Bora',1,NULL,NULL),(950,77,'Jetta',1,NULL,NULL),(951,77,'New Beetle',1,NULL,NULL),(952,77,'Phaeton',1,NULL,NULL),(953,77,'Touareg',1,NULL,NULL),(954,77,'Touran',1,NULL,NULL),(955,77,'Multivan',1,NULL,NULL),(956,77,'Caddy',1,NULL,NULL),(957,77,'Golf Plus',1,NULL,NULL),(958,77,'Fox',1,NULL,NULL),(959,77,'Eos',1,NULL,NULL),(960,77,'Caravelle',1,NULL,NULL),(961,77,'Tiguan',1,NULL,NULL),(962,77,'Transporter',1,NULL,NULL),(963,77,'Lt',1,NULL,NULL),(964,77,'Taro',1,NULL,NULL),(965,77,'Crafter',1,NULL,NULL),(966,77,'California',1,NULL,NULL),(967,77,'Santana',1,NULL,NULL),(968,77,'Scirocco',1,NULL,NULL),(969,77,'Passat Cc',1,NULL,NULL),(970,77,'Amarok',1,NULL,NULL),(971,77,'Beetle',1,NULL,NULL),(972,77,'Up',1,NULL,NULL),(973,77,'Cc',1,NULL,NULL),(974,78,'440',1,NULL,NULL),(975,78,'850',1,NULL,NULL),(976,78,'S70',1,NULL,NULL),(977,78,'V70',1,NULL,NULL),(978,78,'V70 Classic',1,NULL,NULL),(979,78,'940',1,NULL,NULL),(980,78,'480',1,NULL,NULL),(981,78,'460',1,NULL,NULL),(982,78,'960',1,NULL,NULL),(983,78,'S90',1,NULL,NULL),(984,78,'V90',1,NULL,NULL),(985,78,'Classic',1,NULL,NULL),(986,78,'S40',1,NULL,NULL),(987,78,'V40',1,NULL,NULL),(988,78,'V50',1,NULL,NULL),(989,78,'V70 Xc',1,NULL,NULL),(990,78,'Xc70',1,NULL,NULL),(991,78,'C70',1,NULL,NULL),(992,78,'S80',1,NULL,NULL),(993,78,'S60',1,NULL,NULL),(994,78,'Xc90',1,NULL,NULL),(995,78,'C30',1,NULL,NULL),(996,78,'780',1,NULL,NULL),(997,78,'760',1,NULL,NULL),(998,78,'740',1,NULL,NULL),(999,78,'240',1,NULL,NULL),(1000,78,'360',1,NULL,NULL),(1001,78,'340',1,NULL,NULL),(1002,78,'Xc60',1,NULL,NULL),(1003,78,'V60',1,NULL,NULL),(1004,78,'V40 Cross Country',1,NULL,NULL),(1005,79,'353',1,NULL,NULL),(1006,53,'Mini',1,NULL,NULL),(1007,53,'Countryman',1,NULL,NULL),(1008,53,'Paceman',1,NULL,NULL);
/*!40000 ALTER TABLE `modelos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `ordentrabajos`
--

DROP TABLE IF EXISTS `ordentrabajos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ordentrabajos` (
  `idorden` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idrec` int(10) unsigned NOT NULL COMMENT 'es el idrec recepcion',
  `sumrepuestos` double(8,2) NOT NULL DEFAULT 0.00,
  `summano` double(8,2) NOT NULL DEFAULT 0.00,
  `total` double(8,2) NOT NULL DEFAULT 0.00,
  `estado` int(11) NOT NULL DEFAULT 1 COMMENT 'estado de la orden',
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idorden`),
  KEY `ordentrabajos_idrec_foreign` (`idrec`),
  CONSTRAINT `ordentrabajos_idrec_foreign` FOREIGN KEY (`idrec`) REFERENCES `recepcions` (`idrec`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ordentrabajos`
--

LOCK TABLES `ordentrabajos` WRITE;
/*!40000 ALTER TABLE `ordentrabajos` DISABLE KEYS */;
INSERT INTO `ordentrabajos` VALUES (1,1,640.00,2100.00,2740.00,2,1,'2022-04-26 14:12:10','2022-04-26 14:15:47'),(2,2,150.00,100.00,250.00,2,1,'2022-04-27 21:22:10','2022-04-27 21:25:57'),(3,2,0.00,0.00,0.00,1,1,'2022-05-15 14:13:31','2022-05-15 14:13:31');
/*!40000 ALTER TABLE `ordentrabajos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `pagos`
--

DROP TABLE IF EXISTS `pagos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `pagos` (
  `idpago` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idorden` int(10) unsigned NOT NULL COMMENT 'es el idorden orden',
  `detalle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monto` double(8,2) NOT NULL DEFAULT 0.00,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idpago`),
  KEY `pagos_idorden_foreign` (`idorden`),
  CONSTRAINT `pagos_idorden_foreign` FOREIGN KEY (`idorden`) REFERENCES `ordentrabajos` (`idorden`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `pagos`
--

LOCK TABLES `pagos` WRITE;
/*!40000 ALTER TABLE `pagos` DISABLE KEYS */;
INSERT INTO `pagos` VALUES (1,1,'Siliconas',30.00,1,'2022-04-26 14:18:37','2022-04-26 14:18:37'),(2,1,'Juego de empaques',250.00,1,'2022-04-26 14:19:46','2022-04-26 14:19:46'),(3,2,'Frenos',50.00,1,'2022-04-27 21:32:28','2022-04-27 21:32:28'),(4,2,'Aceite',80.00,1,'2022-04-27 21:32:49','2022-04-27 21:32:49');
/*!40000 ALTER TABLE `pagos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `password_resets`
--

DROP TABLE IF EXISTS `password_resets`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `password_resets`
--

LOCK TABLES `password_resets` WRITE;
/*!40000 ALTER TABLE `password_resets` DISABLE KEYS */;
/*!40000 ALTER TABLE `password_resets` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `personal_access_tokens`
--

DROP TABLE IF EXISTS `personal_access_tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) unsigned NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `personal_access_tokens`
--

LOCK TABLES `personal_access_tokens` WRITE;
/*!40000 ALTER TABLE `personal_access_tokens` DISABLE KEYS */;
/*!40000 ALTER TABLE `personal_access_tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recepcions`
--

DROP TABLE IF EXISTS `recepcions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `recepcions` (
  `idrec` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idv` int(10) unsigned NOT NULL COMMENT 'es el idv vehiculo',
  `diag` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ul` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Unidad de luces',
  `an` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Antenas',
  `el` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Espejo lateral',
  `llan` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Llantas 4',
  `bx` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Bocinas claxon',
  `ga` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Gata',
  `llar` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Llave de rueda',
  `llac` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Llave de contacto',
  `eh` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Estuche herramienta',
  `ts` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Triangulo de seguridad',
  `llax` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Llanta de auxilio',
  `ra` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Radio',
  `it` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Instrumento de tablero',
  `en` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Encendedor',
  `ca` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Calefaccion',
  `ce` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Cenicero',
  `to` tinyint(1) NOT NULL DEFAULT 0 COMMENT 'Topetes',
  `obsrec` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `estado` int(11) NOT NULL DEFAULT 1 COMMENT 'estado de la recepcion',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idrec`),
  KEY `recepcions_idv_foreign` (`idv`),
  CONSTRAINT `recepcions_idv_foreign` FOREIGN KEY (`idv`) REFERENCES `vehiculos` (`idv`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recepcions`
--

LOCK TABLES `recepcions` WRITE;
/*!40000 ALTER TABLE `recepcions` DISABLE KEYS */;
INSERT INTO `recepcions` VALUES (1,1,'Desmonte de transición automática\r\nDesarmado de culatas',0,0,0,1,1,1,0,0,1,1,0,0,0,1,0,1,0,'Tiene una llanta de auxilio extra',1,1,'2022-04-26 14:11:05','2022-04-26 14:11:05'),(2,2,'El vehiculo tiene dificultades a hora de arrancar,y no cuenta con filtro de aceite',0,1,1,1,0,1,0,1,1,0,0,0,0,1,0,1,1,'Cuenta con una llanta de auxilo extra en vehiculo',1,1,'2022-04-27 21:19:49','2022-04-27 21:19:49');
/*!40000 ALTER TABLE `recepcions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `reparacions`
--

DROP TABLE IF EXISTS `reparacions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `reparacions` (
  `idrepa` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idorden` int(10) unsigned NOT NULL COMMENT 'es el idorden orden',
  `detalle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idrepa`),
  KEY `reparacions_idorden_foreign` (`idorden`),
  CONSTRAINT `reparacions_idorden_foreign` FOREIGN KEY (`idorden`) REFERENCES `ordentrabajos` (`idorden`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `reparacions`
--

LOCK TABLES `reparacions` WRITE;
/*!40000 ALTER TABLE `reparacions` DISABLE KEYS */;
INSERT INTO `reparacions` VALUES (1,1,'Fuga de Aceite de Motor',1,'2022-04-26 14:16:33','2022-04-26 14:16:33'),(2,2,'Se arreglo el cableado eléctrico',1,'2022-04-27 21:30:38','2022-04-27 21:30:38');
/*!40000 ALTER TABLE `reparacions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `repuestos`
--

DROP TABLE IF EXISTS `repuestos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `repuestos` (
  `idrepu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idorden` int(10) unsigned NOT NULL COMMENT 'es el idorden orden',
  `detalle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `monto` int(11) NOT NULL DEFAULT 0,
  `cant` int(11) NOT NULL DEFAULT 1,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idrepu`),
  KEY `repuestos_idorden_foreign` (`idorden`),
  CONSTRAINT `repuestos_idorden_foreign` FOREIGN KEY (`idorden`) REFERENCES `ordentrabajos` (`idorden`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `repuestos`
--

LOCK TABLES `repuestos` WRITE;
/*!40000 ALTER TABLE `repuestos` DISABLE KEYS */;
INSERT INTO `repuestos` VALUES (1,1,'Kit de reparación',360,2,1,'2022-04-26 14:12:49','2022-04-26 14:12:49'),(2,1,'Silicona',30,2,1,'2022-04-26 14:13:12','2022-04-26 14:13:12'),(3,1,'Juego de Empaques',250,1,1,'2022-04-26 14:13:37','2022-04-26 14:13:37'),(4,2,'Aceite',50,2,1,'2022-04-27 21:23:05','2022-04-27 21:23:05'),(5,2,'Frenos',100,1,1,'2022-04-27 21:23:30','2022-04-27 21:23:30');
/*!40000 ALTER TABLE `repuestos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `submenus`
--

DROP TABLE IF EXISTS `submenus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `submenus` (
  `idsubmenu` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `menu` int(10) unsigned NOT NULL COMMENT 'es el idmenu padre',
  `nombresubmenu` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idsubmenu`),
  KEY `submenus_menu_foreign` (`menu`),
  CONSTRAINT `submenus_menu_foreign` FOREIGN KEY (`menu`) REFERENCES `menus` (`idmenu`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `submenus`
--

LOCK TABLES `submenus` WRITE;
/*!40000 ALTER TABLE `submenus` DISABLE KEYS */;
INSERT INTO `submenus` VALUES (1,1,'Registro Cliente',1,NULL,NULL),(2,1,'Reportes',1,NULL,NULL),(3,2,'Registro Vehiculo',1,NULL,NULL),(4,2,'Recepcion',1,NULL,NULL),(5,2,'Reportes',1,NULL,NULL),(6,3,'Orden de trabajo',1,NULL,NULL),(7,3,'Reparacion',1,NULL,NULL),(9,3,'Notificar',1,NULL,NULL),(10,4,'Registro de pago',1,NULL,NULL),(11,4,'Reportes',1,NULL,NULL);
/*!40000 ALTER TABLE `submenus` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tipo_users`
--

DROP TABLE IF EXISTS `tipo_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tipo_users` (
  `idtipo` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `detalle` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idtipo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tipo_users`
--

LOCK TABLES `tipo_users` WRITE;
/*!40000 ALTER TABLE `tipo_users` DISABLE KEYS */;
INSERT INTO `tipo_users` VALUES (1,'Administador',NULL,NULL),(2,'Contador general',NULL,NULL),(3,'Secretaria',NULL,NULL),(4,'Mecanico',NULL,NULL);
/*!40000 ALTER TABLE `tipo_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `usuario` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nombre` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tipo` int(10) unsigned NOT NULL COMMENT 'es el tipo de usuario del sistema',
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_usuario_unique` (`usuario`),
  KEY `users_tipo_foreign` (`tipo`),
  CONSTRAINT `users_tipo_foreign` FOREIGN KEY (`tipo`) REFERENCES `tipo_users` (`idtipo`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'Fama motors','$2y$10$RwYRHJukSiz5xZN54JPEn.d992w6K4zwzAv/Om/UZ/QO.Xl18s50u',NULL,'Fama Motors',1,NULL,'2022-04-26 13:48:41','2022-04-26 13:48:41');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vehiculos`
--

DROP TABLE IF EXISTS `vehiculos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `vehiculos` (
  `idv` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `idcli` int(10) unsigned NOT NULL COMMENT 'es el idcli cliente',
  `idma` int(10) unsigned NOT NULL COMMENT 'es el idma marca',
  `idmod` int(10) unsigned NOT NULL COMMENT 'es el idmod modelo',
  `color` int(10) unsigned NOT NULL COMMENT 'es el color',
  `placa` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  `obs` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `activo` tinyint(1) NOT NULL DEFAULT 1,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`idv`),
  KEY `vehiculos_idcli_foreign` (`idcli`),
  KEY `vehiculos_idma_foreign` (`idma`),
  KEY `vehiculos_idmod_foreign` (`idmod`),
  KEY `vehiculos_color_foreign` (`color`),
  CONSTRAINT `vehiculos_color_foreign` FOREIGN KEY (`color`) REFERENCES `colors` (`idco`),
  CONSTRAINT `vehiculos_idcli_foreign` FOREIGN KEY (`idcli`) REFERENCES `clientes` (`idcli`),
  CONSTRAINT `vehiculos_idma_foreign` FOREIGN KEY (`idma`) REFERENCES `marcas` (`idma`),
  CONSTRAINT `vehiculos_idmod_foreign` FOREIGN KEY (`idmod`) REFERENCES `modelos` (`idmod`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vehiculos`
--

LOCK TABLES `vehiculos` WRITE;
/*!40000 ALTER TABLE `vehiculos` DISABLE KEYS */;
INSERT INTO `vehiculos` VALUES (1,2,71,876,2,'3154-LPB',2006,'En Buen estado entrante',1,'2022-04-26 14:08:51','2022-04-26 14:08:51'),(2,6,71,881,3,'2049-YIG',2017,'No cuenta con palabrisas delanteras',1,'2022-04-27 21:16:55','2022-04-27 21:16:55');
/*!40000 ALTER TABLE `vehiculos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-05-15 13:54:08
