<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->increments('idcli');
            $table->string('foto')->nullable();
            $table->string('nomcli')->nullable();
            $table->string('apcli')->nullable();
            $table->string('telcli')->nullable();
            $table->bigInteger('ci')->default(0);
            $table->string('emailcli')->nullable();
            $table->text('dircli')->nullable();
            $table->boolean('activo')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
};
