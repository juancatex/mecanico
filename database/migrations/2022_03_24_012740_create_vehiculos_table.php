<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehiculos', function (Blueprint $table) {
            $table->increments('idv');
            $table->integer('idcli')->unsigned()->comment('es el idcli cliente');
            $table->integer('idma')->unsigned()->comment('es el idma marca');
            $table->integer('idmod')->unsigned()->comment('es el idmod modelo');
            $table->integer('color')->unsigned()->comment('es el color'); 
            $table->string('placa')->nullable();
            $table->string('fotov')->nullable();
            $table->integer('anio')->nullable();
            $table->text('obs')->nullable();
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('idcli')->references('idcli')->on('clientes');
            $table->foreign('idma')->references('idma')->on('marcas');
            $table->foreign('idmod')->references('idmod')->on('modelos');
            $table->foreign('color')->references('idco')->on('colors');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehiculos');
    }
};
