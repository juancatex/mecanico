<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('modelos', function (Blueprint $table) {
            $table->increments('idmod');
            $table->integer('marca')->unsigned()->comment('es el idma marca');//
            $table->string('modelo')->nullable();
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('marca')->references('idma')->on('marcas');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('modelos');
    }
};
