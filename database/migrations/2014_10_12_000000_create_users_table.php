<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id(); 
            $table->string('usuario')->unique();
            $table->string('password');
            $table->string('email')->nullable();
            $table->string('nombre');   
            $table->integer('tipo')->unsigned()->comment('es el tipo de usuario del sistema');//
            $table->rememberToken();
            $table->timestamps();

            $table->foreign('tipo')->references('idtipo')->on('tipo_users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
