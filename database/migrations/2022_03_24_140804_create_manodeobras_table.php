<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('manodeobras', function (Blueprint $table) {
            $table->increments('idmano');
            $table->integer('idorden')->unsigned()->comment('es el idorden orden');
            $table->string('detalle')->nullable(); 
            $table->integer('monto')->default(0); 
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('idorden')->references('idorden')->on('ordentrabajos'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('manodeobras');
    }
};
