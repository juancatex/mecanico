<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ordentrabajos', function (Blueprint $table) {
            $table->increments('idorden');
            $table->integer('idrec')->unsigned()->comment('es el idrec recepcion');
            $table->float('sumrepuestos')->default(0);
            $table->float('summano')->default(0);
            $table->float('total')->default(0);
            $table->integer('estado')->default(1)->comment('estado de la orden'); 
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('idrec')->references('idrec')->on('recepcions'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ordentrabajos');
    }
};
