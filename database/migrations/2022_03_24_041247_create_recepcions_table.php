<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('recepcions', function (Blueprint $table) {
            $table->increments('idrec');
            $table->integer('idv')->unsigned()->comment('es el idv vehiculo');
            
            $table->text('diag')->nullable(); 
            $table->boolean('ul')->default(0)->comment('Unidad de luces'); 
            $table->boolean('an')->default(0)->comment('Antenas'); 
            $table->boolean('el')->default(0)->comment('Espejo lateral'); 
            $table->boolean('llan')->default(0)->comment('Llantas 4'); 
            $table->boolean('bx')->default(0)->comment('Bocinas claxon'); 

            $table->boolean('ga')->default(0)->comment('Gata'); 
            $table->boolean('llar')->default(0)->comment('Llave de rueda'); 
            $table->boolean('llac')->default(0)->comment('Llave de contacto'); 
            $table->boolean('eh')->default(0)->comment('Estuche herramienta'); 
            $table->boolean('ts')->default(0)->comment('Triangulo de seguridad'); 
            $table->boolean('llax')->default(0)->comment('Llanta de auxilio'); 

            $table->boolean('ra')->default(0)->comment('Radio'); 
            $table->boolean('it')->default(0)->comment('Instrumento de tablero'); 
            $table->boolean('en')->default(0)->comment('Encendedor'); 
            $table->boolean('ca')->default(0)->comment('Calefaccion'); 
            $table->boolean('ce')->default(0)->comment('Cenicero'); 
            $table->boolean('to')->default(0)->comment('Topetes'); 

            $table->boolean('mc1')->default(0); 
            $table->boolean('mc2')->default(0); 
            $table->boolean('mc3')->default(0); 
            $table->boolean('mc4')->default(0); 
            $table->boolean('mc5')->default(0); 
            $table->boolean('mc6')->default(0); 

            $table->boolean('mp1')->default(0); 
            $table->boolean('mp2')->default(0); 
            $table->boolean('mp3')->default(0); 
            $table->boolean('mp4')->default(0); 
            $table->boolean('mp5')->default(0); 
            $table->boolean('mp6')->default(0); 

            $table->boolean('mpr1')->default(0); 
            $table->boolean('mpr2')->default(0); 
            $table->boolean('mpr3')->default(0);

            $table->boolean('mpre1')->default(0); 
            $table->boolean('mpre2')->default(0); 
            $table->boolean('mpre3')->default(0); 
            $table->boolean('mpre4')->default(0); 

            $table->boolean('rev1')->default(0);
            $table->boolean('rev2')->default(0);
            $table->boolean('rev3')->default(0);
            $table->boolean('rev4')->default(0);
            $table->boolean('rev5')->default(0);
            $table->boolean('rev6')->default(0);
            $table->boolean('rev7')->default(0);
            $table->boolean('rev8')->default(0);
            $table->boolean('rev9')->default(0);
            
            $table->boolean('otro1')->default(0);
            $table->boolean('otro2')->default(0);

            $table->text('obsrec')->nullable();
            $table->boolean('activo')->default(1);
            $table->integer('estado')->default(1)->comment('estado de la recepcion'); 
            $table->timestamps();
            $table->foreign('idv')->references('idv')->on('vehiculos'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('recepcions');
    }
};
