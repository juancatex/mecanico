<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('submenus', function (Blueprint $table) {
            $table->increments('idsubmenu');
            $table->integer('menu')->unsigned()->comment('es el idmenu padre');//
            $table->string('nombresubmenu')->nullable();
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('menu')->references('idmenu')->on('menus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('submenus');
    }
};
