<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reparacions', function (Blueprint $table) {
            $table->increments('idrepa');
            $table->integer('idorden')->unsigned()->comment('es el idorden orden');
            $table->string('detalle')->nullable(); 
            $table->boolean('activo')->default(1);
            $table->timestamps();
            $table->foreign('idorden')->references('idorden')->on('ordentrabajos'); 
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reparacions');
    }
};
